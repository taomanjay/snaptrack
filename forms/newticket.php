<?php
require_once( '../inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 1 ) {
	header("HTTP/1.1 500 Internal Server Error");
	echo "Insufficient privileges";

	die();
	}

if ( isset( $_GET['project_id'] ) ) {
	$components = getcomponents( $_GET['project_id'] );

	array_unshift( $components, array(
		'component_id' => '',
		'component_name' => 'None'
		) );

	$json = json_encode( $components );

	echo $json;

	die();
	}

$projects = getprojects( NULL, NULL, NULL );
$components = getcomponents( $_SESSION['project']['project_id'] ) ;

if ( preg_match( '/newticket.php$/', $_SERVER['SCRIPT_FILENAME'] ) ) {
	$class = '';
	}
else {
	$class = 'hidden';
	}
?>
  <form method='post' id='newticket' class='<?php echo $class?>'>
    <input type='hidden' name='form' value='newticket'>
    <div class='title'>New Ticket
      <div class='titlebar-button close' onclick="killOverlay( event, 'newticket' );"></div>
    </div>
    <table style='vertical-align: top;'>
      <tr>
        <td>
          <div style='position: relative;'>
            <input id='newticket_title' type='text' name='title' autofocus required>
            <label class='tag' for='newticket_title'>Title</label>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div style='position: relative;'>
            <select id='newticket_project_id' name='project_id' onchange="getSelectHTTP('forms/newticket.php?project_id='+this.value,document.getElementById('newticket_component'));">
<?php
foreach ( $projects as $project ) {
	if ( $_SESSION['project']['project_id'] === $project['project_id'] ) {
		$sel = 'selected';
		}
	else {
		$sel = '';
		}

	echo "              <option value={$project['project_id']} $sel>"
		. "{$project['project_name']}"
		. "</option>\n";
	}

echo "            </select>\n"
	. "            <label class='tag' for='newticket_project_id'>"
	. "Project</label>"
	. "          </div>\n"
	. "        </td>\n"
	. "      </tr>\n"
	. "      <tr>\n"
	. "        <td>\n"
	. "          <div style='position: relative;'>\n"
	. "            <select id='newticket_component' name='component_id'>\n"
	. "              <option value=''>None</option>\n";

foreach ( $components as $component ) {
	echo "              <option value={$component['component_id']}>"
		. "{$component['component_name']}</option>\n";
	}
?>
            </select>
            <label class='tag' for='newticket_component'>Component</label>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div style='position: relative;'>
            <textarea rows=20 id='newticket_note' name='note' required></textarea>
            <label class='tag' for='newticket_note'>Note</label>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <input class='buttons' type='submit' value='Create ticket'>
        </td>
      </tr>
    </table>
  </form>
