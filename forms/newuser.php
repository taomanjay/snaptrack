<?php
require_once( '../inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 2 ) {
	header("HTTP/1.1 500 Internal Server Error");
	echo "Insufficient privileges";

	die();
	}
?>
  <form method='post' id='newuser'>
    <input type='hidden' name='form' value='newuser'>
    <div class='title'>Add User
      <div class='titlebar-button close' onclick="killOverlay( event, 'newuser' );"></div>
    </div>
    <table style='vertical-align: top;'>
      <tr>
        <td>
          <div style='position: relative;'>
            <input type='text' id='user_name' name='user_name' tabindex=1 autofocus required>
            <label class='tag' for='user_name'>Username</label>
          </div>
        </td>
        <td>
          <div style='position: relative;'>
            <input type='text' id='email' name='email' tabindex=5 required>
            <label class='tag' for='email'>Email</label>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div style='position: relative;'>
            <input type='text' id='first_name' name='first_name' tabindex=2>
            <label class='tag' for='first_name'>First name</label>
          </div>
        </td>
        <td>
          <div style='position: relative;'>
            <input type='password' id='password' name='password' tabindex=6 required>
            <label class='tag' for='password'>Password</label>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div style='position: relative;'>
            <input type='text' id='surname' name='surname' tabindex=3>
            <label class='tag' for='surname'>Surname</label>
          </div>
        </td>
        <td>
          <div style='position: relative;'>
            <input type='password' id='verify_password' name='verify_password' tabindex=7 required>
            <label class='tag' for='verify_password'>Verify password</label>
          </div>
        </td>
      </tr>
      <tr>
        <td style='padding-top: 17px;'>
          <div style='position: relative;'>
            <select id='priv_id' name='priv_id' tabindex=4>
              <option value=1>User</option>
              <option value=2>Admin</option>
            </select>
            <label class='tag' for='priv_id'>Privilege</label>
          </div>
        </td>
        <td>
          <div style='position: relative;'>
            <input class='buttons' type='submit' value='Add user' tabindex=8 style='height: 54px;'>
          </div>
        </td>
      </tr>
    </table>
  </form>
