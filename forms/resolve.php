<?php
require_once( '../inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 1 ) {
	header("HTTP/1.1 500 Internal Server Error");
	echo "Insufficient privileges";

	die();
	}

if ( isset( $_GET['ticket_id'] ) ) {
	$ticket_id = $_GET['ticket_id'];
	}
else {
	header("HTTP/1.1 500 Internal Server Error");
	echo "Did not supply ticket ID";

	die();
	}

$ticket = getticket( $ticket_id );

if ( isset( $ticket[0] ) && $ticket[0] === -1 ) {
	header("HTTP/1.1 500 Internal Server Error");
	echo "In getticket(): {$ticket[1]}";

	die();
	}

?>
  <form method='post' id='resolve'>
    <input type='hidden' name='form' value='resolve'>
    <input type='hidden' name='ticket_id' value=<?php echo $ticket_id?>>
    <div class='title'>Resolve Ticket #<?php echo $ticket_id?>
      <div class='titlebar-button close' onclick="killOverlay( event, 'newticket' );"></div>
    </div>
    <table style='vertical-align: top;'>
      <tr>
        <td>
          <div style='position: relative;'>
            <textarea rows=20 id='newticket_note' name='note' required></textarea>
            <label class='tag' for='newticket_note'>Note</label>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <input class='buttons' type='submit' value='Resolve ticket'>
        </td>
      </tr>
    </table>
  </form>
