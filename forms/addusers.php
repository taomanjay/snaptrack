<?php
include( 'redirect.php' );

$self = basename( $_SERVER['PHP_SELF'] );
$class = 'hidden';

if ( ! isset( $gadget ) ) {
	die();
	}

/*
if ( $self === 'groups.php' ) {
	$idname = 'group_id';
	}
elseif ( $self === 'projects.php' ) {
	$idname = 'project_id';
	}
*/

if ( $gadget === 'groups' ) {
	$hidden = 'group_id';
	$title = 'Add Users';
	$id = 'groups_addusers';
	}
elseif ( $gadget === 'projects' ) {
	$hidden = 'project_id';
	$title = 'Add Admins';
	$id = 'projects_addusers';
	}
/*
else {
	if ( isset( $_GET['idname'] ) ) {
		$idname = $_GET['idname'];
		}
	else {
		header( 'Location: ../' );

		die();
		}

	if ( isset( $_GET['id'] ) ) {
		$id = $_GET[$idname];
//		$group = getgroup( $group_id );
		}
	else {
		header( 'Location: ../' );

		die();
		}

	$class = '';
	}
*/

echo "  <form method='post' id='$id' class='$class'>
    <div class='title'>$title"; // $group_name\n";

echo "      <div class='titlebar-button close'"
	. " onclick=\"killOverlay( event, '$id' );\"></div>\n";

echo "    </div>
    <input type='hidden' id='{$gadget}_adduser_id'"
	. " name='$hidden' value='ID'>\n";

$users = getusers( NULL, NULL );

foreach ( $users as $user ) {
	$checkbox = "{$gadget}_check{$user['user_id']}";

	echo "    <input type='checkbox' class='addusers' name='users[]'"
		. " id='$checkbox' value='{$user['user_id']}'>"
		. "<label for='$checkbox' class='check'>"
		. "{$user['user_name']}</label>\n";
	}

echo "    <input class='buttons' type='submit' value='Add users'>
  </form>\n";
?>
