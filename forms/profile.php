<?php
//require_once( '../inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 1 ) {
	header("HTTP/1.1 500 Internal Server Error");
	echo "Insufficient privileges";

	die();
	}

if ( file_exists( "avatars/{$_SESSION['user']['user_id']}.jpg" ) ) {
	$avatar = "avatars/{$_SESSION['user']['user_id']}.jpg";
	$avatar .= '?t=' . filemtime( $avatar );
	}
else {
	$avatar = 'avatars/profile.png';
	}
?>
  <form method='post' id='editprofile' enctype='multipart/form-data'>
    <input type='hidden' name='form' value='editprofile'>
    <input type='hidden' name='user_id' value='<?php echo $user_id?>'>
    <div class='title'>My Profile
    </div>
    <table style='vertical-align: top;'>
      <tr>
        <td style='position: relative; padding: 0px;' colspan=2>
          <input type='file' id='avatar' name='avatar' style='display: none;' onchange="updateAvatar(event,'avatarimg');">
          <div id='avatarimg' class='avatar' style='background-image: url(<?php echo $avatar?>);' onclick="document.getElementById('avatar').click()"></div>
          <div style='display: inline-block; position: relative; float: right; margin-top: 17px;'>
            <input type='text' id='user_name' name='user_name' tabindex=1 value='<?php echo $user['user_name'];?>' style='width: 288px;' disabled>
            <label class='tag' for='user_name' style='width: 306px;'>Username</label>
          </div>
          <div style='position: absolute; right: 0px; bottom: 0px;'>
            <input type='text' id='email' name='email' tabindex=1 value='<?php echo $user['email'];?>' style='width: 288px;'>
            <label class='tag' for='email' style='width: 306px;'>Email</label>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div style='position: relative;'>
            <input type='text' id='first_name' name='first_name' tabindex=2 value='<?php echo $user['first_name'];?>'>
            <label class='tag' for='first_name'>First name</label>
          </div>
        </td>
        <td>
          <div style='position: relative;'>
            <input type='password' id='password' name='password' tabindex=3>
            <label class='tag' for='password'>Password</label>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div style='position: relative;'>
            <input type='text' id='surname' name='surname' tabindex=2 value='<?php echo $user['surname'];?>'>
            <label class='tag' for='surname'>Surname</label>
          </div>
        </td>
        <td>
          <div style='position: relative;'>
            <input type='password' id='verify_password' name='verify_password' tabindex=4>
            <label class='tag' for='verify_password'>Verify password</label>
          </div>
        </td>
      </tr>
        <td colspan=2>
          <div style='position: relative;'>
            <input class='buttons' type='submit' value='Save' tabindex=8 style='width: 456px;'>
          </div>
        </td>
      </tr>
    </table>
  </form>
