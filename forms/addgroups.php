<?php
include( 'redirect.php' );

if ( preg_match( '/addusers.php$/', $_SERVER['SCRIPT_FILENAME'] ) ) {
	$class = '';
	}
else {
	$class = 'hidden';
	}

if ( isset( $_GET['group_id'] ) ) {
	$group_id = $_GET['group_id'];
	$group = getgroup( $group_id );
	}
else {
	$group_id = '';
	}

echo "  <form method='post' id='addusers' class='$class'>"
	. "    <div class='title'>Add Users to Group"; // $group_name\n";

if ( empty( $group_id ) ) {
	echo "      <div class='close' onclick=\""
		. "killOverlay( event, 'addusers' );\"></div>\n";
	}

echo "    </div>
<input type='hidden' id='adduser_group_id'"
	. " name='group_id' value='$group_id'>\n";

$users = getusers( NULL, NULL );

foreach ( $users as $user ) {
	echo "<input type='checkbox' class='addusers' name='users[]'"
		. " id='check{$user['user_id']}' value='{$user['user_id']}'>"
		. "<label for='check{$user['user_id']}' class='check'>"
		. "{$user['user_name']}</label>\n";
	}

echo "    <input class='buttons' type='submit' value='Add users'>
  </form>\n";
?>
