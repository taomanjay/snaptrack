<?php
require_once( '../inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 1 ) {
	header("HTTP/1.1 500 Internal Server Error");
	echo "In newgroup.php: Insufficient privileges";

	die();
	}

if ( file_exists( "../avatars/{$_SESSION['user']['user_id']}.jpg" ) ) {
	$avatar = "avatars/{$_SESSION['user']['user_id']}.jpg";
	}
else {
	$avatar = 'images/profile.png';
	}
?>
  <form method='post' id='avatarupload' enctype='multipart/form-data'>
    <input type='hidden' name='form' value='avatarupload'>
    <div class='title'>Change Avatar
      <div class='titlebar-button close' onclick="killOverlay( event );"></div>
    </div>
    <table style='vertical-align: top;'>
      <tr>
        <td>
          <div style='position: relative; height: 168px;'>
            <input type='file' id='avatar' name='avatar' style='display: none; width: 360px; font-size: 14px;' onchange="updateAvatar(event,'avatarlabel');">
            <label class='tag' id='avatarlabel' for='avatar' style='padding: 0px; height: 185px; border-bottom: 1px solid #BDC4C9; background-image: url(<?php echo $avatar;?>); background-repeat: no-repeat; background-position: center; background-size: cover; cursor: pointer;'><div style='padding: 2px 8px 2px 8px; color: #FFF; background-color: rgba(0,0,0,0.5);'>File (Click to select file)</div></label>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <input class='buttons' type='submit' value='Save' style=''>
        </td>
      </tr>
    </table>
  </form>
