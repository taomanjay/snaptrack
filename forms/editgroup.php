<?php
require_once( '../inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 2 ) {
	return( array( -1, 'Insufficient privileges' ) );
	}

if ( isset( $_GET['group_id'] ) ) {
	$group_id = $_GET['group_id'];
	$group = getgroup( $group_id );
	$group_name = $group[0]['group_name'];
	$group_users = '';
	$allusers = getusers( NULL, NULL, NULL );
	$users = '';
	$allprojects = getprojects( NULL, NULL, NULL );
	$projects = '';
	$group_projects = '';

	if ( isset( $group[0] ) && $group[0] === -1 ) {
		header("HTTP/1.1 500 Internal Server Error");
		echo "In getgroup(): {$group[1]}";

		die();
		}
	elseif ( isset( $allusers[0] ) && $allusers[0] === -1 ) {
		header("HTTP/1.1 500 Internal Server Error: {$allusers[1]}");
		echo "In getusers(): {$allusers[1]}";

		die();
		}
	elseif ( isset( $allprojects[0] ) && $allprojects[0] === -1 ) {
		header("HTTP/1.1 500 Internal Server Error: {$allprojects[1]}");
		echo "In getprojects(): {$allprojects[1]}";

		die();
		}

	foreach ( $allusers as $user ) {
		foreach ( $group as $group_user ) {
			if ( $user['user_id'] === $group_user['user_id'] ) {
				continue( 2 );
				}
			}

		$users .= "<option value={$user['user_id']}>"
			. "{$user['user_name']}</option>\n";
		}

	foreach ( $group as $group_user ) {
		if ( empty( $group_user['user_id'] ) ) {
			continue;
			}

		$group_users .= "<option value={$group_user['user_id']}>"
			. "{$group_user['user_name']}</option>\n";
		}

	foreach ( $allprojects as $project ) {
		$groups = getprojectgroups( $project['project_id'] );

		if ( isset( $groups[0] ) && $groups[0] === -1 ) {
			header("HTTP/1.1 500 Internal Server Error");
			echo "In getprojectgroups(): {$groups[1]}";

			die();
			}

		foreach ( $groups as $project_group ) {
			if ( $project_group['group_id'] == $group_id ) {
				$group_projects .= "<option value="
					. "{$project['project_id']}>"
					. "{$project['project_name']}"
					. "</option>\n";

				continue( 2 );
				}
			}

		$projects .= "<option value="
			. "{$project['project_id']}>"
			. "{$project['project_name']}</option>\n";
		}
	}
else {
	header("HTTP/1.1 500 Internal Server Error");
	echo "In editgroup.php: group_id not supplied";

	die();
	}
?>

<form method='post' id='editgroup'>
  <input type='hidden' name='form' value='editgroup'>
  <input type='hidden' name='group_id' value='<?php echo $group_id?>'>
  <div class='title'>Edit Group
    <div class='titlebar-button close' onclick="killOverlay( event, 'editgroup' );"></div>
  </div>
  <table style='vertical-align: top;'>
    <tr>
      <td colspan=2>
        <div style='position: relative;'>
          <input id='editgroup_group_name' type='text' name='group_name' value='<?php echo $group_name;?>' autofocus style='width: 440px;'>
          <label class='tag' for='editgroup_group_name' style='width: 458px;'>Group Name</label>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div style='position: relative;'>
          <select id='editgroup_users' name='editgroup_users' multiple onchange="moveTo(this,'editgroup_group_users');" style='height: 200px;'>
<?php echo $users;?>
          </select>
          <label class='tag' for='editgroup_users'>Non-members</label>
        </div>
      </td>
      <td>
        <div style='position: relative;'>
          <select id='editgroup_group_users' name='users[]' multiple onchange="moveTo(this,'editgroup_users');" style='height: 200px;'>
<?php echo $group_users;?>
          </select>
          <label class='tag' for='editgroup_group_users'>Members</label>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div style='position: relative;'>
          <select id='editgroup_projects' name='editgroup_projects' multiple onchange="moveTo(this,'editgroup_group_projects');" style='height: 200px;'>
<?php echo $projects;?>
          </select>
          <label class='tag' for='editgroup_projectss'>Unassigned Projects</label>
        </div>
      </td>
      <td>
        <div style='position: relative;'>
          <select id='editgroup_group_projects' name='projects[]' multiple onchange="moveTo(this,'editgroup_projects');" style='height: 200px;'>
<?php echo $group_projects;?>
          </select>
          <label class='tag' for='editgroup_group_projects'>Assigned Projects</label>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <input class='buttons delete' name='delete' type='submit' value='Delete group'>
      </td>
      <td>
        <input class='buttons' type='submit' value='Save' onclick="selectAll('editgroup_group_users');selectAll('editgroup_group_projects');">
      </td>
    </tr>
  </table>
</form>
<script src='js/dragula.js'></script>
<script type='text/javascript'>
dragula([document.querySelector('#editgroup_users'),
	document.querySelector('#editgroup_group_users')]);
</script>
