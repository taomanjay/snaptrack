<?php
require_once( '../inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 2 ) {
	header( "HTTP/1.1 401 Unauthorized" );
	die( "In restoreuser.php: Insufficient privileges" );
	}

if ( preg_match( '/restoreuser.php$/', $_SERVER['SCRIPT_FILENAME'] ) ) {
	$class = '';
	}
else {
	$class = 'hidden';
	}

if ( isset( $_GET['user_id'] ) ) {
	$user_id = $_GET['user_id'];
	}
else {
	header( "HTTP/1.1 500 Internal Server Error");
	die( "In restoreuser.php: user id not provided" );
	}
if ( isset( $_GET['user_name'] ) ) {
	$user_name = $_GET['user_name'];
	}
else {
	header( "HTTP/1.1 500 Internal Server Error");
	die( "In restoreuser.php: user name not provided" );
	}
	
?>
  <form method='post' id='restoreuser' class='<?php echo $class?>'>
    <input type='hidden' name='form' value='restoreuser'>
    <input type='hidden' name='user_id' value='<?php echo $user_id?>'>
    <div class='title'>Restore user
      <div class='titlebar-button close' onclick="killOverlay( event );"></div>
    </div>
    <table style='vertical-align: top;'>
      <tr>
        <td style='padding: 0px;'>Restore user <?php echo $user_name?>?</td>
      </tr>
      <tr>
        <td>
          <input class='buttons' type='submit' value='Restore'>
        </td>
      </tr>
    </table>
  </form>
