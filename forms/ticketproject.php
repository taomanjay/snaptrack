<?php
require_once( '../inc/session.php' );

if ( ! isset( $_GET['ticket_id'] ) ) {
	header("HTTP/1.1 500 Internal Server Error");
	die( "In ticketcomponent.php: Failed to supply ticket id" );
	}
else {
	$ticket_id = $_GET['ticket_id'];
	}

if ( ! isset( $_GET['project_id'] ) ) {
	header("HTTP/1.1 500 Internal Server Error");
	die( "In ticketcomponent.php: Failed to supply project id" );
	}
else {
	$project_id = $_GET['project_id'];
	}

if ( ! icanhaz( $ticket_id ) ) {
	header("HTTP/1.1 401 Unauthorized");
	die( 'You do not have permission to modify this' );
	}

$project = getproject( $project_id );
$projects = getprojects( NULL, NULL, NULL );

if ( isset( $project[0] ) && $project[0] === -1 ) {
	header("HTTP/1.1 500 Internal Server Error");
	die( "In getproject(): {$project[1]}" );
	}
elseif ( isset( $components[0] ) && $components[0] === -1 ) {
	header("HTTP/1.1 500 Internal Server Error");
	die( "In getcomponents(): {$components[1]}" );
	}

$options = '';

foreach ( $projects as $project ) {
	$options .= "<option value="
		. "{$project['project_id']}";

	if ( $project['project_id'] == $project_id ) {
		$options .= ' selected';
		}

	$options .= ">{$project['project_name']}</option>\n";
	}
?>

<form method='post' id='ticketproject'>
  <input type='hidden' name='form' value='ticketproject'>
  <input type='hidden' name='ticket_id' value='<?php echo $ticket_id?>'>
  <input type='hidden' name='old_project_id' value='<?php echo $project_id?>'>
  <div class='title'>Change Project
    <div class='titlebar-button close' onclick="killOverlay( event );"></div>
  </div>
  <table style='vertical-align: top;'>
    <tr>
      <td>
        <div style='position: relative;'>
          <select id='project_id' name='project_id' style='background-image: none; height: 200px;' onchange="editComponent(this.value);" size=10 required>
<?php echo $options;?>
          </select>
          <label class='tag' for='project_id'>Projects</label>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <input class='buttons' type='submit' value='Save'>
      </td>
    </tr>
  </table>
</form>
