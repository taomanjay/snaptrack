<?php
require_once( '../inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 2 ) {
	header("HTTP/1.1 500 Internal Server Error");
	echo "Insufficient privileges";

	die();
	}

if ( isset( $_GET['project_id'] ) ) {
	$project_id = $_GET['project_id'];
	$project = getproject( $project_id );
	$project_name = $project['project_name'];
	$admins = getprojectadmins( $project_id );
	$project_admins = '';
	$allusers = getusers( NULL, NULL, NULL );
	$users = '';
	$allgroups = getgroups( NULL, NULL, NULL );
	$groups = '';
	$ingroups = getprojectgroups( $project_id );
	$project_groups = '';

	if ( isset( $project[0] ) && $project[0] === -1 ) {
		header("HTTP/1.1 500 Internal Server Error");
		echo "In getproject(): {$project[1]}";

		die();
		}
	elseif ( isset( $allusers[0] ) && $allusers[0] === -1 ) {
		header("HTTP/1.1 500 Internal Server Error");
		echo "In getusers(): {$allusers[1]}";

		die();
		}
	elseif ( isset( $allgroups[0] ) && $allgroups[0] === -1 ) {
		header("HTTP/1.1 500 Internal Server Error");
		echo "In getgroups(): {$allgroups[1]}";

		die();
		}
	elseif ( isset( $ingroups[0] ) && $ingroups[0] === -1 ) {
		header("HTTP/1.1 500 Internal Server Error");
		echo "In getprojectgroups(): {$ingroups[1]}";

		die();
		}

	foreach ( $allusers as $user ) {
		foreach ( $admins as $admin ) {
			if ( $user['user_id'] === $admin['user_id'] ) {
				$project_admins .= "<option value="
					. "{$user['user_id']}>"
					. "{$user['user_name']}</option>\n";

				continue( 2 );
				}
			}

		$users .= "<option value={$user['user_id']}>"
			. "{$user['user_name']}</option>\n";
		}

	foreach ( $allgroups as $group ) {
		foreach ( $ingroups as $ingroup ) {
			if ( $group['group_id'] == $ingroup['group_id'] ) {
				$project_groups .= "<option value="
					. "{$group['group_id']}>"
					. "{$group['group_name']}</option>\n";

				continue( 2 );
				}
			}

		$groups .= "<option value={$group['group_id']}>"
			. "{$group['group_name']}</option>\n";
		}
	}
else {
	header("HTTP/1.1 500 Internal Server Error");
	echo "In editproject.php: project_id not supplied";

	die();
	}
?>

<form method='post' id='editproject'>
    <div style='width: 0px; height: 0px; overflow: hidden;' onclick="selectAll('editproject_project_admins');selectAll('editproject_project_groups');">
      <input type='submit' value='Save'>
    </div>
  <input type='hidden' name='form' value='editproject'>
  <input type='hidden' name='project_id' value='<?php echo $project_id?>'>
  <div class='title'>Edit Project
    <div class='titlebar-button close' onclick="killOverlay( event );"></div>
  </div>
  <table style='vertical-align: top;'>
    <tr>
      <td colspan=2>
        <div style='position: relative;'>
          <input id='editproject_project_name' type='text' name='project_name' value='<?php echo $project_name;?>' autofocus style='width: 440px;'>
          <label class='tag' for='editproject_project_name' style='width: 458px;'>Project Name</label>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div style='position: relative;'>
          <select id='editproject_admins' name='editproject_admins' multiple onchange="moveTo(this,'editproject_project_admins');" style='height: 200px;'>
<?php echo $users;?>
          </select>
          <label class='tag' for='editproject_admins'>Non-admins</label>
        </div>
      </td>
      <td>
        <div style='position: relative;'>
          <select id='editproject_project_admins' name='admins[]' multiple onchange="moveTo(this,'editproject_admins');" style='height: 200px;'>
<?php echo $project_admins;?>
          </select>
          <label class='tag' for='editproject_project_admins'>Admins</label>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div style='position: relative;'>
          <select id='editproject_groups' name='editproject_groups' multiple onchange="moveTo(this,'editproject_project_groups');" style='height: 200px;'>
<?php echo $groups;?>
          </select>
          <label class='tag' for='editproject_groups'>Unassigned Groups</label>
        </div>
      </td>
      <td>
        <div style='position: relative;'>
          <select id='editproject_project_groups' name='groups[]' multiple onchange="moveTo(this,'editproject_groups');" style='height: 200px;'>
<?php echo $project_groups;?>
          </select>
          <label class='tag' for='editproject_project_groups'>Assigned Groups</label>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <input class='buttons delete' name='delete' type='submit' value='Delete project'>
      </td>
      <td>
        <input class='buttons' type='submit' value='Save' onclick="selectAll('editproject_project_admins');selectAll('editproject_project_groups');">
      </td>
    </tr>
  </table>
</form>
