<?php
require_once( '../inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 2 ) {
	header("HTTP/1.1 500 Internal Server Error");
	echo "Insufficient privileges";

	die();
	}

if ( isset( $_GET['user_id'] ) ) {
	$user_id = $_GET['user_id'];
	$user = getuser( $user_id );
	}
else {
	header("HTTP/1.1 500 Internal Server Error");
	echo "Failed to provide user id";

	die();
	}
?>
  <form method='post' id='edituser'>
    <div style='width: 0px; height: 0px; overflow: hidden;'>
      <input type='submit' value='Save'>
    </div>
    <input type='hidden' name='form' value='edituser'>
    <input type='hidden' name='user_id' value='<?php echo $user_id?>'>
    <div class='title'>Edit User
      <div class='titlebar-button close' onclick="killOverlay( event, 'newuser' );"></div>
    </div>
    <table style='vertical-align: top;'>
      <tr>
        <td colspan=2>
          <div style='position: relative;'>
            <input type='text' id='user_name' name='user_name' tabindex=1 autofocus value='<?php echo $user['user_name'];?>' style='width: 440px;'>
            <label class='tag' for='user_name' style='width: 458px;'>Username</label>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div style='position: relative;'>
            <input type='text' id='first_name' name='first_name' tabindex=2 value='<?php echo $user['first_name'];?>'>
            <label class='tag' for='first_name'>First name</label>
          </div>
        </td>
        <td>
          <div style='position: relative;'>
            <input type='text' id='email' name='email' tabindex=5 value='<?php echo $user['email'];?>'>
            <label class='tag' for='email'>Email</label>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div style='position: relative;'>
            <input type='text' id='surname' name='surname' tabindex=3 value='<?php echo $user['surname'];?>'>
            <label class='tag' for='surname'>Surname</label>
          </div>
        </td>
        <td>
          <div style='position: relative;'>
            <input type='password' id='password' name='password' tabindex=6>
            <label class='tag' for='password'>Password</label>
          </div>
        </td>
      </tr>
      <tr>
        <td style='padding-top: 17px;'>
          <div style='position: relative;'>
            <select id='priv_id' name='priv_id' tabindex=4>
<?php
if ( $user['priv_id'] == 1 ) {
        echo "              <option value=1 selected>User</option>\n"
                . "              <option value=2>Admin</option>\n";
        }
elseif ( $user['priv_id'] == 2 ) {
        echo "              <option value=1>User</option>\n"
                . "              <option value=2 selected>Admin</option>\n";
        }
?>
            </select>
            <label class='tag' for='priv_id'>Privilege</label>
          </div>
        </td>
        <td>
          <div style='position: relative;'>
            <input type='password' id='verify_password' name='verify_password' tabindex=7>
            <label class='tag' for='verify_password'>Verify password</label>
          </div>
        </td>
      </tr>
        <td>
          <div style='position: relative;'>
            <input class='buttons delete' name='delete' type='submit' value='Delete user' tabindex=9>
          </div>
        </td>
        <td>
          <div style='position: relative;'>
            <input class='buttons' type='submit' value='Save' tabindex=8>
          </div>
        </td>
      </tr>
    </table>
  </form>
