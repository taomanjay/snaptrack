<?php
require_once( '../inc/session.php' );

if ( ! isset( $_GET['ticket_id'] ) ) {
	header("HTTP/1.1 500 Internal Server Error");
	die( "In ticketcomponent.php: Failed to supply ticket id" );
	}
else {
	$ticket_id = $_GET['ticket_id'];
	}

if ( ! isset( $_GET['project_id'] ) ) {
	header("HTTP/1.1 500 Internal Server Error");
	die( "In ticketcomponent.php: Failed to supply project id" );
	}
else {
	$project_id = $_GET['project_id'];
	}

if ( ! isset( $_GET['component_id'] ) ) {
	header("HTTP/1.1 500 Internal Server Error");
	die( "In ticketcomponent.php: Failed to supply component id" );
	}
else {
	$component_id = $_GET['component_id'];
	}

if ( ! icanhaz( $ticket_id ) ) {
	header("HTTP/1.1 401 Unauthorized");
	die( 'You do not have permission to modify this' );
	}

$project = getproject( $project_id );
$components = getcomponents( $project_id );

if ( isset( $project[0] ) && $project[0] === -1 ) {
	header("HTTP/1.1 500 Internal Server Error");
	die( "In getproject(): {$project[1]}" );
	}
elseif ( isset( $components[0] ) && $components[0] === -1 ) {
	header("HTTP/1.1 500 Internal Server Error");
	die( "In getcomponents(): {$components[1]}" );
	}

$options = '';

foreach ( $components as $component ) {
	$component_users = split( ',', $component['user_ids'] );
	$component_groups = split( ',', $component['group_ids'] );
	$options .= "<option value="
		. "{$component['component_id']}";

	if ( $component['component_id'] == $component_id ) {
		$options .= ' selected';
		}

	$options .= ">{$component['component_name']}</option>\n";
	}
?>

<form method='post' id='ticketcomponent'>
  <input type='hidden' name='form' value='ticketcomponent'>
  <input type='hidden' name='ticket_id' value='<?php echo $ticket_id?>'>
  <input type='hidden' name='project_id' value='<?php echo $project_id?>'>
  <input type='hidden' name='old_component_id' value='<?php echo $component_id?>'>
  <div class='title'>Change Component
    <div class='titlebar-button close' onclick="killOverlay( event );"></div>
  </div>
  <table style='vertical-align: top;'>
    <tr>
      <td>
        <div style='position: relative;'>
          <select id='component_id' name='component_id' style='background-image: none; height: 200px;' onchange="editComponent(this.value);" size=10 required>
<?php echo $options;?>
          </select>
          <label class='tag' for='component_id'>Components</label>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <input class='buttons' type='submit' value='Save'>
      </td>
    </tr>
  </table>
</form>
