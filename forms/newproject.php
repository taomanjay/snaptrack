<?php
require_once( '../inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 2 ) {
	header("HTTP/1.1 500 Internal Server Error");
	echo "In newgroup.php: Insufficient privileges";

	die();
	}

if ( preg_match( '/newproject.php$/', $_SERVER['SCRIPT_FILENAME'] ) ) {
	$class = '';
	}
else {
	$class = 'hidden';
	}
?>
  <form method='post' id='newproject' class='<?php echo $class?>'>
    <input type='hidden' name='form' value='newproject'>
    <div class='title'>New Project
      <div class='titlebar-button close' onclick="killOverlay( event );"></div>
    </div>
    <table style='vertical-align: top;'>
      <tr>
        <td>
          <div style='position: relative;'>
            <input type='text' id='project_name' name='project_name' autofocus>
            <label class='tag' for='project_name'>Project Name</label>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <input class='buttons' type='submit' value='Add project'>
        </td>
      </tr>
    </table>
  </form>
