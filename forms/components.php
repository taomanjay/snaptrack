<?php
require_once( '../inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 2 ) {
	return( array( -1, 'Insufficient privileges' ) );
	}

if ( isset( $_GET['project_id'] ) ) {
	$project_id = $_GET['project_id'];
	$project = getproject( $project_id );
	$project_name = $project['project_name'];
	$components = getcomponents( $project_id );
	$project_components = '';
	$allusers = getusers( NULL, NULL, NULL );
	$users = '';
	$allgroups = getgroups( NULL, NULL, NULL );
	$groups = '';

	if ( isset( $project[0] ) && $project[0] === -1 ) {
		header("HTTP/1.1 500 Internal Server Error");
		echo "In getproject(): {$project[1]}";

		die();
		}
	elseif ( isset( $components[0] ) && $components[0] === -1 ) {
		header("HTTP/1.1 500 Internal Server Error");
		echo "In getcomponents(): {$components[1]}";

		die();
		}
	elseif ( isset( $allusers[0] ) && $allusers[0] === -1 ) {
		header("HTTP/1.1 500 Internal Server Error");
		echo "In getusers(): {$allusers[1]}";

		die();
		}
	elseif ( isset( $allgroups[0] ) && $allgroups[0] === -1 ) {
		header("HTTP/1.1 500 Internal Server Error");
		echo "In getgroups(): {$allgroups[1]}";

		die();
		}

	foreach ( $components as $component ) {
		$component_users = split( ',', $component['user_ids'] );
		$component_groups = split( ',', $component['group_ids'] );
		$project_components .= "<option value="
			. "{$component['component_id']}>"
			. "{$component['component_name']}</option>\n";
		}
	}
else {
	header("HTTP/1.1 500 Internal Server Error");
	echo "In editproject.php: project_id not supplied";

	die();
	}
?>

<form method='post' id='editcomponents'>
<?php
echo "<div id='component_elements' style='display: none;'>\n";

foreach ( $components as $component ) {
	$component_users = split( ',', $component['user_ids'] );
	$component_groups = split( ',', $component['group_ids'] );
	$members = '';
	$ingroups = '';

	echo "<div id=nonmembers_{$component['component_id']}"
		. " style='position: relative;'>\n"
		. "<select id='editcomponent_users_"
		. "{$component['component_id']}'"
		. " name='editcomponent_users_"
		. "{$component['component_id']}'"
		. " multiple onchange=\"moveTo(this,"
		. "'editcomponent_component_users_"
		. "{$component['component_id']}');\""
		. " style='height: 200px;'>\n";

	foreach ( $allusers as $user ) {
		foreach ( $component_users as $component_user ) {
			if ( $component_user == $user['user_id'] ) {
				$members .= "<option value="
					. "{$user['user_id']}>"
			 		. "{$user['user_name']}"
					. "</option>\n";

				continue( 2 );
				}
			}

		echo "<option value={$user['user_id']}>"
			. "{$user['user_name']}</option>\n";
		}

	echo "</select>\n"
		. "<label class='tag' for='editcomponent_users_"
		. "{$component['component_id']}'>"
		. "Non-members</label>\n</div>\n";

	echo "<div id=members_{$component['component_id']}"
		. " style='position: relative;'>\n"
		. "<select id='editcomponent_component_users_"
		. "{$component['component_id']}'"
		. " name='users_"
		. "{$component['component_id']}[]'"
		. " multiple onchange=\"moveTo(this,"
		. "'editcomponent_users_"
		. "{$component['component_id']}');\""
		. " style='height: 200px;'>\n"
		. $members
		. "</select>\n"
		. "<label class='tag' for='editcomponent_component_users_"
		. "{$component['component_id']}'>"
		. "Members</label>\n</div>\n";

	echo "<div id=groups_{$component['component_id']}"
		. " style='position: relative;'>\n"
		. "<select id='editcomponent_groups_"
		. "{$component['component_id']}'"
		. " name='editcomponent_groups_"
		. "{$component['component_id']}'"
		. " multiple onchange=\"moveTo(this,"
		. "'editcomponent_component_groups_"
		. "{$component['component_id']}');\""
		. " style='height: 200px;'>\n";

	foreach ( $allgroups as $group ) {
		foreach ( $component_groups as $component_group ) {
			if ( $component_group == $group['group_id'] ) {
				$ingroups .= "<option value="
					. "{$group['group_id']}>"
					. "{$group['group_name']}"
					. "</option>\n";

				continue( 2 );
				}
			}

		echo "<option value={$group['group_id']}>"
			. "{$group['group_name']}</option>\n";
		}

	echo "</select>\n"
		. "<label class='tag' for='editcomponent_groups_"
		. "{$component['component_id']}'>"
		. "Unassigned Groups</label>\n</div>\n";

	echo "<div id=ingroups_{$component['component_id']}"
		. " style='position: relative;'>\n"
		. "<select id='editcomponent_component_groups_"
		. "{$component['component_id']}'"
		. " name='groups_"
		. "{$component['component_id']}[]'"
		. " multiple onchange=\"moveTo(this,"
		. "'editcomponent_groups_"
		. "{$component['component_id']}');\""
		. " style='height: 200px;'>\n"
		. $ingroups
		. "</select>\n"
		. "<label class='tag' for='editcomponent_component_groups_"
		. "{$component['component_id']}'>"
		. "Assigned Groups</label>\n</div>\n";
	}

echo "    <div id='nonmembers_new' style='position: relative;'>\n"
	. "      <select id='editcomponent_users_new'"
	. " name='editcomponent_users_new' multiple"
	. " onchange=\"moveTo(this,'editcomponent_component_users_new');\""
	. " style='height: 200px;'>\n";

foreach ( $allusers as $user ) {
	echo "<option value={$user['user_id']}>"
		. "{$user['user_name']}</option>\n";
	}

echo "      </select>\n"
	. "      <label class='tag' for='editcomponent_users_new'>"
	. "Non-members</label>\n"
	. "    </div>\n"
	. "    <div id='members_new' style='position: relative;'>\n"
	. "      <select id='editcomponent_component_users_new'"
	. " name='users_new[]' multiple onchange=\"moveTo(this,"
	. "'editcomponent_users_new');\" style='height: 200px;'>"
	. "      </select>\n"
	. "      <label class='tag' for='editcomponent_component_users_new'>"
	. "Members</label>\n"
	. "    </div>\n"
	. "    <div id='groups_new' style='position: relative;'>\n"
	. "      <select id='editcomponent_groups_new'"
	. " name='editcomponent_groups_new' multiple"
	. " onchange=\"moveTo(this,'editcomponent_component_groups_new');\""
	. " style='height: 200px;'>\n";

foreach ( $allgroups as $group ) {
	echo "<option value={$group['group_id']}>"
		. "{$group['group_name']}</option>\n";
	}

echo "      </select>\n"
	. "      <label class='tag' for='editcomponent_groups_new'>"
	. "Unassigned Groups</label>\n"
	. "    </div>\n"
	. "    <div id='ingroups_new' style='position: relative;'>\n"
	. "      <select id='editcomponent_component_groups_new'"
	. " name='groups_new[]' multiple onchange=\"moveTo(this,"
	. "'editcomponent_groups_new');\" style='height: 200px;'>"
	. "      </select>\n"
	. "      <label class='tag' for='editcomponent_component_groups_new'>"
	. "Assigned Groups</label>\n"
	. "    </div>\n"
	. "  </div>\n";
?>
  <input type='hidden' name='form' value='editcomponents'>
  <input type='hidden' name='project_id' value='<?php echo $project_id?>'>
  <div class='title'>Edit Components (for <?php echo $project_name;?>)
    <div class='titlebar-button close' onclick="killOverlay( event );"></div>
  </div>
  <table style='vertical-align: top;'>
    <tr>
      <td>
        <div style='position: relative;'>
          <textarea id='new_component' name='new_component' style='height: 128px;' autofocus></textarea>
          <label class='tag' for='new_component'>New Component(s)</label>
          <input class='buttons' type='button' value='Add component(s)' style='margin-top: 18px;' onclick="addComponent('new_component','components');">
        </div>
      </td>
      <td rowspan=3 style='width: 1px; background-color: #BDC4C9;'></td>
      <td id='container_editcomponent_users' style='width: 220px;'>
      <div id='helptext' style='position: absolute; top: 300px; left: 430px; font-size: 16px; font-weight: bold;'>Select a component</div>
      </td>
      <td id='container_editcomponent_component_users'>
      </td>
    </tr>
    <tr>
      <td>
        <div style='position: relative;'>
          <select id='components' name='components[]' style='background-image: none; height: 200px;' onchange="editComponent(this.value);" size=10>
<?php echo $project_components;?>
          </select>
          <label class='tag' for='components'>Components</label>
        </div>
      </td>
      <td id='container_editcomponent_groups'>
      </td>
      <td id='container_editcomponent_component_groups'>
      </td>
    </tr>
    <tr>
      <td style='padding: 0px;'>
        <input class='buttons delete' type='button' value='Remove component' onclick="rmComponent('components');">
      </td>
      <td></td>
      <td>
        <input class='buttons' type='submit' value='Save' onclick="selectAll('editcomponents');">
      </td>
    </tr>
  </table>
</form>
