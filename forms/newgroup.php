<?php
require_once( '../inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 2 ) {
	header( "HTTP/1.1 401 Internal Server Error" );
	die( "In newgroup.php: Insufficient privileges" );
	}

if ( preg_match( '/newgroup.php$/', $_SERVER['SCRIPT_FILENAME'] ) ) {
	$class = '';
	}
else {
	$class = 'hidden';
	}
?>
  <form method='post' id='newgroup' class='<?php echo $class?>'>
    <input type='hidden' name='form' value='newgroup'>
    <div class='title'>New Group
      <div class='titlebar-button close' onclick="killOverlay( event );"></div>
    </div>
    <table style='vertical-align: top;'>
      <tr>
        <td>
          <div style='position: relative;'>
            <input type='text' id='group_name' name='group_name' autofocus>
            <label class='tag' for='group_name'>Group Name</label>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <input class='buttons' type='submit' value='Add group'>
        </td>
      </tr>
    </table>
  </form>
