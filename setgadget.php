<?php
include( 'inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 1 ) {
	header( "HTTP/1.1 500 Internal Server Error" );
	die( "Insufficient privileges" );
	}

$gadget = json_decode( file_get_contents( 'php://input' ), TRUE );
$status = setgadget( $gadget );

if ( isset( $status[0] ) && $status[0] === -1 ) {
	header( "HTTP/1.1 500 Internal Server Error" );
	die( "In setgadget(): {$status[1]}" );
	}
?>
