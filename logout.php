<?php
include( 'inc/session.php' );

unset( $_SESSION );
session_unset();
session_destroy();
setcookie( 'PHPSESSID', '', 1 );
session_start();
session_regenerate_id( TRUE );

header( "Location: index.php" );
?>
