var gadgets = new Array();
var timeout = 5000;

function drawForm( formId, getvars ) {
	var uri = 'forms/' + formId + '.php' + getvars;
	var overlay = document.createElement( 'div' );
	overlay.id = 'overlay';
	document.body.appendChild( overlay );
	overlay.style.display = 'block';

	getHTTP( uri, overlay );

	window.setTimeout( function() {
		overlay.style.opacity = '1';
		document.body.className = 'noscroll';
		document.getElementById('header').className = 'blur';
		document.getElementById('content').className = 'blur';
		}, 100 );

/* Might just remove this to prevent confusion on forms vanishing
	overlay.onclick = function( e ) {
		if ( e.target.id === 'overlay' ) {
			killOverlay( e, formId );
			}
		};
 */
	}

function autoFocus( el ) {
	var inputs = el.getElementsByTagName( 'input' );

	for ( var i = 0; i < inputs.length; i++ ) {
		if ( inputs[i].autofocus === true ) {
			inputs[i].select();
			inputs[i].focus();

			break;
			}
		}
	}

function drawOverlay( popupID ) {
	var popup = document.getElementById( popupID );
	var overlay = document.createElement( 'div' );
	overlay.id = 'overlay';
	document.body.appendChild( overlay );
	overlay.style.display = 'block';

	window.setTimeout( function() {
		var inputs = popup.getElementsByTagName('input');
		overlay.appendChild( popup );
		popup.style.display = 'table';
		overlay.style.opacity = '1';
		popup.style.opacity = '1';

		document.body.className = 'noscroll';
		document.getElementById('header').className = 'blur';
		document.getElementById('content').className = 'blur';

		for ( var i = 0; i < inputs.length; i++ ) {
			if ( inputs[i].autofocus === true ) {
				inputs[i].select();
				inputs[i].focus();

				break;
				}
			}
		}, 100 );

	overlay.onclick = function( e ) {
		if ( e.target.id === 'overlay' ) {
			killOverlay( e, popupID );
			}
		};

	}

function killOverlay( e, popupID ) {
	if ( e != null && e.target.id !== 'overlay'
	&& ! hasClass( e.target, 'close' ) ) {
		return;
		}

	var overlay = document.getElementById( 'overlay' );

	overlay.onclick = null;

	window.setTimeout( function() {
		document.body.className = '';
		document.getElementById('header').className = '';
		document.getElementById('content').className = '';

		overlay.style.opacity = '0';
		}, 50 );

	window.setTimeout( function() {
		overlay = document.getElementById( 'overlay' );
		document.body.removeChild( overlay );
		}, 600 );
	}

function getHTTP( uri, el ) {
	var req = new XMLHttpRequest();
	req.timeout = timeout;
	req.ontimeout = function() {
		showError( 'getHTTP()', 'Request timed out to ' + uri );
		};

	req.onreadystatechange = function() {
		if ( el == null ) {
			return;
			}

		if ( req.readyState === 4 && req.status === 200 ) {
			var throbber = document.getElementById( 'throbber' );

			if ( req.responseURL.slice( -9 ) === 'login.php' ) {
				window.location.href = req.responseURL;
				}

			if ( throbber ) {
				window.setTimeout( function() {
					throbber.style.opacity = '0';
					}, 20 );
				window.setTimeout( function() {
					el.innerHTML = req.responseText;
					autoFocus( el );
					}, 200 );
				}
			else {
				el.innerHTML = req.responseText;
				autoFocus( el );
				}
			}
		else if ( req.readyState === 4 && req.status !== 0 ) {
			showError( 'getHTTP()', req.statusText
				+ '<br><br>\n' + req.responseText );
			}
		}

	req.open( 'GET', uri );
	req.send();
	}

function postHTTP( uri, data ) {
	var req = new XMLHttpRequest();

	req.onreadystatechange = function() {
		if ( req.readyState === 4 && req.status !== 200 ) {
			showError( 'postHTTP()', req.statusText
				+ '<br><br>\n' + req.responseText );
			}
		}

	req.open('POST', uri);
	req.setRequestHeader('Content-type', 'text/plain');
	req.send(data);
	}

function showError( func, errstr ) {
	var overlay = document.getElementById( 'overlay' );
	var form = document.createElement( 'form' );
	var title = document.createElement( 'div' );
	var p = document.createElement( 'p' );
	var close = document.createElement( 'input' );
	form.method = 'post';
	form.className = 'error';
	title.className = 'title';
	title.innerHTML = 'ERROR: ' + func;
	p.innerHTML = errstr;
	close.type = 'submit';
	close.className = 'buttons close';
	close.value = 'OK';
//	close.onclick = function() { killOverlay(); }

	if ( overlay == null ) {
		overlay = document.createElement( 'div' );
		overlay.id = 'overlay';
		document.body.appendChild( overlay );
		overlay.style.display = 'block';
		}
	else {
		while ( overlay.firstChild ) {
			overlay.removeChild( overlay.firstChild );
			}
		}

	window.setTimeout( function() {
		overlay.style.opacity = '1';
		overlay.appendChild( form );
		form.appendChild( title );
		form.appendChild( p );
		form.appendChild( close );
		document.body.className = 'noscroll';
		document.getElementById('header').className = 'blur';
		document.getElementById('content').className = 'blur';
		}, 100 );

	overlay.onclick = function( e ) {
		if ( e.target.id === 'overlay' ) {
			killOverlay( e );
			}
		};
	}

function updateGadget( name, httpvars ) {
	var gadget = document.getElementById( name );
	var children = gadget.children;
	var gadgetcontent;
	var throbber;
	var req = new XMLHttpRequest();
	req.timeout = timeout;

	for ( var i = 0; i < children.length; i++ ) {
		if ( children[i].className === 'gadgetcontent' ) {
			gadgetcontent = children[i];
			}
		}

	req.ontimeout = function() {
		gadgetcontent.innerHTML = "<div class='gadgeterror'>"
			+ 'ERROR: getHTTP()<br><br>\n'
			+ 'Request timed out';
		};

	children = gadgetcontent.children;

	for ( var i = 0; i < children.length; i++ ) {
		if ( children[i].id === 'throbber' ) {
			throbber = children[i];
			}
		}

	if ( ! throbber && httpvars ) {
		var overlay = document.createElement( 'div' );
		gadgetcontent.style.position = 'relative';
		overlay.id = 'throbber';
		overlay.style.width = '100%';
		overlay.style.height = '100%';
		overlay.style.position = 'absolute';
		overlay.style.top = '0px';
		overlay.style.backgroundColor = 'rgba(0,0,0,0)';
		overlay.style.backgroundImage = 'url(images/throbber.gif)';
		overlay.style.backgroundRepeat = 'no-repeat';
		overlay.style.backgroundPosition = 'center';
		overlay.style.opacity = '0';
		overlay.style.transition = 'background-color 0.5s,opacity 0.5s';

		gadgetcontent.appendChild( overlay );
		}

	if ( httpvars ) {
		gadgets[name].httpvars = httpvars;
		postHTTP( 'setgadget.php', JSON.stringify( gadgets[name] ) );
		}

	req.onreadystatechange = function() {
		var gadget = document.getElementById( name );
		var children = gadget.children;
		var content;

		for ( var i = 0; i < children.length; i++ ) {
			if ( children[i].className
			=== 'gadgetcontent' ) {
				content = gadget.children[i];

				break;
				}
			}

		if ( req.readyState === 4 && req.status === 200 ) {
			var buffer = document.createElement( 'div' );
			buffer.innerHTML = req.responseText;

			if ( content.innerHTML === buffer.innerHTML ) {
				delete buffer;

				return;
				}

			content.innerHTML = buffer.innerHTML;
			delete buffer;

			}
		else if ( req.readyState === 4 && req.status !== 0 ) {
			content.innerHTML = "<div class='gadgeterror'>"
				+ 'updateGadget(): ' + req.statusText
				+ '<br><br>\n' + req.responseText
				+ '</div>\n';
			}
		}

	req.open( 'GET', gadgets[name].uri + gadgets[name].httpvars );
	req.send();

	if ( ! throbber && httpvars ) {
		window.setTimeout( function() {
			overlay.style.opacity = '1';
			overlay.style.backgroundColor = 'rgba(0,0,0,0.6)';
			}, 200 );
		}
	}

function delGadget( name ) {
	var gadget = document.getElementById( name );
	var gadget_id = gadgets[name].gadget_id;
	var parentNode = gadget.parentNode;
	var uri = 'rmgadget.php?gadget_id=' + gadget_id;
	var leftcolset = document.getElementById( 'leftcolset' );
	var rightcolset = document.getElementById( 'rightcolset' );

	clearInterval( gadgets[gadget.id].interval );
	delete( gadgets[gadget.id] );

	getHTTP( uri, null );
	parentNode.removeChild( gadget );

	if ( leftcolset.checked === true ) {
		getHTTP( 'gadgets/gadgets.php?container=leftcol',
			document.getElementById( 'selectleft' ) );
		}
	if ( rightcolset.checked === true ) {
		getHTTP( 'gadgets/gadgets.php?container=rightcol',
			document.getElementById( 'selectright' ) );
		}
	}

function hide( toggle, element ) {
	var el = document.getElementById( element );

	if ( el.className.indexOf( 'hidden' ) === -1 ) {
		window.setTimeout( function() {
			toggle.className = toggle.className + ' on';
			el.className = el.className + ' hidden';
			}, 50 );
		}
	else {
		window.setTimeout( function() {
			toggle.className =
				toggle.className.replace( ' on', '' );
			el.className = el.className.replace( ' hidden', '' );
			}, 50 );
		}
	}

function search ( toggle, element ) {
	var el = document.getElementById( element );
	var clear = el.nextSibling;

	clear.onclick = function() {
		el.value = '';
		}

	if ( el.className !== 'on' ) {
		window.setTimeout( function() {
			el.className = 'on';
			clear.className = 'clear on'
			}, 50 );
		window.setTimeout( function() {
			el.focus();
			}, 100 );
		}
	else {
		window.setTimeout( function() {
			el.className = '';
			clear.className = 'clear';
			}, 50 );
		}
	}

function reply( toggle, element ) {
	var el = document.getElementById( element );

	if ( el.className.indexOf( 'open' ) === -1 ) {
		window.setTimeout( function() {
			el.className = el.className + ' open';
			}, 100 );
		}
	else {
		window.setTimeout( function() {
			var note = el.getElementsByTagName( 'TEXTAREA' );
			el.className = el.className.replace( ' open', '' );

			for ( var i = 0; i < note.length; i++ ) {
				note[i].value = '';
				}
			}, 100 );
		}
	}

function hasClass( el, str ) {
	var classList = el.className.split( ' ' );

	for ( i = 0; i < classList.length; i++ ) {
		if ( classList[i] === str ) {
			return true;
			}
		}

	return false;
	}

function getSelectHTTP( uri, el ) {
	var req = new XMLHttpRequest();

	req.onreadystatechange = function() {
		if ( el == null ) {
			return;
			}

		if ( req.readyState === 4 && req.status === 200 ) {
			var options = JSON.parse( req.responseText );

			while ( el.firstChild ) {
				el.removeChild( el.firstChild );
				}

			for ( var i = 0; i < options.length; i++ ) {
				var opt = document.createElement( 'option' );
				opt.value = options[i].component_id;
				opt.innerHTML = options[i].component_name;
				el.appendChild( opt );
				}
			}
		else if ( req.readyState === 4 ) {
			el.innerHTML = "<div>ERROR: " + req.status
				+ "<br>" + req.statusText
				+ '</div>';
			}
		};

	req.open( 'GET', uri );
	req.send();
	}

function moveTo( source, dest ) {
	var destination = document.getElementById( dest );
	var tmp = new Array();

	for ( var i = source.length - 1; i >= 0; i-- ) {
		if ( source[i].selected === true ) {
			source[i].selected = false;
			tmp.push( [source[i].text,source[i].value] );
			source.removeChild( source[i] );
			}
		}

	for ( var i = 0; i < destination.length; i++ ) {
		tmp.push( [destination[i].text, destination[i].value] );
		}

	tmp.sort( function( a, b ) {
		return a[0].toLowerCase().localeCompare( b[0].toLowerCase() );
		} );

	while ( destination.firstChild ) {
		destination.removeChild( destination.firstChild );
		}

	for ( var i = 0; i < tmp.length; i++ ) {
		var option = document.createElement( 'option' );
		option.text = tmp[i][0];
		option.value = tmp[i][1];
		destination.appendChild( option );
		}
	}

function selectAll( target ) {
	var el = document.getElementById( target );

	if ( el.nodeName === 'SELECT' ) {
		el.multiple = true;

		for ( var i = 0; i < el.length; i++ ) {
			el[i].selected = true;
			}
		}
	else {
		var elements = el.getElementsByTagName( 'SELECT' );

		for ( var i = 0; i < elements.length; i++ ) {
			elements[i].multiple = true;

			for ( var j = 0; j < elements[i].length; j++ ) {
				elements[i][j].selected = true;
				}
			}
		}
	}

function selectOne( el, target ) {
	for ( i = 0; i < el.length; i++ ) {
		el[i].selected = false;
		}

	el[target].selected = true;
	}

function addComponent( source, target ) {
	var el = document.getElementById( source );
	var text = el.value;
	var options = text.split( /\s/ );
	var destination = document.getElementById( target );

	options.sort( function( a, b ) {
		return a.toLowerCase().localeCompare( b.toLowerCase() );
		} );

	for ( i = 0; i < options.length; i++ ) {
		if ( options[i] ) {
			var option = document.createElement( 'option' );
			option.text = options[i];
			option.value = options[i];
			var container = document.getElementById(
				'component_elements' );
			var nonmembers = document.getElementById(
				'nonmembers_new' ).cloneNode( true );
			var members = document.getElementById(
				'members_new' ).cloneNode( true );
			var groups = document.getElementById(
				'groups_new' ).cloneNode( true );
			var ingroups = document.getElementById(
				'ingroups_new' ).cloneNode( true );

			nonmembers.id = 'nonmembers_' + options[i];
			members.id = 'members_' + options[i];
			groups.id = 'groups_' + options[i];
			ingroups.id = 'ingroups_' + options[i];

			nonmembers.innerHTML = nonmembers.innerHTML.replace(
				/_new/g, '_' + options[i] );
			members.innerHTML = members.innerHTML.replace(
				/_new/g, '_' + options[i] );
			groups.innerHTML = groups.innerHTML.replace(
				/_new/g, '_' + options[i] );
			ingroups.innerHTML = ingroups.innerHTML.replace(
				/_new/g, '_' + options[i] );

			container.appendChild( nonmembers );
			container.appendChild( members );
			container.appendChild( groups );
			container.appendChild( ingroups );
			destination.appendChild( option );
			option.selected = true;

			editComponent( options[i] );
			}
		}

	el.value = null;
	}

function rmComponent( element ) {
	var el = document.getElementById( element );

	for ( i = el.length - 1; i >= 0; i-- ) {
		if ( el[i].selected === true ) {
			var component_id = el[i].value;
			var nonmembers = document.getElementById(
				'nonmembers_' + component_id );
			var members = document.getElementById(
				'members_' + component_id );
			var groups = document.getElementById(
				'groups_' + component_id );
			var ingroups = document.getElementById(
				'ingroups_' + component_id );
			var container_users = document.getElementById(
				'container_editcomponent_users' );
			var helptext = document.getElementById( 'helptext' );

			el.removeChild( el[i] );
			nonmembers.parentNode.removeChild( nonmembers );
			members.parentNode.removeChild( members );
			groups.parentNode.removeChild( groups );
			ingroups.parentNode.removeChild( ingroups );

			container_users.appendChild( helptext );
			}
		}
	}

function editComponent( component_id ) {
	var source = document.getElementById( 'component_elements' );
	var nonmembers = document.getElementById(
		'nonmembers_' + component_id );
	var members = document.getElementById( 'members_' + component_id );
	var groups = document.getElementById( 'groups_' + component_id );
	var ingroups = document.getElementById( 'ingroups_' + component_id );
	var container_users = document.getElementById(
		'container_editcomponent_users' );
	var container_component_users = document.getElementById(
		'container_editcomponent_component_users' );
	var container_groups = document.getElementById(
		'container_editcomponent_groups' );
	var container_component_groups = document.getElementById(
		'container_editcomponent_component_groups' );

	while ( container_users.firstChild ) {
		source.appendChild( container_users.firstChild );
		}

	while ( container_component_users.firstChild ) {
		source.appendChild( container_component_users.firstChild );
		}

	while ( container_groups.firstChild ) {
		source.appendChild( container_groups.firstChild );
		}

	while ( container_component_groups.firstChild ) {
		source.appendChild( container_component_groups.firstChild );
		}

	container_users.appendChild( nonmembers );
	container_component_users.appendChild( members );
	container_groups.appendChild( groups );
	container_component_groups.appendChild( ingroups );
	}

function updateAvatar( event, element ) {
	var el = document.getElementById( element );
	var reader = new FileReader();

	reader.onload = function( e ) {
		el.style.backgroundImage = 'url(' + e.target.result +')';
		el.style.backgroundSize = 'cover';
		};

	reader.readAsDataURL( event.target.files[0] );
	}

function searchstring( name, searchstring ) {
	var input = document.getElementById( name + '-search' );
	var httpvars = gadgets[name].httpvars.replace(
		/&searchstring=.*(&|$)|$/, '&searchstring=' + searchstring );

	if ( ! searchstring ) {
		input.value = '';
		}

	updateGadget( name, httpvars );
	}
