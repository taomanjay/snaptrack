<?php
require( 'inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 1 ) {
	header( 'Location: ./' );

	die();
	}

if ( isset( $_GET['ticket_id'] ) ) {
	$ticket_id = $_GET['ticket_id'];
	}
else {
	header( 'Location: ./' );

	die();
	}

include( 'inc/head.php' );

$ticket = getticket( $ticket_id );
$watchers = getwatchers( $ticket_id );
$watching = FALSE;
$icanhaz = icanhaz( $ticket[0]['ticket_id'] );

foreach ( $watchers as $watcher ) {
	if ( $watcher['user_id'] === $_SESSION['user']['user_id'] ) {
		$watching = TRUE;

		break;
		}
	}

echo "<div class='gadget ticket'>
  <div class='title' style='position: relative;'><div class='titletext'>#{$ticket[0]['ticket_id']}"
	. " - {$ticket[0]['title']}</div><div class='ticketbuttons'>\n";

if ( $icanhaz ) {
	if ( $ticket[0]['owner'] === $_SESSION['user']['user_id']
	&& $ticket[0]['status'] < 2 ) {
		echo "    <div class='ticketbutton'"
			. " onclick=\"drawForm('reject',"
			. "'?ticket_id=$ticket_id')\">Reject</div>\n"
			. "    <div class='ticketbutton'"
			. " onclick=\"drawForm('resolve',"
			. "'?ticket_id=$ticket_id')\">Resolve</div>\n";
		}
	}

if ( ! $watching && $ticket[0]['creator']
!== $_SESSION['user']['user_id'] && $ticket[0]['owner']
!== $_SESSION['user']['user_id'] ) {
	echo "    <div class='ticketbutton'>\n"
		. "      <a href='watch.php"
		. "?ticket_id={$ticket[0]['ticket_id']}'>Watch</a>\n"
		. "    </div>\n";
	}
elseif ( $watching ) {
	echo "    <div class='ticketbutton'>\n"
		. "      <a href='ignore.php"
		. "?ticket_id={$ticket[0]['ticket_id']}'>Ignore</a>\n"
		. "    </div>\n";
	}

if ( $icanhaz ) {
	if ( empty( $ticket[0]['owner'] ) && $ticket[0]['status'] < 2 ) {
		echo "    <div class='ticketbutton'>\n"
			. "      <a href='take.php"
			. "?ticket_id={$ticket[0]['ticket_id']}'>Take</a>\n"
			. "    </div>\n";
		}
	elseif ( $ticket[0]['owner'] !== $_SESSION['user']['user_id']
	&& $ticket[0]['status'] < 2 ) {
		echo "    <div class='ticketbutton'>\n"
			. "      <a href='steal.php"
			. "?ticket_id={$ticket[0]['ticket_id']}'>Steal</a>\n"
			. "    </div>\n";
		}
	elseif ( $ticket[0]['status'] >= 2 ) {
		echo "    <div class='ticketbutton'"
			. " onclick=\"drawForm('reopen',"
			. "'?ticket_id=$ticket_id')\">Re-open</div>";
		}
	}

if ( empty( $ticket[0]['component_id'] ) ) {
	$ticket[0]['component'] = 'undefined';
	}

echo "  </div></div>
  <div id='ticketinfo'>
    <div class='gadget'>
      <div class='title'>Ticket Information</div>
      <table>
        <tr>
          <td>Status:</td><td>{$ticket[0]['status_name']}</td>
        </tr>
        <tr>
          <td>Project:</td><td><a href='#' onclick=\"drawForm('ticketproject','?ticket_id=$ticket_id&project_id={$ticket[0]['project_id']}')\">{$ticket[0]['project_name']}</a></td>
        </tr>
        <tr>
          <td>Component:</td><td><a href='#' onclick=\"drawForm('ticketcomponent','?ticket_id=$ticket_id&project_id={$ticket[0]['project_id']}&component_id={$ticket[0]['component_id']}')\">{$ticket[0]['component']}</a></td>
        </tr>
      </table>
    </div>
    <div class='gadget'>
      <div class='title'>Users</div>
      <table>
        <tr>
          <td>Created by:</td><td>{$ticket[0]['creator']}</td>
        </tr>\n";

if ( $ticket[0]['owner_disabled'] != 0 ) {
	$ticket[0]['owner_name'] = "<span class='strike'>"
		. "{$ticket[0]['owner_name']}</span>\n";
	}

echo "        <tr>
          <td>Owner:</td><td>{$ticket[0]['owner_name']}</td>
        </tr>
        <tr>
          <td>Watchers:</td>
          <td>";

$first = TRUE;

foreach ( $watchers as $watcher ) {
	if ( ! $first ) {
		echo ', ';
		}

	echo "{$watcher['user_name']}";

	$first = FALSE;
	}

echo "</td>
        </tr>
      </table>
    </div>
    <div class='gadget'>
      <div class='title'>Dates</div>
      <table>
        <tr>
          <td>Created:</td><td>{$ticket[0]['created_time']}</td>
        </tr>
        <tr>
          <td>Assigned:</td><td>{$ticket[0]['assigned_time']}</td>
        </tr>
        <tr>
          <td>Resolved:</td><td>{$ticket[0]['resolved_time']}</td>
        </tr>
      </table>
    </div>
  </div>
</div>\n";

$level = 0;
ordernotes( $ticket );
nestnotes( $ticket, $level );

include( 'inc/foot.php' );
?>
