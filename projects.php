<?php
require( 'inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 2 ) {
	header( 'Location: ./' );

	die();
	}

$container = 'container';
$gadget_id = 0;

include( 'inc/head.php' );

echo "<div id='container' class='gadget single'>
  <div class='title'>Projects</div>
  <div class='gadgetcontent'>\n";
require( 'gadgets/projects.php' );
echo "  </div>
</div>\n";

require( 'inc/foot.php' );
?>
