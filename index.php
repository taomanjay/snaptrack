<?php
require( 'inc/session.php' );

if ( $_SESSION['user']['user_id'] === 0 ) {
	header( 'Location: ./login.php' );

	die();
	}

$gadgets = getgadgets();

/*
 * This could probably be dealt with better, but for now we
 * use this to set any default gadgets for projects which
 * currently have none for that user.
 */

if ( count( $gadgets ) == 0 ) {
	$gadgets = array( array(
		'project_id' => $_SESSION['project']['project_id'],
		'container' => 'leftcol',
		'row' => 0,
		'id' => 'newtickets',
		'httpvars' => ''
		) );

	$status = setgadgets( $gadgets );

	$gadgets = getgadgets();
	}

$leftcol = '';
$rightcol = '';

foreach ( $gadgets as $gadget ) {
	$uri = "gadgets/{$gadget['gadget']}.php";
	$httpvars = $gadget['httpvars'];
	$searchstring = '';

	preg_match( '/&searchstring=(.*)?(&|$)/', $httpvars, $match );

	if ( ! empty( $match[1] ) ) {
		$searchstring = $match[1];
		$class = 'on';
		}
	else {
		$class = '';
		}

	if ( empty( $gadget['httpvars'] ) ) {
		$httpvars = "?gadget_id={$gadget['gadget_id']}"
			. "&container={$gadget['container']}";
		}

	$gadgethtml = "  <div id='{$gadget['gadget']}' class='gadget'>\n"
		. "    <div class='title'>{$gadget['gadget']}"
		. "<div class='titlebar-button close'"
		. " onclick=\"delGadget( '{$gadget['gadget']}' )\"></div>"
		. "<div class='titlebar-button hide'"
		. " onclick=\"hide( this, '{$gadget['gadget']}' )\"></div>"
		. "<div class='titlebar-button search'"
		. " onclick=\"search( this, '{$gadget['gadget']}-search' )\">"
		. "</div><input id='{$gadget['gadget']}-search'"
		. " type='search' value='$searchstring' class='$class'"
		. " onkeyup=\"searchstring( '{$gadget['gadget']}',"
		. " this.value )\"></input><div class='clear $class'"
		. " onmousedown=\"searchstring( '{$gadget['gadget']}', '' );\">"
		. "</div></div>\n"
		. "    <div class='gadgetcontent'></div>\n"
		. "    <script type='text/javascript'>\n"
		. "      gadgets['{$gadget['gadget']}'] = {\n"
		. "        'gadget_id': '{$gadget['gadget_id']}',\n"
		. "        'project_id': '{$gadget['project_id']}',\n"
		. "        'id': '{$gadget['gadget']}',\n"
		. "        'container': '{$gadget['container']}',\n"
		. "        'row': '{$gadget['row']}',\n"
		. "        'uri': '$uri',\n"
		. "        'httpvars': '$httpvars'\n"
		. "        }\n"
		. "    </script>\n"
		. "  </div>\n";

	if ( $gadget['container'] === 'leftcol' ) {
		$leftcol .= $gadgethtml;
		}
	elseif ( $gadget['container'] === 'rightcol' ) {
		$rightcol .= $gadgethtml;
		}
	}
	
include( 'inc/head.php' );

echo "<div class='gadgets'>
  <div style='float: left; width: 668px;'>
    <div class='selectgadget left'>
      <label for='leftcolset' onclick=\"getHTTP('gadgets/gadgets.php"
	. "?container=leftcol',"
	. "document.getElementById('selectleft'));\"></label>
      <form>
        <input id='leftcolset' type='checkbox'>
        <div id='selectleft' class='gadget'>
        </div>
      </form>
    </div>
  <div id='leftcol' class='column'>$leftcol</div>
  </div>
  <div style='float: right; width: 420px;'>
    <div class='selectgadget right'>
      <label for='rightcolset' onclick=\"getHTTP('gadgets/gadgets.php"
	. "?container=rightcol',"
	. "document.getElementById('selectright'));\"></label>
      <form>
        <input id='rightcolset' type='checkbox'>
        <div id='selectright' class='gadget'>
        </div>
      </form>
    </div>
  <div id='rightcol' class='column'>$rightcol</div>
</div>
</div>
<script src='js/dragula.js'></script>
<script type='text/javascript'>
dragula([document.querySelector('#leftcol'),
	document.querySelector('#rightcol')]);
for ( var name in gadgets ) {
	gadgets[name].interval = setInterval( function( tmp ) {
		updateGadget( tmp );
		}, 5000, name );

	updateGadget( name );
	}
</script>\n";

include( 'inc/foot.php' );
?>
