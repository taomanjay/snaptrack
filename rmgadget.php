<?php
include( 'inc/session.php' );

if ( $_SESSION['user']['user_id'] < 1 ) {
	die();
	}

if ( isset( $_GET['gadget_id'] ) ) {
	$gadget_id = $_GET['gadget_id'];
	}
else {
	die();
	}

$status = rmgadget( $gadget_id );

if ( isset( $status[0] ) && $status[0] !== 0 ) {
	$_SESSION['status'] = array(
		'func' => 'rmgadget()',
		'code' => $status[0],
		'errstr' => $status[1]
		);
	}

header( "Location: index.php" );
/*
	die();
	}
else {
	$_SESSION['status'] = array(
		'func' => 'steal.php',
		'code' => -1,
		'errstr' => '- ticket_id empty'
		);

	header( 'Location: ./' );
	}
*/
?>
