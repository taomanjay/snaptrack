<?php
require( 'inc/config.php' );
require( 'inc/functions.php' );

$step = 1;

if ( isset( $_POST['step2'] ) ){
	$step = 2;
	}
elseif ( isset( $_POST['step3'] ) ) {
	$step = 3;
	}
elseif ( isset( $_POST['step4'] ) ) {
	$step = 4;
	}
elseif ( isset( $_POST['finish'] ) ) {
	header( 'Location: login.php' );
	die( 'wtf' );
	}

/*
 * Just a quick check to see if the db user is defined. If so we'll
 * assume the intention is not to install
 */

if ( ! empty( $conf['db']['user'] ) ) {
	die();
	}

/*
 * This might be done a little better at some point if needed, but
 * it should be fine for now. We store the CREATE TABLE statements
 * for the schema in $tables and simply execute it. This SQL should
 * fail if the tables already exist.
 *
 * For now I think we'll go with enabling this installer if no
 * username is provided for the MySQL user in config.php
 */

$sql = "
CREATE TABLE `component_groups` (
  `component_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`component_id`,`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `component_users` (
  `component_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`component_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `components` (
  `component_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `component_name` varchar(64) NOT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`component_id`),
  UNIQUE KEY `component_name` (`project_id`,`component_name`),
  UNIQUE KEY `project_id` (`project_id`,`component_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `cust_field_links` (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `value` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  KEY `ticket_id` (`ticket_id`),
  KEY `field_id` (`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `cust_fields` (
  `field_id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(32) NOT NULL,
  PRIMARY KEY (`field_id`),
  UNIQUE KEY `field_name` (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `events` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) NOT NULL,
  `event_time` datetime NOT NULL,
  PRIMARY KEY (`event_id`),
  KEY `ticket_id` (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `gadgets` (
  `gadget_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `container` varchar(32) NOT NULL,
  `row` int(11) NOT NULL DEFAULT '0',
  `gadget` varchar(32) NOT NULL,
  `httpvars` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`gadget_id`),
  UNIQUE KEY `user_id` (`user_id`,`project_id`,`container`,`row`),
  UNIQUE KEY `user_id_2` (`user_id`,`project_id`,`gadget`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `group_users` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(64) NOT NULL,
  `disabled` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `group_name` (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `notes` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) NOT NULL,
  `reply_to` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_time` datetime NOT NULL,
  `note` text NOT NULL,
  PRIMARY KEY (`note_id`),
  KEY `ticket_id` (`ticket_id`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `privs` (
  `priv_id` int(11) NOT NULL,
  `priv_name` varchar(16) NOT NULL,
  PRIMARY KEY (`priv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `project_admins` (
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`project_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `project_groups` (
  `project_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`project_id`,`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(64) NOT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_id`),
  UNIQUE KEY `project_name` (`project_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `sessions` (
  `id` varchar(32) NOT NULL,
  `access` int(10) unsigned DEFAULT NULL,
  `data` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;
CREATE TABLE `statuses` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(32) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `tickets` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `component_id` int(11) DEFAULT NULL,
  `title` varchar(512) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `assigned_time` datetime DEFAULT NULL,
  `resolved_time` datetime DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ticket_id`),
  KEY `project_id` (`project_id`),
  KEY `status` (`status`),
  KEY `owner` (`owner`),
  KEY `component_id` (`component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) NOT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `surname` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `priv_id` int(11) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  KEY `priv_id` (`priv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `watchers` (
  `ticket_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`ticket_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE component_groups ADD CONSTRAINT
  `fk_component_groups_components_component_id` FOREIGN KEY (`component_id`)
  REFERENCES `components` (`component_id`);
ALTER TABLE component_groups ADD CONSTRAINT
  `fk_component_groups_groups_group_id` FOREIGN KEY (`group_id`)
  REFERENCES `groups` (`group_id`);
ALTER TABLE component_users ADD CONSTRAINT
  `fk_component_users_components_component_id` FOREIGN KEY (`component_id`)
  REFERENCES `components` (`component_id`);
ALTER TABLE component_users ADD CONSTRAINT
  `fk_component_users_users_user_id` FOREIGN KEY (`user_id`)
  REFERENCES `users` (`user_id`);
ALTER TABLE components ADD CONSTRAINT
  `fk_components_projects_project_id` FOREIGN KEY (`project_id`)
  REFERENCES `projects` (`project_id`);
ALTER TABLE cust_field_links ADD CONSTRAINT
  `fk_cust_field_links_tickets_ticket_id` FOREIGN KEY (`ticket_id`)
  REFERENCES `tickets` (`ticket_id`);
ALTER TABLE cust_field_links ADD CONSTRAINT
  `fk_cust_field_links_cust_fields_field_id` FOREIGN KEY (`field_id`)
  REFERENCES `cust_fields` (`field_id`);
ALTER TABLE events ADD CONSTRAINT
  `fk_events_tickets_ticket_id` FOREIGN KEY (`ticket_id`)
  REFERENCES `tickets` (`ticket_id`);
ALTER TABLE gadgets ADD CONSTRAINT
  `fk_gadgets_users_user_id` FOREIGN KEY (`user_id`)
  REFERENCES `users` (`user_id`);
ALTER TABLE gadgets ADD CONSTRAINT
  `fk_gadgets_projects_project_id` FOREIGN KEY (`project_id`)
  REFERENCES `projects` (`project_id`);
ALTER TABLE group_users ADD CONSTRAINT
  `fk_group_users_groups_group_id` FOREIGN KEY (`group_id`)
  REFERENCES `groups` (`group_id`);
ALTER TABLE group_users ADD CONSTRAINT
  `fk_group_users_users_user_id` FOREIGN KEY (`user_id`)
  REFERENCES `users` (`user_id`);
ALTER TABLE notes ADD CONSTRAINT
  `fk_notes_tickets_ticket_id` FOREIGN KEY (`ticket_id`)
  REFERENCES `tickets` (`ticket_id`);
ALTER TABLE notes ADD CONSTRAINT
  `fk_notes_users_user_id` FOREIGN KEY (`created_by`)
  REFERENCES `users` (`user_id`);
ALTER TABLE project_admins ADD CONSTRAINT
  `fk_project_admins_projects_project_id` FOREIGN KEY (`project_id`)
  REFERENCES `projects` (`project_id`);
ALTER TABLE project_admins ADD CONSTRAINT
  `fk_project_admins_users_user_id` FOREIGN KEY (`user_id`)
  REFERENCES `users` (`user_id`);
ALTER TABLE project_groups ADD CONSTRAINT
  `fk_project_groups_projects_project_id` FOREIGN KEY (`project_id`)
  REFERENCES `projects` (`project_id`);
ALTER TABLE project_groups ADD CONSTRAINT
  `fk_project_groups_groups_group_id` FOREIGN KEY (`group_id`)
  REFERENCES `groups` (`group_id`);
ALTER TABLE tickets ADD CONSTRAINT
  `fk_tickets_projects_project_id` FOREIGN KEY (`project_id`)
  REFERENCES `projects` (`project_id`);
ALTER TABLE tickets ADD CONSTRAINT
  `fk_tickets_users_user_id` FOREIGN KEY (`owner`)
  REFERENCES `users` (`user_id`);
ALTER TABLE tickets ADD CONSTRAINT
  `fk_tickets_components_component_id` FOREIGN KEY (`component_id`)
  REFERENCES `components` (`component_id`);
ALTER TABLE users ADD CONSTRAINT
  `fk_users_privs_priv_id` FOREIGN KEY (`priv_id`)
  REFERENCES `privs` (`priv_id`);
ALTER TABLE watchers ADD CONSTRAINT
  `fk_watchers_tickets_ticket_id` FOREIGN KEY (`ticket_id`)
  REFERENCES `tickets` (`ticket_id`);
ALTER TABLE watchers ADD CONSTRAINT
  `fk_watchers_users_user_id` FOREIGN KEY (`user_id`)
  REFERENCES `users` (`user_id`);
INSERT INTO privs VALUES
  (0,'Anonymous'),
  (1,'User'),
  (2,'Admin');
INSERT INTO statuses VALUES
  (0,'NEW'),
  (1,'ASSIGNED'),
  (2,'RESOLVED'),
  (3,'REJECTED');
INSERT INTO projects VALUES(0,'Default',0)";

require( 'inc/head.php' );

echo "<form id='install' method='post' action=''>\n"
	. "  <div class='title'>Installation</div>\n";

if ( $step === 1 ) {
	echo "  <h2>STEP 1</h2>\n"
		. "  <p>Create the MySQL database:</p>\n"
		. "  <p class='code'>$ mysql -u root -p\n"
		. "Enter password: \n"
		. "mysql> CREATE DATABASE snaptrack;</p>\n"
		. "  <p>Create the MySQL user:</p>\n"
		. "  <p class='code'>mysql> CREATE USER"
		. " 'username'@'localhost' IDENTIFIED BY 'password';</p>\n"
		. "  <p>Assign full permissions to the"
		. " snaptrack database:</p>\n"
		. "  <p class='code'>mysql> GRANT ALL PRIVILEGES"
		. " ON snaptrack.* TO 'username'@'localhost';</p>\n"
		. "  <p>Flush privileges:</p>\n"
		. "  <p class='code'>mysql> FLUSH PRIVILEGES;</p>\n"
		. "  <p><input type='submit' name='step2'"
		. " value='Proceed to Step 2'></p>\n";
	}
elseif ( $step === 2 ) {
	echo "  <h2>STEP 2</h2>\n"
		. "  <p style='margin-bottom: 0px;'>"
		. "Enter database information:</p>\n"
		. "  <table><tbody>\n"
		. "    <tr>\n"
		. "      <td><div style='position: relative;'>\n"
		. "        <input type='text' id='dbhost'"
		. " name='dbhost' required autofocus>\n"
		. "        <label class='tag' for='dbhost'>"
		. "Database Host</label>\n"
		. "      </div></td>\n"
		. "    </tr>\n"
		. "    <tr>\n"
		. "      <td><div style='position: relative;'>\n"
		. "        <input type='text' id='dbuser'"
		. " name='dbuser' required>\n"
		. "        <label class='tag' for='dbuser'>"
		. "Database username</label>\n"
		. "      </div></td>\n"
		. "    </tr>\n"
		. "    <tr>\n"
		. "      <td><div style='position: relative;'>\n"
		. "        <input type='password' id='dbpass'"
		. " name='dbpass' required>\n"
		. "        <label class='tag' for='dbpass'>"
		. "Database password</label>\n"
		. "      </div></td>\n"
		. "    </tr>\n"
		. "    <tr>\n"
		. "      <td><div style='position: relative;'>\n"
		. "        <input type='text' id='database' name='database'"
		. " required>\n"
		. "        <label class='tag' for='database'>Database</label>\n"
		. "      </div></td>\n"
		. "    </tr>\n"
		. "    <tr>\n"
		. "      <td style='padding: 0px;'>"
		. "<input type='submit' name='step3'"
		. " value='Proceed to Step 3'></td>\n"
		. "    </tr>\n"
		. "  </tbody></table>\n";
	}
elseif ( $step === 3 ) {
	$error = FALSE;

	if ( empty( $_POST['dbhost'] ) ) {
		echo "  <script type='text/javascript'>"
			. "showError( 'Step 3', 'Failed to provide host' );"
			. "</script>\n";
		}
	elseif ( empty( $_POST['dbuser'] ) ) {
		echo "  <script type='text/javascript'>"
			. "showError( 'Step 3', 'Failed to provide username' );"
			. "</script>\n";
		}
	elseif ( empty( $_POST['dbpass'] ) ) {
		echo "  <script type='text/javascript'>"
			. "showError( 'Step 3', 'Failed to provide password' );"
			. "</script>\n";
		}
	elseif ( empty( $_POST['database'] ) ) {
		echo "  <script type='text/javascript'>"
			. "showError( 'Step 3', 'Failed to provide database' );"
			. "</script>\n";
		}
	else {
		$conf['db']['host'] = $_POST['dbhost'];
		$conf['db']['user'] = $_POST['dbuser'];
		$conf['db']['pass'] = $_POST['dbpass'];
		$conf['db']['database'] = $_POST['database'];
		$tables = explode( ';', $sql );

		$mysqli = new mysqli( $conf['db']['host'],
			$conf['db']['user'], $conf['db']['pass'],
			$conf['db']['database'] );

		if ( $errno = mysqli_connect_errno() ) {
			$error = TRUE;
			$errstr = mysqli_connect_error();

			echo "<p>Database connection failed</p>\n"
				. "<p class='code'>$errstr</p>\n";
			}

		foreach ( $tables as $table ) {
			if ( $stmt = $mysqli->prepare( $table ) ) {
				mysqli_stmt_execute( $stmt );
				}

			if ( mysqli_error( $mysqli ) ) {
				$error = TRUE;
				$errstr = mysqli_error( $mysqli );

				echo "<p>For query: $table<br><br>"
					. "$errstr</p>\n";

				break;
				}
			elseif ( mysqli_stmt_error( $stmt ) ) {
				$error = TRUE;
				$errstr = mysqli_stmt_error( $stmt );

				echo "<p>For query: $table<br><br>"
					. "$errstr</p>";

				break;
				}
			}
		$mysqli->close();
		}

	if ( ! $error ) {
		echo "  <h2>STEP 3</h2>\n"
			. "  <p style='margin-bottom: 0px;'>"
			. "Create admin account:</p>\n"
			. "  <input type='hidden' name='dbhost' value='"
			. $conf['db']['host'] = $_POST['dbhost'] . "'>\n"
			. "  <input type='hidden' name='dbuser' value='"
			. $conf['db']['user'] = $_POST['dbuser'] . "'>\n"
			. "  <input type='hidden' name='dbpass' value='"
			. $conf['db']['pass'] = $_POST['dbpass'] . "'>\n"
			. "  <input type='hidden' name='database' value='"
			. $conf['db']['database'] = $_POST['database'] . "'>\n"
			. "  <table><tbody>\n"
			. "    <tr>\n"
			. "      <td><div style='position: relative;'>\n"
			. "        <input type='text' id='adminuser'"
			. "name='adminuser' required autofocus>\n"
			. "        <label class='tag' for='adminuser'>"
			. "Admin username</label>\n"
			. "      </div></td>\n"
			. "    </tr>\n"
			. "    <tr>\n"
			. "      <td><div style='position: relative;'>\n"
			. "        <input type='password' id='adminpass'"
			. " name='adminpass' required>\n"
			. "        <label class='tag' for='adminpass'>"
			. "Admin password</label>\n"
			. "      </div></td>\n"
			. "    </tr>\n"
			. "    <tr>\n"
			. "      <td style='padding: 0px;'>\n"
			. "        <input type='submit' name='step4'"
			. " value='Proceed to Step 4'>\n"
			. "      </td>\n"
			. "    </tr>\n"
			. "  </tbody></table>\n";
		}
	}
elseif ( $step === 4 ) {
	$error = FALSE;

	if ( empty( $_POST['adminuser'] ) ) {
		echo "  <script type='text/javascript'>"
			. "showError( 'Step 3', 'Failed to provide admin' );"
			. "</script>\n";
		}
	elseif ( empty( $_POST['adminpass'] ) ) {
		echo "  <script type='text/javascript'>"
			. "showError( 'Step 3', 'Failed to provide password' );"
			. "</script>\n";
		}
	elseif ( empty( $_POST['dbhost'] ) ) {
		echo "  <script type='text/javascript'>"
			. "showError( 'Step 3', 'Failed to provide host' );"
			. "</script>\n";
		}
	elseif ( empty( $_POST['dbuser'] ) ) {
		echo "  <script type='text/javascript'>"
			. "showError( 'Step 3', 'Failed to provide username' );"
			. "</script>\n";
		}
	elseif ( empty( $_POST['dbpass'] ) ) {
		echo "  <script type='text/javascript'>"
			. "showError( 'Step 3', 'Failed to provide password' );"
			. "</script>\n";
		}
	elseif ( empty( $_POST['database'] ) ) {
		echo "  <script type='text/javascript'>"
			. "showError( 'Step 3', 'Failed to provide database' );"
			. "</script>\n";
		}
	else {
		$adminuser = $_POST['adminuser'];
		$adminpass = $_POST['adminpass'];
		$conf['db']['host'] = $_POST['dbhost'];
		$conf['db']['user'] = $_POST['dbuser'];
		$conf['db']['pass'] = $_POST['dbpass'];
		$conf['db']['database'] = $_POST['database'];

		$mysqli = new mysqli( $conf['db']['host'],
			$conf['db']['user'], $conf['db']['pass'],
			$conf['db']['database'] );

		$hash = hashpass( $adminpass );
		$query = "INSERT INTO users(
			user_name,
			password,
			priv_id) VALUES('$adminuser','$hash',2)";

		if ( $stmt = $mysqli->prepare( $query ) ) {
			mysqli_stmt_execute( $stmt );
			}
		else {
			$error = TRUE;
			$errstr = mysqli_error( $mysqli );

			echo "<p>For query: $query<br><br>"
				. "$errstr</p>\n";
			}

		if ( mysqli_error( $mysqli ) ) {
			$error = TRUE;
			$errstr = mysqli_error( $mysqli );

			echo "<p>For query: $query<br><br>"
				. "$errstr</p>\n";
			}
		elseif ( mysqli_stmt_error( $stmt ) ) {
			$error = TRUE;
			$errstr = mysqli_stmt_error( $stmt );

			echo "<p>For query: $query<br><br>"
				. "$errstr</p>";
			}

		}

	if ( ! $error ) {
		echo "<h2>Step 4</h2>\n"
			. "<p>Set username/password in inc/config.php<p>\n"
			. "<p class='code'>\$conf['db']['user'] = 'username';\n"
			. "\$conf['db']['pass'] = 'password';</p>\n"
			. "<p><input type='submit' name='finish'"
			. " value='Proceed to Login'>"
			. "</p>\n";
		}
	}

echo "</form>\n";

require( 'inc/foot.php' );
?>
