<?php
include( 'inc/session.php' );

if ( $_SESSION['user']['user_id'] < 1 ) {
	die();
	}

if ( isset( $_GET['ticket_id'] ) ) {
	$ticket_id = $_GET['ticket_id'];

	$status = steal( $ticket_id );

	if ( isset( $status[0] ) && $status[0] !== 0 ) {
		$_SESSION['status'] = array(
			'func' => 'steal()',
			'code' => $status[0],
			'errstr' => $status[1]
			);
		}

	header( "Location: ticket.php?ticket_id=$ticket_id" );

	die();
	}
else {
	$_SESSION['status'] = array(
		'func' => 'steal.php',
		'code' => -1,
		'errstr' => '- ticket_id empty'
		);

	header( 'Location: ./' );
	}
?>
