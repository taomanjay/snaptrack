<?php
require( 'config.php' );
require( 'functions.php' );

/*
 * Here we check to see if the database user is defined, and if not
 * we start the installer
 */

if ( empty( $conf['db']['user'] ) ) {
	header( 'Location: install.php' );
	die();
	}

/*
 * Here we're defining the functions that will be used for
 * storing and manipulating session data. The data is stored
 * in the database.
 */

session_set_save_handler( 'sess_open', 'sess_close', 'sess_read', 'sess_write',
	'sess_destroy', 'sess_clean' );
register_shutdown_function( 'session_write_close' );

sess_clean( $conf['general']['session_timeout'] );

session_start();

if ( ! isset( $_SESSION['user']['uri'] ) ) {
//	$_SESSION['user']['uri'] = $_SERVER['REQUEST_URI'];
	}

if ( ! isset( $_SESSION['user']['user_id'] ) ) {
	$_SESSION['user'] = array(
		'user_id' => 0,
		'user_name' => 'Anonymous',
		'priv_id' => 0,
		);
	}
elseif ( $_SESSION['user']['user_id'] !== 0 ) {
	$user = getuser( $_SESSION['user']['user_id'] );
	$_SESSION['user']['user_name'] = $user['user_name'];
	$_SESSION['user']['priv_id'] = $user['priv_id'];
	}

$cwd = basename( getcwd() );
$self = basename( __FILE__ );
$self = basename( $_SERVER['PHP_SELF'] );

$_SESSION['current_uri'] = preg_replace( '/.*\/|\?.*/', '',
	$_SERVER['REQUEST_URI'] );

/*
 * Here we see if the user has POSTed anything and supply
 * the values to the appropriate function
 */

if ( $_SERVER['REQUEST_METHOD'] === 'POST'
&& $_SESSION['user']['priv_id'] > 0 ) {
	if ( isset( $_POST['form'] ) && $_POST['form'] === 'editgroup' ) {
		if ( isset( $_POST['delete'] ) ) {
			$status = delgroup( $_POST );
			$status['func'] = 'delgroup()';
			}
		else {
			$status = editgroup( $_POST );
			$status['func'] = 'editgroup()';
			}
		}
	elseif ( isset( $_POST['form'] ) && $_POST['form'] === 'newgroup' ) {
		$status = newgroup( $_POST );
		$status['func'] = 'newgroup()';
		}
	elseif ( isset( $_POST['form'] ) && $_POST['form'] === 'editproject' ) {
		if ( isset( $_POST['delete'] ) ) {
			$status = delproject( $_POST );
			$status['func'] = 'delproject()';
			}
		else {
			$status = editproject( $_POST );
			$status['func'] = 'editproject()';
			}
		}
	elseif ( isset( $_POST['form'] ) && $_POST['form'] === 'newproject' ) {
		$status = newproject( $_POST );
		$status['func'] = 'newproject()';
		}
	elseif ( isset( $_POST['form'] ) && $_POST['form'] === 'reply' ) {
		$status = reply( $_POST );
		$status['func'] = 'reply()';

		if ( ! isset( $status[0] ) || $status[0] === 0 ) {
			header( "Location: ticket.php"
				. "?ticket_id={$_POST['ticket_id']}"
				. "#note{$_POST['reply_to']}" );

			die();
			}
		}
	elseif ( isset( $_POST['form'] )
	&& $_POST['form'] === 'editcomponents' ) {
		$status = editcomponents( $_POST );
		$status['func'] = 'editcomponents()';
		}
	elseif ( isset( $_POST['session_project'] ) ) {
		$_SESSION['project'] = getproject( $_POST['session_project'] );

		header( 'Location: ./' );
		}
	elseif ( isset( $_POST['ticketsearch'] ) ) {
		$ticket_id = $_POST['ticketsearch'];
		$ticket = getticket( $ticket_id );

		if ( ! isset( $ticket[0] ) ) {
			$_SESSION['status'] = array(
				'func' => 'getticket()',
				'code' => -1,
				'errstr' => "Ticket '$ticket_id' not found"
				);
			}
		elseif ( count( $ticket ) > 0 && $ticket[0] !== -1 ) {
			header( "Location: ticket.php?ticket_id=$ticket_id" );

			die();
			}
		else {
			$status = $ticket;
			$status['func'] = 'getticket()';
			}
		}
	elseif ( isset( $_POST['form'] ) && $_POST['form'] === 'newuser' ) {
		$status = newuser( $_POST );
		$status['func'] = 'newuser()';
		}
	elseif ( isset( $_POST['form'] ) && $_POST['form'] === 'edituser' ) {
		$status = edituser( $_POST );
		$status['func'] = 'edituser()';
		}
	elseif ( isset( $_POST['form'] ) && $_POST['form'] === 'newticket' ) {
		$status = newticket( $_POST );
		$status['func'] = 'newticket()';
		}
	elseif ( isset( $_POST['form'] ) && $_POST['form'] === 'editprofile' ) {
		$status = editprofile( $_POST );

		if ( isset( $status[0] ) && $status[0] === -1 ) {
			$status['func'] = 'editprofile()';
			}
		elseif ( ! empty( $_FILES['avatar']['tmp_name'] ) ) {
			$status = avatarupload( $_FILES );
			$status['func'] = 'avatarupload()';

			header( 'Location: profile.php' );
			die();
			}
		}
	elseif ( isset( $_POST['form'] ) && $_POST['form'] === 'resolve' ) {
		$status = resolve( $_POST );
		$status['func'] = 'resolve()';
		}
	elseif ( isset( $_POST['form'] ) && $_POST['form'] === 'reject' ) {
		$status = reject( $_POST );
		$status['func'] = 'reject()';
		}
	elseif ( isset( $_POST['form'] ) && $_POST['form'] === 'reopen' ) {
		$status = reopen( $_POST );
		$status['func'] = 'reopen()';
		}
	elseif ( isset( $_POST['form'] ) && $_POST['form']
	=== 'ticketproject' ) {
		$status = setproject( $_POST );
		$status['func'] = 'setproject()';
		}
	elseif ( isset( $_POST['form'] ) && $_POST['form']
	=== 'ticketcomponent' ) {
		$status = setcomponent( $_POST );
		$status['func'] = 'setcomponent()';
		}
	elseif ( isset( $_POST['form'] ) && $_POST['form']
	=== 'restoreuser' ) {
		$status = restoreuser( $_POST );
		$status['func'] = 'restoreuser()';
		}
	}

if ( isset( $status[0] ) && $status[0] === -1 ) {
	$_SESSION['status'] = array(
		'func' => $status['func'],
		'code' => -1,
		'errstr' => $status[1]
		);
	}

if ( $_SESSION['user']['priv_id'] > 0
&& empty( $_SESSION['project']['project_id'] ) ) {
	$_SESSION['project'] = getminproject();
	}
elseif ( $_SESSION['user']['priv_id'] > 0 ) {
	$_SESSION['project'] = getproject( $_SESSION['project']['project_id'] );
	}
?>
