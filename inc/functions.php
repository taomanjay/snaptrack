<?php

/*
 * This function will return an array of each returned row
 * with each row stored as an associative array. One wouldn't
 * want to use this for retrieving a massive number of rows.
 *
 * It should be invoked like this:
 * query( <SQL statement>, <bind parameter array> )
 *
 * The bind parameter array should have the format:
 * ( 'is', 42, 'a string' )
 *
 * If bind parameters are not required simply pass NULL for
 * the $bind_param value
 */

function query( $query, $bind_param ) {
	global $conf;
	$rows = array();

	$mysqli = mysqli_connect( $conf['db']['host'], $conf['db']['user'],
		$conf['db']['pass'], $conf['db']['database'] );

	if ( $errno = mysqli_connect_errno() ) {
		return( array( -1, 'mysqli_connect: '
			. mysqli_connect_error() ) );
		}

	if ( $stmt = mysqli_prepare( $mysqli, $query ) ) {
		if ( isset( $bind_param ) ) {
			$bind_param_ref = array();

			foreach ( $bind_param as $key => $value ) {
				$bind_param_ref[$key] = &$bind_param[$key];
				}

			call_user_func_array( array( $stmt, 'bind_param' ),
				$bind_param_ref );

			/*
			 * We use call_user_func_array so that any number
			 * of bind variables can be used
			 */

			if ( ! call_user_func_array( array( $stmt,
			'bind_param' ), $bind_param_ref ) ) {
				$error = error_get_last();

				mysqli_stmt_close( $stmt );
				mysqli_close( $mysqli );
				return( array(
					-1,
					$error['message']
					) );
				}
			}

		if ( mysqli_stmt_execute( $stmt ) ) {
			$result = mysqli_stmt_get_result( $stmt );

			if ( ! empty( $result ) ) {
				while ( $row = mysqli_fetch_assoc( $result ) ) {
					array_push( $rows, $row );
					}
				}
			}
		elseif ( $errstr = mysqli_stmt_error( $stmt ) ) {
			mysqli_stmt_close( $stmt );
			mysqli_close( $mysqli );

			return( array( -1, "stmt: $errstr" ) );
			}

		mysqli_stmt_close( $stmt );
		}
	elseif ( $errstr = mysqli_stmt_error( $stmt ) ) {
		mysqli_stmt_close( $stmt );
		mysqli_close( $mysqli );

		return( array( -1, "stmt: $errstr" ) );
		}

	if ( $errstr = mysqli_error( $mysqli ) ) {
		mysqli_stmt_close( $stmt );
		mysqli_close( $mysqli );

		return( array( -1, "mysqli: $errstr" ) );
		}

	mysqli_close( $mysqli );

	return( $rows );
	}

/*
 * This function is useful when multiple queries need to be
 * executed within the single transaction. Really only intended
 * for inserts, so it doesn't attempt to return rows.
 *
 * It is similar to query(), but the individual queries are
 * all stored in the $queries array and the matching bind
 * parameters are stored in the $bind_params array of arrays
 */

function multi_query( $queries, $bind_params ) {
	global $conf;
	$querynum = count( $queries );
	$stmts = array();

	$mysqli = mysqli_connect( $conf['db']['host'], $conf['db']['user'],
		$conf['db']['pass'], $conf['db']['database'] );

	if ( $errno = mysqli_connect_errno() ) {
		return( array( -1, 'mysqli_connect: '
			. mysqli_connect_error() ) );
		}

	mysqli_autocommit( $mysqli, FALSE );

	for ( $i = 0; $i < $querynum; $i++ ) {
		$query = $queries[$i];
		$bind_param = $bind_params[$i];

		if ( $stmts[$i] = mysqli_prepare( $mysqli, $query ) ) {
			if ( isset( $bind_param ) ) {
				$bind_param_ref = array();

				foreach ( $bind_param as $key => $value ) {
					$bind_param_ref[$key] =
						&$bind_param[$key];
					}

				/*
				 * We use call_user_func_array so that any
				 * number of bind variables can be used
				 */

				if ( ! call_user_func_array( array( $stmts[$i],
				'bind_param' ), $bind_param_ref ) ) {
					$error = error_get_last();

					mysqli_stmt_close( $stmt );
					mysqli_close( $mysqli );
					return( array(
						-1,
						$error['message']
						) );
					}
				}

			if ( ! mysqli_stmt_execute( $stmts[$i] ) ) {
				mysqli_stmt_close( $stmt );
				mysqli_close( $mysqli );

				return( array( -1, "stmt: $errstr" ) );
				}

			mysqli_stmt_close( $stmts[$i] );
			}
		elseif ( $errstr = mysqli_stmt_error( $stmts[$i] ) ) {
			mysqli_stmt_close( $stmts[$i] );
			mysqli_close( $mysqli );

			return( array( -1, "stmt: $errstr" ) );
			}
		}

	mysqli_autocommit( $mysqli, TRUE );
	mysqli_commit( $mysqli );

	if ( $errstr = mysqli_error( $mysqli ) ) {
		mysqli_stmt_close( $stmt );
		mysqli_close( $mysqli );

		return( array( -1, "mysqli: $errstr" ) );
		}

	mysqli_close( $mysqli );

	return;
	}

/*
 * The following 6 sess_* functions allow us to store all
 * session information in the database. This should make
 * sessions slightly more secure in a shared hosting
 * environment.
 */

function sess_open() {
	global $conf;
	global $sess_dbh;

	$sess_dbh = new mysqli( $conf['db']['host'], $conf['db']['user'],
		$conf['db']['pass'], $conf['db']['database'] );

	if ( $errno = mysqli_connect_errno() ) {
		return( FALSE );
		}

	return( TRUE );
	}

function sess_close() {
	global $sess_dbh;

	return mysqli_close( $sess_dbh );
	}

function sess_read( $id ) {
	global $sess_dbh;

	$query = "SELECT data FROM sessions WHERE id=?";

	if ( $stmt = $sess_dbh->prepare( $query ) ) {
		$stmt->bind_param( 's', $id );
		$stmt->execute();
		$result = $stmt->get_result();
		}

	if ( mysqli_num_rows( $result ) ) {
		$row = mysqli_fetch_assoc( $result );

		return( $row['data'] );
		}

	return( FALSE );
	}

function sess_write( $id, $data ) {
	global $sess_dbh;

	$timestamp = time();
	$query = "REPLACE INTO sessions VALUES(?,?,?)";

	if ( $stmt = $sess_dbh->prepare( $query ) ) {
		$stmt->bind_param( 'sis', $id, $timestamp, $data );

		if ( $stmt->execute() ) {
			return( TRUE );
			}
		}

	return( FALSE );
	}

function sess_destroy( $id ) {
	global $sess_dbh;

	$query = "DELETE FROM sessions WHERE id=?";

	if ( $stmt = $sess_dbh->prepare( $query ) ) {
		$stmt->bind_param( 's', $id );

		if ( $stmt->execute() ) {
			return( TRUE );
			}
		}

	return( FALSE );
	}

function sess_clean( $max ) {
	global $sess_dbh;

	$maxage = time() - $max;
	$query = "DELETE FROM sessions WHERE access < ?";

	$status = query( $query, array( 'i', $maxage ) );

	if ( isset( $status[0] ) && $status[0] === 0 ) {
		return( TRUE );
		}

	return( FALSE );
	}

/*
 * We pull the user's password hash from the db, then use it as
 * a salt along with the supplied password as a string. The
 * resulting hash should match the hash from the db.
 */

function login( $user_name, $password ) {
	$query = 'SELECT * FROM users WHERE user_name=? AND disabled=0';
	$user = query( $query, array( 's', $user_name ) );

	if ( $user[0]['password'] === crypt( $password,
	$user[0]['password'] ) ) {
		$query = "UPDATE users SET last_login=NOW() WHERE user_id=?";
		
		query( $query, array( 'i', $user[0]['user_id'] ) );

		return( $user[0] );
		}

	return( array( -1, 'Invalid Login' ) );
	}

function getusercount( $searchstring ) {
	if ( empty( $searchstring ) ) {
		$searchstring = '%';
		}

	$query = "SELECT COUNT(user_id) AS count FROM users WHERE
		(user_name LIKE CONCAT('%',?,'%') OR
		first_name LIKE CONCAT('%',?,'%') OR
		surname LIKE CONCAT('%',?,'%') OR
		email LIKE CONCAT('%',?,'%'))";

	if ( $searchstring === '%' ) {
		$query .= ' AND disabled=0';
		}

	$count = query( $query, array( 'ssss',
		$searchstring,
		$searchstring,
		$searchstring,
		$searchstring
		) );

	return( $count[0]['count'] );
	}

function getuser( $user_id ) {
	$query = "SELECT
		user_name,	
		first_name,
		surname,
		email,
		priv_id,
		last_login,
		disabled
		FROM users WHERE user_id=? AND disabled=0";
	$user = query( $query, array( 'i', $user_id ) );

	return( $user[0] );
	}

function getusers( $page, $sort, $searchstring ) {
	$sortfields = array(
		'user_name',
		'first_name',
		'surname',
		'email',
		'priv_name'
		);
	$orders = array(
		'ASC',
		'DESC'
		);

	if ( isset( $sort['field'] ) ) {
		$key = array_search( $sort['field'], $sortfields );
		$field = $sortfields[$key];
		}
	else {
		$field = 'user_name';
		}

	if ( isset( $sort['order'] ) ) {
		$key = array_search( $sort['order'], $orders );
		$order = $orders[$key];
		}
	else {
		$order = 'ASC';
		}

	$query = "SELECT
		a.user_id,
		a.user_name,
		a.first_name,
		a.surname,
		a.email,
		a.priv_id,
		a.disabled,
		COALESCE(a.last_login,'Never') as last_login,
		b.priv_name
		FROM users a LEFT JOIN privs b
		ON a.priv_id=b.priv_id WHERE
		(user_name LIKE CONCAT('%',?,'%') OR
		first_name LIKE CONCAT('%',?,'%') OR
		surname LIKE CONCAT('%',?,'%') OR
		email LIKE CONCAT('%',?,'%'))\n";

	if ( empty( $searchstring ) ) {
		$searchstring = '%';
		$query .= "AND disabled=0\n";
		}

	$query .= "ORDER BY $field $order";

	if ( isset( $page ) ) {
		$query .= " LIMIT ?,?";
		$bind_param = array( 'ssssii',
			$searchstring,
			$searchstring,
			$searchstring,
			$searchstring,
			$page['start'],
			$page['limit']
			);

		$users = query( $query, $bind_param );
		}
	else {
		$bind_param = array( 'ssss',
			$searchstring,
			$searchstring,
			$searchstring,
			$searchstring
			);
			
		$users = query( $query, $bind_param );
		}

	return( $users );
	}

function usertally( $page, $sort, $searchstring ) {
	$sortfields = array(
		'user_name',
		'opennum',
		'resolvednum'
		);
	$orders = array(
		'ASC',
		'DESC'
		);

	if ( isset( $sort['field'] ) ) {
		$key = array_search( $sort['field'], $sortfields );
		$field = $sortfields[$key];
		}
	else {
		$field = 'user_name';
		}

	if ( isset( $sort['order'] ) ) {
		$key = array_search( $sort['order'], $orders );
		$order = $orders[$key];
		}
	else {
		$order = 'ASC';
		}

	$query = "SELECT a.user_name,a.disabled,
		COALESCE(SUM(b.status<2),0) AS opennum,
		COALESCE(SUM(b.status>=2),0) AS resolvednum
		FROM users a LEFT JOIN tickets b ON
		a.user_id=b.owner WHERE
		a.user_name LIKE CONCAT('%',?,'%')\n";

	if ( empty( $searchstring ) ) {
		$query .= "AND a.disabled=0\n";
		$searchstring = '%';
		}

	$query .= "GROUP BY a.user_name
		ORDER BY $field $order";

	if ( isset( $page ) ) {
		$query .= " LIMIT ?,?";
		$bind_param = array( 'sii',
			$searchstring,
			$page['start'],
			$page['limit']
			);
		}
	else {
		$bind_param = NULL;
		}
			
	$usertally = query( $query, $bind_param );

	return( $usertally );
	}

function newuser( $user ) {
	if ( $_SESSION['user']['priv_id'] < 2 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$errstr = '';

	if ( empty( $user['user_name'] ) ) {
		$errstr .= " - username is a required field";
		}
	if ( empty( $user['email'] ) ) {
		if ( ! empty( $errstr ) ) {
			$errstr .= "\n";
			}

		$errstr .= " - email is a required field";
		}
	if ( empty( $user['password'] ) ) {
		if ( ! empty( $errstr ) ) {
			$errstr .= "\n";
			}

		$errstr .= " - password is a required field";
		}
	if ( $user['password'] !== $user['verify_password'] ) {
		if ( ! empty( $errstr ) ) {
			$errstr .= "\n";
			}

		$errstr .= " - password doesn't match";
		}

	if ( ! empty( $errstr ) ) {
		return( array( -1, $errstr ) );
		}

	$hash = hashpass( $user['password'] );

	$query = "INSERT INTO
		users(
			user_name,
			first_name,
			surname,
			email,
			password,
			priv_id
			)
		VALUES(?,?,?,?,?,? )";

	$user = query( $query, array( 'sssssi',
		$user['user_name'],
		$user['first_name'],
		$user['surname'],
		$user['email'],
		$hash,
		$user['priv_id']
		) );

	if ( isset( $user[0] ) && $user[0] === -1 ) {
		return( array( -1, $user[1] ) );
		}

	return;
	}

function edituser( $user ) {
	if ( $_SESSION['user']['priv_id'] < 2 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$errstr = '';

	if ( empty( $user['user_id'] ) ) {
		$errstr .= ' - user_id is required';
		}
	if ( empty( $user['user_name'] ) ) {
		if ( ! empty( $errstr ) ) {
			$errstr .= "\n";
			}

		$errstr .= ' - username is a required field';
		}
	if ( empty( $user['email'] ) ) {
		if ( ! empty( $errstr ) ) {
			$errstr .= "\n";
			}

		$errstr .= ' - email is a required field';
		}
	if ( ! empty( $user['password'] ) ) {
		if ( $user['password'] !== $user['verify_password'] ) {
			if ( ! empty( $errstr ) ) {
				$errstr .= "\n";
				}

			$errstr .= " - passwords do not match";
			}
		}

	if ( ! empty( $errstr ) ) {
		return( array( -1, $errstr ) );
		}

	if ( isset( $user['delete'] ) ) {
		$queries = array(
			'UPDATE users SET disabled=1 WHERE user_id=?',
			'DELETE FROM group_users WHERE user_id=?',
			'DELETE FROM component_users WHERE user_id=?',
			'DELETE FROM project_admins WHERE user_id=?'
			);
		$bind_params = array(
			array( 'i', $user['user_id'] ),
			array( 'i', $user['user_id'] ),
			array( 'i', $user['user_id'] ),
			array( 'i', $user['user_id'] )
			);
		$status = multi_query( $queries, $bind_params );

		return( $status );
		}

	if ( ! empty( $user['password'] ) ) {
		$hash = hashpass( $user['password'] );

		$query = "UPDATE users SET
				user_name=?,
				first_name=?,
				surname=?,
				email=?,
				password=?,
				priv_id=?
				WHERE user_id=?";

		$status = query( $query, array( 'sssssii',
			$user['user_name'],
			$user['first_name'],
			$user['surname'],
			$user['email'],
			$hash,
			$user['priv_id'],
			$user['user_id']
			) );
		}
	else {
		$query = "UPDATE users SET
				user_name=?,
				first_name=?,
				surname=?,
				email=?,
				priv_id=?
				WHERE user_id=?";

		$status = query( $query, array( 'ssssii',
			$user['user_name'],
			$user['first_name'],
			$user['surname'],
			$user['email'],
			$user['priv_id'],
			$user['user_id']
			) );
		}

	return( $status );
	}

function restoreuser( $user ) {
	if ( $_SESSION['user']['user_id'] < 2 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	if ( empty( $user['user_id'] ) ) {
		return( array( -1, ' - user_id is required' ) );
		}

	$query = 'UPDATE users SET disabled=0 WHERE user_id=?';
	$bind_param = array( 'i', $user['user_id'] );
	$status = query( $query, $bind_param );

	return( $status );
	}

function editprofile( $user ) {
	if ( $_SESSION['user']['user_id'] != $user['user_id'] ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$errstr = '';

	if ( empty( $user['user_id'] ) ) {
		$errstr .= ' - user_id is required';
		}
	if ( empty( $user['email'] ) ) {
		if ( ! empty( $errstr ) ) {
			$errstr .= "\n";
			}

		$errstr .= ' - email is a required field';
		}
	if ( ! empty( $user['password'] ) ) {
		if ( $user['password'] !== $user['verify_password'] ) {
			if ( ! empty( $errstr ) ) {
				$errstr .= "\n";
				}

			$errstr .= " - passwords do not match";
			}
		}

	if ( ! empty( $errstr ) ) {
		return( array( -1, $errstr ) );
		}

	if ( ! empty( $user['password'] ) ) {
		$hash = hashpass( $user['password'] );

		$query = "UPDATE users SET
				first_name=?,
				surname=?,
				email=?,
				password=?
				WHERE user_id=?";

		$status = query( $query, array( 'ssssi',
			$user['first_name'],
			$user['surname'],
			$user['email'],
			$hash,
			$user['user_id']
			) );
		}
	else {
		$query = "UPDATE users SET
				first_name=?,
				surname=?,
				email=?
				WHERE user_id=?";

		$status = query( $query, array( 'sssi',
			$user['first_name'],
			$user['surname'],
			$user['email'],
			$user['user_id']
			) );
		}

	return( $status );
	}

function avatarupload( $avatar ) {
	if ( empty( $avatar['avatar']['tmp_name'] ) ) {
		return( array( -1, ' - No image uploaded' ) );
		}

	$size = getimagesize( $avatar['avatar']['tmp_name'] );

	if ( $size === false ) {
		return( array( -1, ' - Only images can be uploaded' ) );
		}

	$type = exif_imagetype( $avatar['avatar']['tmp_name'] );

	if ( $type !== 2 ) {
		return( array( -1, " - Not a JPEG image -$type" ) );
		}

	move_uploaded_file( $avatar['avatar']['tmp_name'],
	"avatars/{$_SESSION['user']['user_id']}.jpg" );
	resize( "avatars/{$_SESSION['user']['user_id']}.jpg", 128 );

	return;
	}

function getgroupcount( $searchstring ) {
	if ( empty( $searchstring ) ) {
		$searchstring = '%';
		}

	$query = "SELECT COUNT(group_id) AS count FROM groups
		WHERE disabled=0 AND group_name LIKE CONCAT('%',?,'%')";
	$bind_param = array( 's', $searchstring );
	$count = query( $query, $bind_param );

	return( $count[0]['count'] );
	}

function getgroup( $group_id ) {
	$query = "SELECT a.group_name,c.user_id,c.user_name FROM groups a
                LEFT JOIN group_users b ON a.group_id=b.group_id
                LEFT JOIN users c ON b.user_id=c.user_id
                WHERE a.group_id=? AND (c.disabled=0 or c.user_id IS NULL)
                ORDER BY c.user_name";
	$query = "SELECT a.group_name,c.user_id,c.user_name FROM groups a
                LEFT JOIN group_users b ON a.group_id=b.group_id
                LEFT JOIN users c ON b.user_id=c.user_id
                WHERE a.group_id=? AND (c.disabled=0 or c.user_id IS NULL)
                ORDER BY c.user_name";
	$query = "SELECT * FROM groups a
                LEFT JOIN group_users b ON a.group_id=b.group_id
                LEFT JOIN users c ON b.user_id=c.user_id AND c.disabled=0
                WHERE a.group_id=? ORDER BY c.user_name";

	$group = query( $query, array( 'i', $group_id ) );

	return( $group );
	}

function getgroupusers( $group_id ) {
	$query = "SELECT a.user_id,b.user_name FROM group_users a
                LEFT JOIN users b ON a.user_id=b.user_id
                WHERE a.group_id=? AND b.disabled=0
                ORDER BY b.user_name";

	$group = query( $query, array( 'i', $group_id ) );

	return( $group );
	}

function getgroupprojects( $group_id ) {
	$query = "SELECT a.project_id,b.project_name FROM project_groups a
                LEFT JOIN projects b ON a.project_id=b.project_id
                WHERE a.group_id=? AND b.disabled=0
                ORDER BY b.project_name";

	$group = query( $query, array( 'i', $group_id ) );

	return( $group );
	}

function getgroups( $page, $sort, $searchstring ) {
	if ( empty( $searchstring ) ) {
		$searchstring = '%';
		}

	$sortfields = array(
		'group_name',
		'membernum',
		'projectnum'
		);
	$orders = array(
		'ASC',
		'DESC'
		);

	/*
	 * Here we allow the user to order by a specific select group
	 * of fields, or use the defaults. Done this way to prevent
	 * SQL injection
	 */

	if ( isset( $sort['field'] ) ) {
		$key = array_search( $sort['field'], $sortfields );
		$field = $sortfields[$key];
		}
	else {
		$field = 'group_name';
		}

	if ( isset( $sort['order'] ) ) {
		$key = array_search( $sort['order'], $orders );
		$order = $orders[$key];
		}
	else {
		$order = 'ASC';
		}

	if ( $page === NULL ) {
		$query = "SELECT *,(SELECT count(user_id) FROM
			group_users WHERE group_id=a.group_id) AS membernum,
			(SELECT count(project_id) FROM project_groups
			WHERE group_id=a.group_id) AS projectnum
			FROM groups a WHERE disabled=0 AND
			group_name LIKE CONCAT('%',?,'%')
			ORDER BY $field $order";
		$bind_param = array( 's', $searchstring );
		}
	else {
		$query = "SELECT *,(SELECT count(user_id) FROM
			group_users WHERE group_id=a.group_id) AS membernum,
			(SELECT count(project_id) FROM project_groups
			WHERE group_id=a.group_id) AS projectnum
			FROM groups a WHERE disabled=0 AND
			group_name LIKE CONCAT('%',?,'%')
			ORDER BY $field $order LIMIT ?,?";
		$bind_param = array( 'sii',
			$searchstring,
			$page['start'],
			$page['limit']
			);
		}

	$groups = query( $query, $bind_param );

	return( $groups );
	}

function newgroup( $group ) {
	if ( $_SESSION['user']['priv_id'] < 2 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$errstr = '';

	if ( empty( $group['group_name'] ) ) {
		$errstr .= " - group name is a required field";
		}

	if ( ! empty( $errstr ) ) {
		return( array( -1, $errstr ) );
		}

	$query = "INSERT INTO groups(group_name) VALUES(?)";

	$group = query( $query, array( 's',
		$group['group_name'],
		) );

	if ( isset( $group[0] ) && $group[0] === -1 ) {
		return( array( -1, $group[1] ) );
		}

	return;
	}

function editgroup( $group ) {
	if ( $_SESSION['user']['priv_id'] < 2 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$errstr = '';

	if ( empty( $group['group_id'] ) ) {
		$errstr .= " - group id is a required field";
		}
	if ( empty( $group['group_name'] ) ) {
		$errstr .= " - group name is a required field";
		}

	if ( ! empty( $errstr ) ) {
		return( array( -1, $errstr ) );
		}

	$queries = array( 'UPDATE groups SET group_name=? WHERE group_id=?',
		'DELETE FROM group_users WHERE group_id=?',
		'DELETE FROM project_groups WHERE group_id=?' );
	$bind_params = array(
		array( 'si',
			$group['group_name'],
			$group['group_id']
			),
		array( 'i',
			$group['group_id']
			),
		array( 'i',
			$group['group_id']
			)
		);

	if ( isset( $group['users'] ) ) {
		foreach ( $group['users'] as $user_id ) {
			array_push( $queries,
				"INSERT INTO group_users VALUES(?,?)" );
			array_push( $bind_params, array( 'ii',
				$group['group_id'],
				$user_id
				) );
			}
		}

	if ( isset( $group['projects'] ) ) {
		foreach ( $group['projects'] as $project_id ) {
			array_push( $queries,
				"INSERT INTO project_groups VALUES(?,?)" );
			array_push( $bind_params, array( 'ii',
				$project_id,
				$group['group_id']
				) );
			}
		}

	$status = multi_query( $queries, $bind_params );

	return( $status );
	}

function delgroup( $group ) {
	if ( $_SESSION['user']['priv_id'] < 2 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$status = array( 0, '' );

	if ( empty( $group['group_id'] ) ) {
		$status[0]++;
		$status[1] .= ' - group_id is required';
		}

	$queries = array( 'UPDATE groups SET disabled=1 WHERE group_id=?',
		'DELETE FROM component_groups WHERE group_id=?',
		'DELETE FROM project_groups WHERE group_id=?' );
	$bind_params = array(
		array( 'i', $group['group_id'] ),
		array( 'i', $group['group_id'] ),
		array( 'i', $group['group_id'] )
		);
	$status = multi_query( $queries, $bind_params );

	return( $status );

	$query = "UPDATE groups SET disabled=1 WHERE group_id=?";
	$status = query( $query, array( 'i', $group['group_id'] ) );

	return( $status );
	}

function getprojectcount() {
	$query = "SELECT COUNT(project_id) AS count FROM projects
		WHERE disabled=0";
	$count = query( $query, NULL );

	return( $count[0]['count'] );
	}

function getminproject() {
	$query = "SELECT MIN(project_id) AS project_id,project_name
		FROM projects WHERE disabled=0";

	$project = query( $query, NULL );

	return( array(
		'project_id' => $project[0]['project_id'],
		'project_name' => $project[0]['project_name'] )
		);
	}

function getproject( $project_id ) {
	$query = "SELECT * FROM projects WHERE project_id=?";
	$project = query( $query, array( 'i', $project_id ) );

	return( $project[0] );
	}

function getprojects( $page, $sort, $searchstring ) {
	$sortfields = array(
		'project_name',
		'ticketnum',
		'adminnum',
		'groupnum',
		'componentnum'
		);
	$orders = array(
		'ASC',
		'DESC'
		);

	/*
	 * Here we allow the user to order by a specific select group
	 * of fields, or use the defaults. Done this way to prevent
	 * SQL injection
	 */

	if ( isset( $sort['field'] ) ) {
		$key = array_search( $sort['field'], $sortfields );
		$field = $sortfields[$key];
		}
	else {
		$field = 'project_name';
		}

	if ( isset( $sort['order'] ) ) {
		$key = array_search( $sort['order'], $orders );
		$order = $orders[$key];
		}
	else {
		$order = 'ASC';
		}

	if ( ! isset( $searchstring ) ) {
		$searchstring = '%';
		}

	if ( $page === NULL ) {
		$query = "SELECT *,(SELECT count(ticket_id) FROM
			tickets WHERE project_id=a.project_id AND
			status<2) AS ticketnum,
			(SELECT count(component_id) FROM components
			WHERE project_id=a.project_id AND disabled=0)
			AS componentnum,(SELECT count(user_id) FROM
			project_admins WHERE project_id=a.project_id)
			AS adminnum,(SELECT count(group_id) FROM
			project_groups WHERE project_id=a.project_id)
			AS groupnum FROM projects a
			WHERE disabled=0 AND
			project_name LIKE CONCAT('%',?,'%')
			ORDER BY $field $order";
		$projects = query( $query, array( 's', $searchstring ) );
		}
	else {
		$query = "SELECT *,(SELECT count(ticket_id) FROM
			tickets WHERE project_id=a.project_id AND
			status<2) AS ticketnum,
			(SELECT count(component_id) FROM
			components WHERE project_id=a.project_id AND disabled=0)
			AS componentnum,(SELECT count(user_id) FROM
			project_admins WHERE project_id=a.project_id)
			AS adminnum,(SELECT count(group_id) FROM
			project_groups WHERE project_id=a.project_id)
			AS groupnum FROM projects a
			WHERE disabled=0 AND
			project_name LIKE CONCAT('%',?,'%')
			ORDER BY $field $order LIMIT ?,?";
		$projects = query( $query, array( 'sii',
			$searchstring,
			$page['start'],
			$page['limit']
			) );
		}

	return( $projects );
	}

function getprojectgroups( $project_id ) {
	$errstr = '';

	if ( empty( $project_id ) ) {
		$errstr .= " - project id is a required field";
		}

	if ( ! empty( $errstr ) ) {
		return( array( -1, $errstr ) );
		}

	$query = "SELECT a.group_id,b.group_name FROM project_groups a
                LEFT JOIN groups b ON a.group_id=b.group_id
                WHERE a.project_id=? AND b.disabled=0
                ORDER BY b.group_name";

	$groups = query( $query, array( 'i', $project_id ) );

	return( $groups );
	}

function getprojectadmins( $project_id ) {
	$query = "SELECT a.*,c.user_id,c.user_name FROM projects a LEFT JOIN
		project_admins b ON a.project_id=b.project_id LEFT JOIN
		users c ON b.user_id=c.user_id WHERE a.project_id=? AND
		a.disabled=0 AND c.disabled=0 ORDER BY c.user_name";
	$projectadmins = query( $query, array( 'i', $project_id ) );

	return( $projectadmins );
	}

function newproject( $project ) {
	if ( $_SESSION['user']['priv_id'] < 2 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$errstr = '';

	if ( empty( $project['project_name'] ) ) {
		$errstr .= " - project name is a required field";
		}

	if ( ! empty( $errstr ) ) {
		return( array( -1, $errstr ) );
		}

	$query = "INSERT INTO projects(project_name) VALUES(?)";

	$status = query( $query, array( 's',
		$project['project_name'],
		) );

	return( $status );
	}

function editproject( $project ) {
	if ( $_SESSION['user']['priv_id'] < 1 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$errstr = '';

	if ( empty( $project['project_id'] ) ) {
		$errstr .= " - project id is a required field";
		}
	if ( empty( $project['project_name'] ) ) {
		$errstr .= " - project name is a required field";
		}

	if ( ! empty( $errstr ) ) {
		return( array( -1, $errstr ) );
		}

	$queries = array(
		'UPDATE projects SET project_name=? WHERE project_id=?',
		'DELETE FROM project_admins WHERE project_id=?',
		'DELETE FROM project_groups WHERE project_id=?'
		);
	$bind_params = array(
		array( 'si',
			$project['project_name'],
			$project['project_id']
			),
		array( 'i',
			$project['project_id']
			),
		array( 'i',
			$project['project_id']
			)
		);

	if ( isset( $project['admins'] ) ) {
		foreach ( $project['admins'] as $user_id ) {
			array_push( $queries,
				"INSERT INTO project_admins VALUES(?,?)" );
			array_push( $bind_params, array( 'ii',
				$project['project_id'],
				$user_id
				) );
			}
		}

	if ( isset( $project['groups'] ) ) {
		foreach ( $project['groups'] as $group_id ) {
			array_push( $queries,
				"INSERT INTO project_groups VALUES(?,?)" );
			array_push( $bind_params, array( 'ii',
				$project['project_id'],
				$group_id
				) );
			}
		}

	$status = multi_query( $queries, $bind_params );

	return( $status );
	}

function delproject( $project ) {
	if ( $_SESSION['user']['priv_id'] < 2 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$status = array( 0, '' );

	if ( empty( $project['project_id'] ) ) {
		$status[0]++;
		$status[1] .= ' - project_id is required';
		}

	$query = "UPDATE projects SET disabled=1 WHERE project_id=?";
	$status = query( $query, array( 'i', $project['project_id'] ) );

	if ( isset( $status[0] ) && $status[0] === -1 ) {
		return( $status );
		}

	if ( $project['project_id'] == $_SESSION['project']['project_id'] ) {
		$_SESSION['project'] = getminproject();
		}

	return( $status );
	}

function getcomponents( $project_id ) {
	$components = array( 0, '' );

	if ( empty( $project_id ) ) {
		return( array( -1, ' - Failed to supply project id' ) );
		}

	$query = "SELECT * FROM components WHERE project_id=?
		AND disabled=0 ORDER BY component_name";
	$query = "SELECT
			a.*,
			c.user_name,
			e.group_name
		FROM components a LEFT JOIN
		component_users b ON a.component_id=b.component_id LEFT JOIN
		users c ON b.user_id=c.user_id LEFT JOIN
		component_groups d ON a.component_id=d.component_id LEFT JOIN
		groups e ON d.group_id=e.group_id
		WHERE a.project_id=?";
	$query = "SELECT
			a.*,
			GROUP_CONCAT(c.user_id ORDER BY
				c.user_name ASC SEPARATOR ',') AS user_ids,
			GROUP_CONCAT(c.user_name ORDER BY
				c.user_name ASC SEPARATOR ',') AS user_names,
			GROUP_CONCAT(e.group_id ORDER BY
				e.group_name ASC SEPARATOR ',') AS group_ids,
			GROUP_CONCAT(e.group_name ORDER BY
				e.group_name ASC SEPARATOR ',') AS group_names
		FROM components a LEFT JOIN
		component_users b ON a.component_id=b.component_id LEFT JOIN
		users c ON b.user_id=c.user_id LEFT JOIN
		component_groups d ON a.component_id=d.component_id LEFT JOIN
		groups e ON d.group_id=e.group_id
		WHERE a.project_id=? AND a.disabled=0 GROUP BY a.component_id";
	$query = "SELECT *,
		(SELECT GROUP_CONCAT(b.user_id) FROM component_users b
			WHERE component_id=a.component_id) as user_ids,
		(SELECT GROUP_CONCAT(group_id) FROM component_groups
			WHERE component_id=a.component_id) AS group_ids
		FROM components a WHERE project_id=? AND a.disabled=0";
	$components = query( $query, array( 'i',
		$project_id
		) );

	return( $components );
	}

function getcomponentusers( $component_id ) {
	$users = array( 0, '' );

	if ( empty( $component_id ) ) {
		$query = "SELECT * FROM component_users";
		$users = query( $query, NULL );
		}
	else {
		$query = "SELECT * FROM component_users WHERE project_id=?
			AND disabled=0";
		$users = query( $query, array( 'i', $component_id ) );
		}

	return( $users );
	}

function getcomponentgroups( $component_id ) {
	$groups = array( 0, '' );

	if ( empty( $component_id ) ) {
		$query = "SELECT * FROM component_groups";
		$groups = query( $query, NULL );
		}
	else {
		$query = "SELECT * FROM component_groups WHERE project_id=?
			AND disabled=0";
		$groups = query( $query, array( 'i', $component_id ) );
		}

	return( $groups );
	}

function editcomponents( $data ) {
	if ( $_SESSION['user']['priv_id'] < 1 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$components = array();
	$new = array();
	$errstr = '';

	if ( empty( $data['project_id'] ) ) {
		$errstr .= " - project id is a required field";
		}

	if ( ! empty( $errstr ) ) {
		return( array( -1, $errstr ) );
		}

	$queries = array(
		'UPDATE components SET disabled=1 WHERE project_id=?',
		'DELETE a FROM component_users a LEFT JOIN
			components b ON a.component_id=b.component_id
			WHERE project_id=?',
		'DELETE a FROM component_groups a LEFT JOIN
			components b ON a.component_id=b.component_id
			WHERE project_id=?',
		);
	$bind_params = array(
		array( 'i',
			$data['project_id']
			),
		array( 'i',
			$data['project_id']
			),
		array( 'i',
			$data['project_id']
			)
		);

	if ( ! isset( $data['components'] ) ) {
		$status = multi_query( $queries, $bind_params );

		return( $status );
		}

	foreach ( $data['components'] as $component ) {
		if ( is_numeric( $component ) ) {
			array_push( $queries, "UPDATE components SET disabled=0
				WHERE component_id=?" );
			array_push( $bind_params, array( 'i', $component ) );
			}
		else {
			array_push( $queries, "INSERT INTO components(
				project_id,
				component_name,
				disabled
				) VALUES(?,?,?) ON DUPLICATE KEY
				update disabled=0" );
			array_push( $bind_params, array( 'isi',
				$data['project_id'],
				$component,
				0 ) );
			}

		if ( isset( $data["users_$component"] ) ) {
			foreach ( $data["users_$component"] as $user ) {
				if ( is_numeric( $component ) ) {
					array_push( $queries, "INSERT INTO
						component_users VALUES(?,?)" );
					array_push( $bind_params, array( 'ii',
						$component,
						$user
						) );
					}
				else {
					array_push( $queries, "INSERT INTO
						component_users VALUES(
						(SELECT component_id FROM
							components WHERE
							project_id=? AND
							component_name=?),?)" );
					array_push( $bind_params, array( 'isi',
						$data['project_id'],
						$component,
						$user
						) );
					}
				}
			}

		if ( isset( $data["groups_$component"] ) ) {
			foreach ( $data["groups_$component"] as $group ) {
				if ( is_numeric( $component ) ) {
					array_push( $queries, "INSERT INTO
						component_groups VALUES(?,?)" );
					array_push( $bind_params, array( 'ii',
						$component,
						$group
						) );
					}
				else {
					array_push( $queries, "INSERT INTO
						component_groups VALUES(
						(SELECT component_id FROM
							components WHERE
							project_id=? AND
							component_name=?),?)" );
					array_push( $bind_params, array( 'isi',
						$data['project_id'],
						$component,
						$group
						) );
					}
				}
			}
		}

	$status = multi_query( $queries, $bind_params );

	return( $status );
	}

function addusers( $addusers ) {
	if ( $_SESSION['user']['priv_id'] < 2 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$status = array( 0, '' );

	if ( isset( $addusers['group_id'] ) ) {
		$table = 'group_users';
		$field = 'group_id';
		$id = $addusers['group_id'];
		}
	elseif ( isset( $addusers['project_id'] ) ) {
		$table = 'project_admins';
		$field = 'project_id';
		$id = $addusers['project_id'];
		}
	else {
		$status[0] = -1;
		$status[1] = ' - id not supplied';
		}

	if ( $status[0] === -1 ) {
		return( $status );
		}

	$queries = array( "DELETE FROM $table WHERE $field=?" );
	$bind_params = array( array( 'i', $id ) );

	foreach ( $addusers['users'] as $user_id ) {
		array_push( $queries, "INSERT INTO $table VALUES(?,?)" );
		array_push( $bind_params, array( 'ii',
			$id,
			$user_id
			) );
		}

	$status = multi_query( $queries, $bind_params );

	return( $status );
	}

function rmuser ( $rmuser ) {
	if ( $_SESSION['user']['priv_id'] < 2 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$status = array( 0, '' );

	if ( empty( $rmuser['user_id'] ) ) {
		$status[0]++;
		$status[1] .= ' - user id is required';
		}

	if ( isset( $rmuser['group_id'] ) ) {
		$table = 'group_users';
		$idname = 'group_id';
		$id = $rmuser['group_id'];
		}
	elseif ( isset( $rmuser['project_id'] ) ) {
		$table = 'project_admins';
		$idname = 'project_id';
		$id = $rmuser['project_id'];
		}
	else {
		$status[0]++;
		$status[1] .= ' - incorrect parameters';
		}

	if ( isset( $status[0] ) && $status[0] === -1 ) {
		return( $status );
		}

	$query = "DELETE FROM $table WHERE $idname=? AND user_id=?";

	$status = query( $query, array( 'ii',
		$id,
		$rmuser['user_id']
		) );

	return( $status );
	}

function newticket( $ticket ) {
	if ( $_SESSION['user']['priv_id'] < 1 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$errstr = '';

	if ( empty( $ticket['title'] ) ) {
		$errstr .= " - title is a required field";
		}
	if ( empty( $ticket['project_id'] ) ) {
		if ( ! empty( $errstr ) ) {
			$errstr .= "\n";
			}

		$errstr .= " - project is a required field";
		}
	if ( empty( $ticket['note'] ) ) {
		if ( ! empty( $errstr ) ) {
			$errstr .= "\n";
			}

		$errstr .= " - note is a required field";
		}

	if ( ! empty( $errstr ) ) {
		return( array( -1, $errstr ) );
		}

	if ( empty( $ticket['component_id'] ) ) {
		$ticket['component_id'] = NULL;
		}

	$query = array( "INSERT INTO tickets(
			project_id,
			component_id,
			title,
			created_by,
			created_time
			) VALUES(?,?,?,?,NOW());",
		"INSERT INTO notes(
			ticket_id,
			created_by,
			created_time,
			note
			) VALUES(LAST_INSERT_ID(),?,NOW(),?);" );

	$status = multi_query( $query, array(
		array( 'iisi',
			$ticket['project_id'],		
			$ticket['component_id'],		
			$ticket['title'],
			$_SESSION['user']['user_id']
			),
		array( 'is',
			$_SESSION['user']['user_id'],
			$ticket['note']
			)
		) );

	return( $status );
	}

function getticketcount( $gadget, $searchstring ) {
	if ( empty( $searchstring ) ) {
		$searchstring = '%';
		}

	$bind_param = array( 'is',
		$_SESSION['project']['project_id'],
		$searchstring
		);

	if ( $gadget == 'mytickets' ) {
	 	$query = "SELECT COUNT(ticket_id) AS count FROM tickets
 			WHERE project_id=? AND owner=? AND status < 2
			AND title LIKE CONCAT('%',?,'%')";
		$bind_param = array( 'iis',
			$_SESSION['project']['project_id'],
			$_SESSION['user']['user_id'],
			$searchstring
			);
		}
	else if ( $gadget == 'newtickets' ) {
	 	$query = "SELECT COUNT(ticket_id) AS count FROM tickets
 			WHERE project_id=? AND owner IS NULL
			AND title LIKE CONCAT('%',?,'%')";
		}
	else if ( $gadget == 'oldtickets' ) {
		$query = "SELECT COUNT(ticket_id) AS count FROM tickets
			WHERE project_id=? AND status=2
			AND title LIKE CONCAT('%',?,'%')";
		}
	else {
		$query = "SELECT COUNT(ticket_id) AS count FROM tickets
			WHERE project_id=? AND status < 2
			AND title LIKE CONCAT('%',?,'%')";
		}

	$count = query( $query, $bind_param );

	return( $count[0]['count'] );
	}

function getticket( $ticket_id ) {
	$query = "SELECT
		a.ticket_id,
		a.title,
		a.project_id,
		a.component_id,
		b.project_name,
		d.component_name AS component,
		a.created_by,
		(SELECT user_name FROM users
			WHERE user_id=a.created_by) AS creator,
		a.created_time,
		a.assigned_time,
		a.resolved_time,
		a.owner,
		(SELECT user_name FROM users
			WHERE user_id=a.owner) AS owner_name,
		(SELECT disabled FROM users
			WHERE user_id=a.owner) AS owner_disabled,
		a.status,
		(SELECT status_name FROM statuses
			WHERE status_id=a.status) AS status_name,
		c.note_id,
		c.reply_to,
		c.created_by AS notes_created_by,
		(SELECT user_name FROM users
			WHERE user_id=c.created_by) AS notes_creator,
		c.created_time AS notes_created_time,
		c.note
		FROM tickets a LEFT JOIN projects b ON
		a.project_id=b.project_id LEFT JOIN notes c ON
		a.ticket_id=c.ticket_id LEFT JOIN components d ON
		a.component_id=d.component_id WHERE a.ticket_id=?
		ORDER BY c.note_id";
	$ticket = query( $query, array( 'i', $ticket_id ) );

	return( $ticket );
	}

function gettickets( $gadget, $page, $sort, $searchstring ) {
	if ( empty( $searchstring ) ) {
		$searchstring = '%';
		}

	$sortfields = array(
		'ticket_id',
		'component',
		'title',
		'owner',
		'creator',
		'created_time'
		);
	$orders = array(
		'ASC',
		'DESC'
		);
	$query = "SELECT
		a.ticket_id,
		a.title,
		a.created_time,
		b.user_name AS creator,
		c.user_name AS owner,
		d.component_name AS component
		FROM tickets a LEFT JOIN users b ON
		a.created_by=b.user_id LEFT JOIN users c ON
		a.owner=c.user_id LEFT JOIN components d ON
		a.component_id=d.component_id
		WHERE a.project_id=?";
	$bind_param = array( 'isii',
		$_SESSION['project']['project_id'],
		$searchstring,
		$page['start'],
		$page['limit']
		);

	/*
	 * Here we allow the user to order by a specific select group
	 * of fields, or use the defaults. Done this way to prevent
	 * SQL injection - bind variables don't work for order values
	 */

	if ( isset( $sort['field'] ) ) {
		$key = array_search( $sort['field'], $sortfields );
		$field = $sortfields[$key];
		}
	else {
		$field = 'created_time';
		}

	if ( isset( $sort['order'] ) ) {
		$key = array_search( $sort['order'], $orders );
		$order = $orders[$key];
		}
	else {
		$order = 'ASC';
		}
		

	if ( $gadget == 'mytickets' ) {
		$query .= ' AND owner=? AND status < 2';
		$bind_param = array( 'iisii',
			$_SESSION['project']['project_id'],
			$_SESSION['user']['user_id'],
			$searchstring,
			$page['start'],
			$page['limit']
			);
		}
	else if ( $gadget == 'newtickets' ) {
		$query .= ' AND status < 2 AND owner IS NULL';
		}
	else if ( $gadget == 'oldtickets' ) {
		$query .= ' AND status=2';
		}
	else {
		$query .= ' AND status < 2';
		}
			
	$query .= " AND title LIKE CONCAT('%',?,'%')
		ORDER BY $field $order LIMIT ?,?";
	$tickets = query( $query, $bind_param );

	return( $tickets );
	}

function getcreator( $ticket_id ) {
	$query = 'SELECT b.* FROM tickets a LEFT JOIN
		users b ON a.created_by=b.user_id WHERE ticket_id=?';
	$bind_param = array( 'i', $ticket_id );
	$watchers = query( $query, $bind_param );

	return( $watchers );
	}

function getowner( $ticket_id ) {
	$query = 'SELECT b.* FROM tickets a LEFT JOIN
		users b ON a.owner=b.user_id WHERE ticket_id=?';
	$bind_param = array( 'i', $ticket_id );
	$watchers = query( $query, $bind_param );

	return( $watchers );
	}

function getwatchers( $ticket_id ) {
	$query = 'SELECT a.user_id,b.user_name,b.email FROM watchers a LEFT JOIN
		users b ON a.user_id=b.user_id WHERE ticket_id=?';
	$bind_param = array( 'i', $ticket_id );
	$watchers = query( $query, $bind_param );

	return( $watchers );
	}

function take( $ticket_id ) {
	if ( ! icanhaz( $ticket_id ) ) {
		return( array( -1, ' - Insufficient permissions' ) );
		}

	if ( empty( $ticket_id ) ) {
		$errstr = ' - ticket_id empty';

		return( array( -1, $errstr ) );
		}

	$note = "Taken by {$_SESSION['user']['user_name']}";
	$queries = array(
		'UPDATE tickets SET owner=?,assigned_time=NOW(),
			status=1 WHERE ticket_id=? AND owner IS NULL',
		'INSERT INTO notes(
			ticket_id,
			reply_to,
			created_by,
			created_time,
			note)
		VALUES(?,(SELECT note_id FROM
			(SELECT note_id FROM notes WHERE ticket_id=?
				AND reply_to IS NULL) AS t ),
			?,NOW(),?)'
		);
	$bind_params = array(
		array( 'ii',
			$_SESSION['user']['user_id'],
			$ticket_id
			),
		array( 'iiis',
			$ticket_id,
			$ticket_id,
			NULL,
			$note
			)
		);

	$status = multi_query( $queries, $bind_params );

	if ( ! isset( $status[0] ) || $status[0] !== -1 ) {
		notify( $ticket_id, 'take', $note );
		}

	return( $status );
	}

function steal( $ticket_id ) {
	if ( ! icanhaz( $ticket_id ) ) {
		return( array( -1, ' - Insufficient permissions' ) );
		}

	$note = "Stolen by {$_SESSION['user']['user_name']}";
	$queries = array(
		'UPDATE tickets SET owner=?,assigned_time=NOW(),
			status=1 WHERE ticket_id=?',
		'INSERT INTO notes(
			ticket_id,
			reply_to,
			created_by,
			created_time,
			note)
		VALUES(?,(SELECT note_id FROM
			(SELECT note_id FROM notes WHERE ticket_id=?
				AND reply_to IS NULL) AS t ),
			?,NOW(),?)'
		);
	$bind_params = array(
		array( 'ii',
			$_SESSION['user']['user_id'],
			$ticket_id
			),
		array( 'iiis',
			$ticket_id,
			$ticket_id,
			NULL,
			$note
			)
		);

	if ( empty( $ticket_id ) ) {
		$errstr = '- ticket_id empty';

		return( array( -1, $errstr ) );
		}

	$status = multi_query( $queries, $bind_params );

	if ( ! isset( $status[0] ) || $status[0] !== -1 ) {
		notify( $ticket_id, 'steal', $note );
		}

	return( $status );
	}

function watch( $ticket_id ) {
	if ( empty( $ticket_id ) ) {
		$errstr = ' - ticket_id empty';

		return( array( -1, $errstr ) );
		}

	$note = "Watched by {$_SESSION['user']['user_name']}";
	$queries = array(
		'INSERT INTO watchers VALUES(?,?)',
		'INSERT INTO notes(
			ticket_id,
			reply_to,
			created_by,
			created_time,
			note)
		VALUES(?,(SELECT note_id FROM
			(SELECT note_id FROM notes WHERE ticket_id=?
				AND reply_to IS NULL) AS t),
			?,NOW(),?)'
		);
	$bind_params = array(
		array( 'ii',
			$ticket_id,
			$_SESSION['user']['user_id']
			),
		array( 'iiis',
			$ticket_id,
			$ticket_id,
			NULL,
			$note
			)
		);

	$status = multi_query( $queries, $bind_params );

	return( $status );
	}

function ignore( $ticket_id ) {
	if ( empty( $ticket_id ) ) {
		$errstr = ' - ticket_id empty';

		return( array( -1, $errstr ) );
		}

	$note = "Ignored by {$_SESSION['user']['user_name']}";
	$queries = array(
		'INSERT INTO notes(
			ticket_id,
			reply_to,
			created_by,
			created_time,
			note)
		VALUES((SELECT ticket_id FROM watchers WHERE ticket_id=?
			AND user_id=?),(SELECT note_id FROM (SELECT note_id
				FROM notes WHERE ticket_id=?
				AND reply_to IS NULL) AS t),
			?,NOW(),?)',
		'DELETE FROM watchers WHERE ticket_id=? AND user_id=?',
		);
	$bind_params = array(
		array( 'iiiis',
			$ticket_id,
			$_SESSION['user']['user_id'],
			$ticket_id,
			NULL,
			$note
			),
		array( 'ii',
			$ticket_id,
			$_SESSION['user']['user_id']
			)
		);

	$status = multi_query( $queries, $bind_params );

	return( $status );
	}

function setproject( $ticket ) {
	$errstr = '';

	if ( empty( $ticket['ticket_id'] ) ) {
		$errstr = '- ticket_id empty';
		}
	if ( empty( $ticket['project_id'] ) ) {
		if ( ! empty( $errstr ) ) {
			$errstr .= "\n";
			}

		$errstr .= ' - project_id empty';
		}

	if ( ! empty( $errstr ) ) {
		return( array( -1, $errstr ) );
		}

	if ( ! icanhaz( $ticket['ticket_id'] ) ) {
		return( array( -1, ' - Insufficient privileges' ) );
		}

	$queries = array(
		'UPDATE tickets SET project_id=?,owner=NULL WHERE ticket_id=?',
		'INSERT INTO notes(
			ticket_id,
			reply_to,
			created_by,
			created_time,
			note)
		VALUES(?,(SELECT note_id FROM
			(SELECT note_id FROM notes WHERE ticket_id=?
				AND reply_to IS NULL) AS t ),
			?,NOW(),CONCAT("Project changed from ",
				COALESCE((SELECT project_name FROM projects
					WHERE project_id=?),"undefined"),
				" to ",(SELECT project_name FROM projects
					WHERE project_id=?),
				" by ",?))'
		);
	$bind_params = array(
		array( 'ii',
			$ticket['project_id'],
			$ticket['ticket_id'],
			),
		array( 'iiiiis',
			$ticket['ticket_id'],
			$ticket['ticket_id'],
			NULL,
			$ticket['old_project_id'],
			$ticket['project_id'],
			$_SESSION['user']['user_name']
			),
		array( 'i',
			$ticket['project_id']
			)
		);

	$status = multi_query( $queries, $bind_params );

	if ( ! isset( $status[0] ) || $status[0] !== -1 ) {
		$query = 'SELECT project_name FROM projects
			WHERE project_id=?';
		$bind_param = array( 'i', $ticket['project_id'] );
		$status = query( $query, $bind_param );

		if ( ! isset( $status[0] ) || $status[0] !== -1 ) {
			notify( $ticket['ticket_id'], 'setproject',
				'Component changed to '
				. $status[0]['project_name'] );
			}
		}

	return( $status );
	}

function setcomponent( $ticket ) {
	$errstr = '';

	if ( empty( $ticket['ticket_id'] ) ) {
		$errstr = '- ticket_id empty';
		}
	if ( empty( $ticket['project_id'] ) ) {
		if ( ! empty( $errstr ) ) {
			$errstr .= "\n";
			}

		$errstr .= ' - project_id empty';
		}

	if ( ! empty( $errstr ) ) {
		return( array( -1, $errstr ) );
		}

	if ( ! icanhaz( $ticket['ticket_id'] ) ) {
		return( array( -1, ' - Insufficient privileges' ) );
		}

	$queries = array(
		'UPDATE tickets SET component_id=? WHERE ticket_id=?',
		'UPDATE tickets SET owner=NULL WHERE ticket_id=?',
		'INSERT INTO notes(
			ticket_id,
			reply_to,
			created_by,
			created_time,
			note)
		VALUES(?,(SELECT note_id FROM
			(SELECT note_id FROM notes WHERE ticket_id=?
				AND reply_to IS NULL) AS t ),
			?,NOW(),CONCAT("Component changed from ",
				COALESCE((SELECT component_name FROM components
					WHERE component_id=?),"undefined"),
				" to ",(SELECT component_name FROM components
					WHERE component_id=?),
				" by ",?))'
		);
	$bind_params = array(
		array( 'ii',
			$ticket['component_id'],
			$ticket['ticket_id'],
			),
		array( 'i',
			$ticket['ticket_id']
			),
		array( 'iiiiis',
			$ticket['ticket_id'],
			$ticket['ticket_id'],
			NULL,
			$ticket['old_component_id'],
			$ticket['component_id'],
			$_SESSION['user']['user_name']
			),
		array( 'i',
			$ticket['component_id']
			)
		);

	$status = multi_query( $queries, $bind_params );

	if ( ! isset( $status[0] ) || $status[0] !== -1 ) {
		$query = 'SELECT component_name FROM components
			WHERE component_id=?';
		$bind_param = array( 'i', $ticket['component_id'] );
		$status = query( $query, $bind_param );

		if ( ! isset( $status[0] ) || $status[0] !== -1 ) {
			notify( $ticket['ticket_id'], 'setcomponent',
				'Component changed to '
				. $status[0]['component_name'] );
			}
		}

	return( $status );
	}

function resolve( $ticket ) {
	if ( empty( $ticket['note'] ) ) {
		return( array( -1, ' - Note is a required field' ) );
		}
	if ( empty( $ticket['ticket_id'] ) ) {
		$errstr = '- ticket_id empty';

		return( array( -1, $errstr ) );
		}

	$queries = array(
		'INSERT INTO notes(
			ticket_id,
			reply_to,
			created_by,
			created_time,
			note) VALUES(?,(SELECT * FROM (SELECT MIN(note_id) FROM
				notes WHERE ticket_id=?) AS t),
			?,NOW(),?)',
		'UPDATE tickets SET resolved_time=NOW(),
			status=2 WHERE ticket_id=?',
		'INSERT INTO notes(
			ticket_id,
			reply_to,
			created_by,
			created_time,
			note)
		VALUES(?,(SELECT note_id FROM
			(SELECT note_id FROM notes WHERE ticket_id=?
				AND reply_to IS NULL) AS t ),
			?,NOW(),?)'
		);
	$bind_params = array(
		array( 'iiis',
			$ticket['ticket_id'],
			$ticket['ticket_id'],
			$_SESSION['user']['user_id'],
			$ticket['note']
			),
		array( 'i',
			$ticket['ticket_id']
			),
		array( 'iiis',
			$ticket['ticket_id'],
			$ticket['ticket_id'],
			NULL,
			"Resolved by {$_SESSION['user']['user_name']}"
			)
		);

	$status = multi_query( $queries, $bind_params );

	if ( ! isset( $status[0] ) || $status[0] !== -1 ) {
		notify( $ticket['ticket_id'], 'resolve', $ticket['note'] );
		}

	return( $status );
	}

function reject( $ticket ) {
	if ( empty( $ticket['note'] ) ) {
		return( array( -1, ' - Note is a required field' ) );
		}
	if ( empty( $ticket['ticket_id'] ) ) {
		$errstr = '- ticket_id empty';

		return( array( -1, $errstr ) );
		}

	$queries = array(
		'INSERT INTO notes(
			ticket_id,
			reply_to,
			created_by,
			created_time,
			note) VALUES(?,(SELECT * FROM (SELECT MIN(note_id) FROM
				notes WHERE ticket_id=?) AS t),
			?,NOW(),?)',
		'UPDATE tickets SET resolved_time=NOW(),
			status=3 WHERE ticket_id=?',
		'INSERT INTO notes(
			ticket_id,
			reply_to,
			created_by,
			created_time,
			note)
		VALUES(?,(SELECT note_id FROM
			(SELECT note_id FROM notes WHERE ticket_id=?
				AND reply_to IS NULL) AS t ),
			?,NOW(),?)'
		);
	$bind_params = array(
		array( 'iiis',
			$ticket['ticket_id'],
			$ticket['ticket_id'],
			$_SESSION['user']['user_id'],
			$ticket['note']
			),
		array( 'i',
			$ticket['ticket_id']
			),
		array( 'iiis',
			$ticket['ticket_id'],
			$ticket['ticket_id'],
			NULL,
			"Rejected by {$_SESSION['user']['user_name']}"
			)
		);

	$status = multi_query( $queries, $bind_params );

	if ( ! isset( $status[0] ) || $status[0] !== -1 ) {
		notify( $ticket['ticket_id'], 'reject', $ticket['note'] );
		}

	return( $status );
	}

function reopen( $ticket ) {
	if ( empty( $ticket['note'] ) ) {
		return( array( -1, ' - Note is a required field' ) );
		}
	if ( empty( $ticket['ticket_id'] ) ) {
		$errstr = '- ticket_id empty';

		return( array( -1, $errstr ) );
		}

	$queries = array(
		'UPDATE tickets SET resolved_time=NOW(),
			status=CASE WHEN owner IS NULL THEN 0 ELSE 1 END,
			resolved_time=NULL
			WHERE ticket_id=?',
		'INSERT INTO notes(
			ticket_id,
			reply_to,
			created_by,
			created_time,
			note)
		VALUES(?,(SELECT note_id FROM
			(SELECT note_id FROM notes WHERE ticket_id=?
				AND reply_to IS NULL) AS t ),
			?,NOW(),?)',
		'INSERT INTO notes(
			ticket_id,
			reply_to,
			created_by,
			created_time,
			note) VALUES(?,(SELECT * FROM (SELECT MIN(note_id) FROM
				notes WHERE ticket_id=?) AS t),
			?,NOW(),?)'
		);
	$bind_params = array(
		array( 'i',
			$ticket['ticket_id']
			),
		array( 'iiis',
			$ticket['ticket_id'],
			$ticket['ticket_id'],
			NULL,
			"Re-opened by {$_SESSION['user']['user_name']}"
			),
		array( 'iiis',
			$ticket['ticket_id'],
			$ticket['ticket_id'],
			$_SESSION['user']['user_id'],
			$ticket['note']
			)
		);

	$status = multi_query( $queries, $bind_params );

	if ( ! isset( $status[0] ) || $status[0] !== -1 ) {
		notify( $ticket['ticket_id'], 'reopen', $ticket['note'] );
		}

	return( $status );
	}

function random( $size ){
	$chars = array_merge( range( 'a', 'z' ), range( 'A', 'Z' ),
		range( '0', '9' ) );
	$rand = '';

	for( $i = 0; $i < $size; $i++ )
		$rand .= $chars[mt_rand( 0, count( $chars ) - 1 )];

	return( $rand );
	}

function hashpass( $password ) {
	$hash = crypt( $password, '$2a$08$' . random( 32 ) );

	return( $hash );
	}

function pager( $total, $pagereq ) {
	$pager = array();

	if ( isset( $pagereq['limit'] ) && $pagereq['limit'] > 0 ) {
		$pager['limit'] = $pagereq['limit'];
		}
	else {
		$pager['limit'] = 10;
		}

	if ( isset( $pagereq['start'] ) && $pagereq['start'] < $total ) {
		$pager['start'] = $pagereq['start'];
		}
	else {
		$pager['start'] = 0;
		}

	if ( ( $pager['start'] - $pager['limit'] ) > 0 ) {
		$pager['prev'] = $pager['start'] - $pager['limit'];
		}
	else {
		$pager['prev'] = 0;
		}

	if ( ( $pager['start'] + $pager['limit'] ) > $total ) {
		$pager['next'] = $pager['start'];

		if ( ( $pager['start'] + 1 ) === $total || $total === 0 ) {
			$pager['info'] = "Viewing $total of $total";
			}
		else {
			$pager['info'] = 'Viewing ' . ( $pager['start'] + 1 )
				. ' - ' . "$total of $total";
			}
		}
	else {
		$pager['next'] = $pager['start'] + $pager['limit'];

		$pager['info'] = 'Viewing ' . ( $pager['start'] + 1 ) . ' - '
			. ( $pager['start'] + $pager['limit'] ) . " of $total";
		}

	return( $pager );
	}

function ordernotes ( &$ticket ) {
	$notenum = count( $ticket );

	for ( $i = $notenum - 1; $i >= 0; $i-- ) {
		for ( $j = $notenum - 1; $j >= 0; $j-- ) {
			if ( $ticket[$i]['reply_to']
			=== $ticket[$j]['note_id'] ) {
				if ( ! isset( $ticket[$j]['children'] ) ) {
					$ticket[$j]['children'] = array();
					}

				array_unshift( $ticket[$j]['children'],
					$ticket[$i] );

				unset( $ticket[$i] );

				$notenum--;

				break;
				}
			}
		}
	}

function nestnotes( &$notes, &$level ) {
	list( $red, $green, $blue ) = getcolor( $level );

	foreach ( $notes as $note ) {
		$note['note'] = htmlspecialchars( $note['note'] );
		$indent = '';

		if ( file_exists(
		"avatars/{$note['notes_created_by']}.jpg" ) ) {
			$avatar = "avatars/{$note['notes_created_by']}.jpg";
			$avatar .= '?t=' . filemtime( $avatar );
			$pstyle = "style='min-height: 64px;'";
			}
		else {
			$avatar = FALSE;
			$pstyle = '';
			}

		for ( $i = $level; $i > 0; $i-- ) {
			$indent .= "  ";
			}

		if ( empty( $note['notes_creator'] ) ) {
			echo "$indent<div class='note system'>"
				. "{$note['notes_created_time']} -"
				. " {$note['note']}\n";
			}
		else {
			echo "$indent<div class='note'"
				. " id='note{$note['note_id']}'"
				. " style='border-left: 4px solid"
				. " rgba( $red, $green, $blue, .35 )'>\n"
				. "$indent  <div class='title'>"
				. "{$note['notes_created_time']} -"
				. " {$note['notes_creator']}";

			if ( $note['status'] < 2 ) {
				echo "<div class='titlebar-button reply'"
					. " onclick=\"reply(this,"
					. "'reply{$note['note_id']}')\">"
					. "</div>";
				}

			echo "<div class='titlebar-button hide'"
				. " onclick=\"hide(this,"
				. "'note{$note['note_id']}')\">"
				. "</div></div>\n"
				. "$indent  <p $pstyle>";

			if ( $avatar ) {
				echo "<img src='$avatar' style='float: left;"
				. " width: 64px; margin: 0 8px 8px 0;"
				. " border-radius: 3px;'>";
				}

			echo nl2br( $note['note'] ) . "</p>\n"
				. "$indent  <div id='reply{$note['note_id']}'"
				. " class='reply-form'>\n";

			if ( $note['status'] < 2 ) {
				echo "$indent    <form"
					. " id='reply{$note['note_id']}-form'"
					. " method='post'>\n"
					. "$indent      <input type='hidden'"
					. " name='form' value='reply'>\n"
					. "$indent      <input type='hidden'"
					. " name='reply_to'"
					. " value='{$note['note_id']}'>\n"
					. "$indent      <input type='hidden'"
					. " name='ticket_id'"
					. " value='{$note['ticket_id']}'>\n"
					. "$indent      <textarea name='note'>"
					. "</textarea>\n"
					. "$indent      <input class='buttons'"
					. " value='Save' type='submit'>\n"
					. "$indent      <input class='buttons"
					. " cancel' value='Cancel'"
					. " type='button' onclick=\""
					. "reply(this,"
					. "'reply{$note['note_id']}')\">\n"
					. "$indent    </form>\n";
				}

			echo "$indent  </div>\n";
			}

		if ( isset( $note['children'] ) ) {
			$level++;

			nestnotes( $note['children'], $level );

			$level--;
			}

		echo "$indent</div>\n";
		}
	}

function getcolor ( $val ) {
	$colors = array(
		array( 41, 133, 23 ),
		array( 133, 62, 62 ),
		array( 23, 78, 133 ),
		array( 133, 84, 36 ),
		array( 133, 49, 91 ),
		array( 133, 125, 89 ),
		array( 23, 23, 133 ),
		array( 114, 23, 133 ),
		array( 133, 103, 89 ),
		array( 103, 133, 89 ),
		array( 118, 89, 133 ),
		array( 133, 50, 9 ),
		array( 133, 116, 36 ),
		array( 9, 133, 112 ),
		array( 98, 172, 209 ),
		array( 112, 209, 15 ),
		array( 98, 117, 209 ),
		array( 209, 36, 180 ),
		array( 98, 209, 154 ),
		array( 209, 15, 79 ),
		array( 209, 122, 36 ),
		array( 98, 209, 209 ),
		array( 93, 36, 209 ),
		array( 209, 36, 36 ),
		array( 209, 149, 119 ),
		array( 194, 209, 119 ),
		array( 209, 151, 36 ),
		array( 209, 209, 36 ),
		array( 209, 98, 154 ),
		array( 154, 98, 209 ),
		array( 209, 79, 15 ),
		array( 209, 140, 140 )
		);

	return( $colors[$val] );
	}

function reply( $note ) {
	if ( $_SESSION['user']['priv_id'] < 1 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$errstr = '';

	if ( empty( $note['reply_to'] ) ) {
		$errstr .= " - reply to is a required field";
		}
	if ( empty( $note['ticket_id'] ) ) {
		if ( ! empty( $errstr ) ) {
			$errstr .= "\n";
			}

		$errstr .= " - ticket id is a required field";
		}
	if ( empty( $note['note'] ) ) {
		if ( ! empty( $errstr ) ) {
			$errstr .= "\n";
			}

		$errstr .= " - note is a required field";
		}

	if ( ! empty( $errstr ) ) {
		return( array( -1, $errstr ) );
		}

	$query = "INSERT INTO notes(
		ticket_id,
		reply_to,
		created_by,
		created_time,	
		note) VALUES((SELECT ticket_id FROM tickets WHERE
			ticket_id=? && status<2),?,?,NOW(),?)";
		
	$status = query( $query, array( 'iiis',
		$note['ticket_id'],
		$note['reply_to'],
		$_SESSION['user']['user_id'],
		$note['note']
		) );

	if ( ! isset( $status[0] ) || $status[0] !== -1 ) {
		notify( $note['ticket_id'], 'reply', $note['note'] );
		}

	return( $status );
	}

function getgadgets() {
	if ( $_SESSION['user']['priv_id'] < 1 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$query = "SELECT * FROM gadgets WHERE user_id=? AND project_id=?
		ORDER BY container,row";

	$gadgets = query( $query, array( 'ii',
		$_SESSION['user']['user_id'],
		$_SESSION['project']['project_id']
		) );

	return( $gadgets );
	}

function lsgadgets( $container ) {
	if ( $_SESSION['user']['priv_id'] < 1 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}
	$curgadgets = getgadgets();

	$files = scandir( './' );
	$count = 0;

/*
 * Need to figure out how to make the title work properly
 * here, but not that important right now
 *
 *	echo "<div class='title'>Gadgets</div>\n";
 */

	foreach ( $files as $file ) {
		if ( ! is_file( "$file" )
		|| substr( $file, 0, 1 ) === '.' || $file === 'error.php'
		|| $file === 'gadgets.php' ) {
			continue;
			}

		$gadget_name = str_replace( '.php', '', $file );

		foreach ( $curgadgets as $curgadget ) {
			if ( $gadget_name === $curgadget['gadget'] ) {
				continue( 2 );
				}
			}

		$count++;

		echo "<a href='addgadget.php"
			. "?gadget=$gadget_name"
			. "&container=$container'"
			. " style='background-image:"
			. " url(gadgets/images/$gadget_name.png);"
			. " background-position: center;'>"
			. "$gadget_name</a>\n";
		}

	if ( $count <= 0 ) {
		echo "<font color='red'><i>No gadgets remaining</i></font>\n";
		}
	}

function addgadget( $container, $gadget ) {
	if ( $_SESSION['user']['priv_id'] < 1 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$errstr = '';

	if ( empty( $container ) ) {
		$errstr .= " - container not supplied";
		}
	if ( empty( $gadget ) ) {
		if ( ! empty( $errstr ) ) {
			$errstr .= "\n";
			}

		$errstr .= " - gadget not supplied";
		}

	if ( ! empty( $errstr ) ) {
		return( array( -1, $errstr ) );
		}

	$queries = array(
		'UPDATE gadgets SET row=row+1 WHERE
			user_id=? AND project_id=? AND container=?
			ORDER BY row DESC',
		'INSERT INTO gadgets(
			user_id,
			project_id,
			container,
			row,
			gadget)
		VALUES(?,?,?,0,?)'
		);
	$bind_params = array(
		array( 'iis',
			$_SESSION['user']['user_id'],
			$_SESSION['project']['project_id'],
			$container
			),
		array( 'iiss',
			$_SESSION['user']['user_id'],
			$_SESSION['project']['project_id'],
			$container,
			$gadget
			)
		);

	$status = multi_query( $queries, $bind_params );

	return( $status );
	}

function rmgadget( $gadget_id ) {
	if ( $_SESSION['user']['priv_id'] < 1 ) {
		return( array( -1, 'Insufficient privileges' ) );
		}

	$errstr = '';

	if ( empty( $gadget_id ) ) {
		$errstr .= " - gadget id not supplied";
		return( array( -1, $errstr ) );
		}

	$queries = array(
		'DELETE FROM gadgets WHERE user_id=? AND gadget_id=?',
		'UPDATE gadgets SET row=row-1 WHERE user_id=? AND gadget_id=?
			ORDER BY row',
		);
	$bind_params = array(
		array( 'ii',
			$_SESSION['user']['user_id'],
			$gadget_id
			),
		array( 'ii',
			$_SESSION['user']['user_id'],
			$gadget_id
			)
		);

	$status = multi_query( $queries, $bind_params );

	return( $status );
	}

function getproto() {
	if ( ( ! empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] !== 'off')
	|| $_SERVER['SERVER_PORT'] == 443) {
		return TRUE;
		}

	return FALSE;
	}

function setgadget( $gadget ) {
//return( array( -1, print_r( $gadget, true ) ) );
	$query = 'SELECT container,row FROM gadgets WHERE user_id=?
		AND project_id=? AND gadget_id=?';
	$bind_param = array( 'iii',
		$_SESSION['user']['user_id'],
		$_SESSION['project']['project_id'],
		$gadget['gadget_id']
		);

	$status = query( $query, $bind_param );

	if ( isset( $status[0] ) && $status[0] === -1 ) {
		return( $status );
		}

	$oldcontainer = $status[0]['container'];
	$oldrow = $status[0]['row'];

	$queries = array( 'DELETE FROM gadgets WHERE user_id=?
		AND project_id=? AND gadget_id=?',
		'UPDATE gadgets SET row=row-1 WHERE user_id=?
			AND project_id=? AND container=? AND row>?
			ORDER BY row ASC',
		'UPDATE gadgets SET row=row+1 WHERE user_id=?
			AND project_id=? AND container=? AND row>=?
			ORDER BY row DESC',
		'INSERT INTO gadgets(
			gadget_id,
			user_id,
			project_id,
			container,
			row,
			gadget,
			httpvars) VALUES(?,?,?,?,?,?,?)'
		 );
	$bind_params = array(
		array( 'iii',
			$_SESSION['user']['user_id'],
			$_SESSION['project']['project_id'],
			$gadget['gadget_id']
			),
		array( 'iisi',
			$_SESSION['user']['user_id'],
			$_SESSION['project']['project_id'],
			$oldcontainer,
			$oldrow
			),
		array( 'iisi',
			$_SESSION['user']['user_id'],
			$_SESSION['project']['project_id'],
			$gadget['container'],
			$gadget['row']
			),
		array( 'iiisiss',
			$gadget['gadget_id'],
			$_SESSION['user']['user_id'],
			$gadget['project_id'],
			$gadget['container'],
			$gadget['row'],
			$gadget['id'],
			$gadget['httpvars']
			)
		);

	$status = multi_query( $queries, $bind_params );

	return( $status );
	}

function setgadgets( $gadgets ) {
	$queries = array( 'DELETE FROM gadgets WHERE user_id=?
		AND project_id=?' );
	$bind_params = array(
		array( 'ii',
			$_SESSION['user']['user_id'],
			$_SESSION['project']['project_id']
			)
		);

	foreach ( $gadgets as $gadget ) {
		if ( ! isset( $gadget['gadget_id'] ) ) {
			array_push( $queries,
				'INSERT INTO gadgets(
					user_id,
					project_id,
					container,
					row,
					gadget,
					httpvars) VALUES(?,?,?,?,?,?)' );

			array_push( $bind_params, array( 'iisiss',
				$_SESSION['user']['user_id'],
				$gadget['project_id'],
				$gadget['container'],
				$gadget['row'],
				$gadget['id'],
				$gadget['httpvars']
				) );
			}
		else {
			array_push( $queries,
				'INSERT INTO gadgets(
					gadget_id,
					user_id,
					project_id,
					container,
					row,
					gadget,
					httpvars) VALUES(?,?,?,?,?,?,?)' );

			array_push( $bind_params, array( 'iiisiss',
				$gadget['gadget_id'],
				$_SESSION['user']['user_id'],
				$gadget['project_id'],
				$gadget['container'],
				$gadget['row'],
				$gadget['id'],
				$gadget['httpvars']
				) );
			}
		}

	$status = multi_query( $queries, $bind_params );

	return( $status );
	}

function icanhaz( $ticket_id ) {
	if ( $_SESSION['user']['priv_id'] == 2 ) {
		return( TRUE );
		}

	$query = "SELECT SUM(
		project_admin+group_user+component_user+component_group)
		AS count FROM
		(SELECT count(*) AS project_admin FROM tickets a LEFT JOIN
			project_admins b ON a.project_id=b.project_id
			WHERE a.ticket_id=? AND b.user_id=?) t1,
		(SELECT count(*) AS group_user FROM tickets a LEFT JOIN
			project_groups b ON a.project_id=b.project_id
			LEFT JOIN group_users c ON b.group_id=c.group_id
			WHERE a.ticket_id=? AND c.user_id=?) t2,
		(SELECT count(*) AS component_user FROM tickets a LEFT JOIN
			component_users b ON a.component_id=b.component_id
			WHERE a.ticket_id=? AND b.user_id=?) t3,
		(SELECT count(*) AS component_group FROM tickets a LEFT JOIN
			component_groups b ON a.component_id=b.component_id
			LEFT JOIN group_users c ON b.group_id=c.group_id
			WHERE a.ticket_id=? AND c.user_id=?) t4";
	$status = query( $query, array( 'iiiiiiii',
		$ticket_id,
		$_SESSION['user']['user_id'],
		$ticket_id,
		$_SESSION['user']['user_id'],
		$ticket_id,
		$_SESSION['user']['user_id'],
		$ticket_id,
		$_SESSION['user']['user_id']
		) );

	if ( isset( $status[0]['count'] ) && $status[0]['count'] > 0 ) {
		return( TRUE );
		}

	return( FALSE );
	}

function resize( $file, $size ) {
	list( $oldwidth, $oldheight ) = getimagesize( $file );
	$type = exif_imagetype( $file );
	$data = file_get_contents( $file );
	$img = imagecreatefromstring( $data );
	$tmp = imagecreatetruecolor( $size, $size );
	$x = 0;
	$y = 0;

	if ( $oldwidth > $oldheight ) {
		$x = ( $oldwidth - $oldheight ) / 2;
		$oldsize = $oldheight;
		}
	elseif ( $oldwidth < $oldheight ) {
		$y = ( $oldheight - $oldwidth ) / 2;
		$oldsize = $oldwidth;
		}
	else {
		$oldsize = $oldwidth;
		}

	imagecopyresampled( $tmp, $img, 0, 0, $x, $y, $size, $size,
		$oldsize, $oldsize );

	imagejpeg( $tmp, $file, 100 );

	imagedestroy( $img );
	imagedestroy( $tmp );
	}

function notify( $ticket_id, $action, $notes ) {
	global $conf;
	$subject = "SnapTrack [$ticket_id]: ";
	$body = "Ticket ID: $ticket_id\r\n";

	if ( $action === 'take' ) {
		$subject .= "Taken";
		$body .= "Taken by: " . $_SESSION['user']['user_name'];
		}
	elseif ( $action === 'steal' ) {
		$subject .= "Stolen";
		$body .= "Stolen by: " . $_SESSION['user']['user_name'];
		}
	elseif ( $action === 'reply' ) {
		$subject .= "Reply";
		$body .= "Reply by: " . $_SESSION['user']['user_name'];
		}
	elseif ( $action === 'resolve' ) {
		$subject .= "Resolved";
		$body .= "Resolved by: " . $_SESSION['user']['user_name'];
		}
	elseif ( $action === 'reject' ) {
		$subject .= "Rejected";
		$body .= "Rejected by: " . $_SESSION['user']['user_name'];
		}
	elseif ( $action === 'setcomponent' ) {
		$subject .= "Component changed";
		$body .= "Component changed by: "
			. $_SESSION['user']['user_name'];
		}

	$body .= "\r\nDate: " . date("Y-m-d H:i:s") . "\r\n\r\n";

	if ( ! empty( $notes ) ) {
		$body .= htmlentities( $notes );
		}

	$creator = getcreator( $ticket_id );
	$owner = getowner( $ticket_id );
	$watchers = getwatchers( $ticket_id );
	$to = $creator[0]['email'] . ',' . $owner[0]['email'];
	$headers = "From: {$conf['general']['email']}\r\n"
		. "Reply-To: {$conf['general']['email']}\r\n"
		. 'X-Mailer: PHP/' . phpversion();

	if ( $creator[0]['email'] === $owner[0]['email'] ) {
		$to = $creator[0]['email'];
		}
	else {
		$to = $creator[0]['email'] . ',' . $owner[0]['email'];
		}
	foreach ( $watchers as $watcher ) {
		$to .= ",{$watcher['email']}";
		}

	mail( $to, $subject, $body, $headers );
	}

?>
