<?php
$menu = array(
	"{$_SESSION['user']['user_name']}" => array(
		'priv_id' => array( 1, 2 ),
		'uri' => 'account.php',
		'submenu' => array(
			'profile' => array(
				'img' => 'images/user.png',
				'uri' => 'profile.php'
				),
			'logout' => array(
				'img' => 'images/logout.png',
				'uri' => 'logout.php'
				)
			)
		),
	'settings' => array(
		'img' => 'images/admin.png',
		'priv_id' => array( 2 ),
		'uri' => 'settings.php',
		'submenu' => array(
			'projects' => array(
				'img' => 'images/compass.png',
				'uri' => 'projects.php'
				),
			'groups' => array(
				'img' => 'images/group.png',
				'uri' => 'groups.php'
				),
			'users' => array(
				'img' => 'images/user.png',
				'uri' => 'users.php'
				)
			)
		)
	);

if ( isset( $_SESSION['user']['priv_id'] )
&& $_SESSION['user']['priv_id'] > 0 ) {
	$projects = getprojects( NULL, NULL, NULL );

	echo "  <div id='projectselect'>"
		. "{$_SESSION['project']['project_name']}\n"
		. "    <div id='projectlist'>\n";

	foreach ( $projects as $project ) {
		if ( $project['project_id']
		=== $_SESSION['project']['project_id'] ) {
			continue;
			}

		echo "      <form class='project' method='post'>"
			. "        <input type='hidden' name='session_project'"
			. " value='{$project['project_id']}'>\n"
			. "        <input type='submit'"
			. " value='{$project['project_name']}'></form>\n";
		}

	echo "    </div>
  </div>
  <form style='position: absolute; border: 0px;' method='post'>
    <input type='text' id='ticketsearch' name='ticketsearch'>
  </form>\n";
	}

echo "  <div id='logo'><a href='./'></a></div>
  <div id='menu'>\n";

foreach ( $menu as $link => $attr ) {
	if ( ! in_array( $_SESSION['user']['priv_id'], $attr['priv_id'] ) ) {
		continue;
		}

	if ( $_SESSION['current_uri'] === str_replace( '/', '', $attr['uri'] ) )
		$class = 'menulink on';
	else
		$class = 'menulink';

	if ( isset( $attr['img'] ) ) {
		$link = "<img src='{$attr['img']}' alt='$link'>";
		}

	echo "    <div class='$class'><a href='{$attr['uri']}'>$link</a>";

	if ( count( $attr{'submenu'} ) > 0 ) {
		echo "\n      <div class='submenu'>\n";

		foreach ( $attr{'submenu'} as $sublink => $subattr ) {
			echo "        <div class='submenulink'>"
				. "<a href='{$subattr['uri']}'>"
				. "$sublink";

			if ( isset( $subattr['img'] ) ) {
				echo "<img src='{$subattr['img']}'"
					. " alt='$sublink'>";
				}

			echo "</a></div>\n";
			}

		echo "      </div>\n    ";
		}

	echo "</div>\n";
	}

echo "  </div>\n";
?>
