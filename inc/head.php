<!DOCTYPE html>
<html>
<head>
  <title>snaptrack</title>
  <meta http-equiv='content-type' content='text/html; charset=utf-8'>
  <style type='text/css'>@import url(css/style.css);</style>
  <script type='text/javascript' src='js/functions.js'></script>
  <link rel="icon" type="image/png" href="images/favicon.png">
</head>
<body>
<?php
$tmpclass = '';

if ( isset( $_SESSION['status']['code'] )
&& $_SESSION['status']['code'] !== 0 ) {
	$tmpclass = "class='blur'";

	echo "<div id='overlay' style='display: block; opacity: 1;'>\n";

	include( 'forms/error.php' );

	echo "</div>\n";

	$_SESSION['status'] = array(
		'func' => '',
		'code' => 0,
		'errstr' => ''
		);
	}

echo "<div id='header' $tmpclass>\n";

include( 'menu.php' );

echo "</div>
<div id='content' $tmpclass>\n";
?>
