<?php
/*
 * General configuration
 *
 * 'baseuri': this is mainly used to provide links in email
 * 'email': this is the email seen in the 'From:' field of notifications
 * 'session_timeout': length of time sessions are to remain active
 */
$conf['general']['baseurl'] = '';
$conf['general']['email'] = 'snaptrack@example.com';
$conf['general']['session_timeout'] = 86400;

/*
 * Database configuration
 *
 * 'host': defines the host where the database resides
 * 'database': the name of the database
 * 'user': database user login
 * 'pass': password for the database user
 */
$conf['db']['host'] = 'localhost';
$conf['db']['database'] = 'snaptrack';
$conf['db']['user'] = '';
$conf['db']['pass'] = '';
?>
