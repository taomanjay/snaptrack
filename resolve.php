<?php
include( 'inc/session.php' );

if ( $_SESSION['user']['user_id'] < 1 ) {
	die();
	}

if ( isset( $_GET['ticket_id'] ) ) {
	$ticket_id = $_GET['ticket_id'];

	$status = resolve( $ticket_id );

	if ( isset( $status[0] ) && $status[0] !== 0 ) {
		$_SESSION['status'] = array(
			'func' => 'resolve()',
			'code' => $status[0],
			'errstr' => $status[1]
			);
		}

	header( "Location: ticket.php?ticket_id=$ticket_id" );

	die();
	}
else {
	$_SESSION['status'] = array(
		'func' => 'resolve.php',
		'code' => -1,
		'errstr' => '- ticket_id empty'
		);

	header( 'Location: ./' );
	}
?>
