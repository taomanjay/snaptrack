<?php
require( 'inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 2 ) {
	header( 'Location: ./' );

	die();
	}

include( 'inc/head.php' );

echo "<div class='gadget single'>
  <div class='title'>Users</div>
  <div class='gadgetcontent'>\n";
require( 'gadgets/users.php' );
echo "  </div>
</div>\n";

include( 'inc/foot.php' );
?>
