<?php
require( 'inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 2 ) {
	header( 'Location: ./' );

	die();
	}

if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
	if ( isset( $_POST['error'] ) && $_POST['error'] === 'true' ) {
		$status = array( 0, '' );
		}
	elseif ( isset( $_POST['group_id'] ) ) {
		if ( isset( $_POST['delete'] ) ) {
			$status = delgroup( $_POST );
			}
		else{
			$status = editgroup( $_POST );
			}
		}
	else {
		$status = newgroup( $_POST );
		}

	if ( empty( $status ) || $status[0] === 0 ) {
		header( 'Location: groups.php' );

		die();
		}
	}
echo "WTF: ";
print_r( $status );

include( 'inc/head.php' );

require( 'forms/editgroup.php' );

include( 'inc/foot.php' );
?>
