<?php
include( 'inc/session.php' );

if ( $_SESSION['user']['user_id'] < 1 ) {
	die();
	}

if ( isset( $_GET['gadget'] ) ) {
	$gadget = $_GET['gadget'];
	}
else {
	die();
	}

if ( isset( $_GET['container'] ) ) {
	$container = $_GET['container'];
	}
else {
	die();
	}

$status = addgadget( $container, $gadget );

if ( isset( $status[0] ) && $status[0] !== 0 ) {
	$_SESSION['status'] = array(
		'func' => 'addgadget()',
		'code' => $status[0],
		'errstr' => $status[1]
		);
	}

header( "Location: index.php" );
?>
