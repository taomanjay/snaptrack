<?php
require( 'inc/session.php' );

if ( $_SESSION['user']['priv_id'] > 0 ) {
	header( 'Location: ./' );

	die();
	}

if ( ! empty( $_POST['user_name'] ) && ! empty( $_POST['password'] ) ) {
	$user = login( $_POST['user_name'], $_POST['password'] );

	if ( isset( $user['user_id'] ) ) {
		$_SESSION['user']['user_id'] = $user['user_id'];
	        $_SESSION['user']['user_name'] = $user['user_name'];
	        $_SESSION['user']['priv_id'] = $user['priv_id'];

		if ( isset( $_SESSION['user']['uri'] ) ) {
			header( "Location: {$_SESSION['user']['uri']}" );
			}
		else {
		        header( "Location: index.php" );
			}
	        die();
	        }
	else {
		$_SESSION['status'] = array(
			'func' => 'login()',
			'code' => -1,
			'errstr' => 'Login Failed'
			);
		}
	}

include( 'inc/head.php' );
include( 'forms/login.php' );
include( 'inc/foot.php' );
?>
