<?php
require( 'inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 2 ) {
	header( 'Location: ./' );

	die();
	}

$status = rmuser( $_GET );

if ( isset( $status[0] ) && $status[0] !== 0 ) {
	$_SESSION['status'] = array(
		'func' => 'rmuser()',
		'code' => $status[0],
		'errstr' => $status[1]
		);
	}

header( "Location: {$_SERVER['HTTP_REFERER']}" );
?>
