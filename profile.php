<?php
require( 'inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 1 ) {
	header( 'Location: ./' );

	die();
	}

if ( isset( $_GET['user_id'] ) ) {
	$user_id = $_GET['user_id'];
	$user = getuser( $_GET['user_id'] );
	}
else {
	$user_id = $_SESSION['user']['user_id'];
	$user = getuser( $_SESSION['user']['user_id'] );
	}

include( 'inc/head.php' );

require( 'forms/profile.php' );

include( 'inc/foot.php' );
?>
