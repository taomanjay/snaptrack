<?php
$gadget = 'oldtickets';

if ( basename( getcwd() ) === 'gadgets' ) {
	require_once( '../inc/session.php' );

	$single = FALSE;

	if ( isset( $_GET['gadget_id'] ) ) {
		$gadget_id = $_GET['gadget_id'];
		}
	else {
		die();
		}

	if ( isset( $_GET['container'] ) ) {
		$container = $_GET['container'];
		}
	else {
		die();
		}
	}
else {
	require_once( 'inc/session.php' );

	$single = TRUE;
	$gadget_id = '';
	$container = '';
	}

if ( $_SESSION['user']['priv_id'] < 1 ) {
	header( 'HTTP/1.1 401 Unauthorized' );
	die( 'You do not have permission to view this resource' );
	}

$sort = array();

if ( isset( $_GET['field'] ) ) {
	$sort['field'] = $_GET['field'];
	}
else {
	$sort['field'] = 'created_time';
	}
if ( isset( $_GET['order'] ) ) {
	$sort['order'] = $_GET['order'];
	}
else {
	$sort['order'] = 'ASC';
	}

if ( isset( $_GET['searchstring'] ) ) {
	$searchstring = $_GET['searchstring'];
	}
else {
	$searchstring = NULL;
	}

$pager = pager( getticketcount( $gadget, $searchstring ), $_GET );
$tickets = gettickets( $gadget, $pager, $sort, $searchstring );

if ( isset( $tickets[0] ) && $tickets[0] === -1 ) {
	header( 'HTTP/1.1 500 Internal server error' );
	die( "ERROR: in gettickets():\n\n" . $tickets[1] );
	}

/*
 * Here we're just populating an array that describes the TH fields
 * This should make it a little nicer to add/remove fields if needed
 * without having to mess with the sort logic
 */

$fields = array(
	'ticket_id' => array(
		'title' => 'Ticket ID',
		'sortable' => TRUE,
		'containers' => array(
			'leftcol',
			'rightcol'
			)
		),
	'component' => array(
		'title' => 'Component',
		'sortable' => TRUE,
		'containers' => array(
			'leftcol',
			'rightcol'
			)
		),
	'title' => array(
		'title' => 'Title',
		'sortable' => TRUE,
		'containers' => array(
			'leftcol',
			'rightcol'
			)
		),
	'owner' => array(
		'title' => 'Owner',
		'sortable' => TRUE,
		'containers' => array(
			'leftcol'
			)
		),
	'creator' => array(
		'title' => 'Creator',
		'sortable' => TRUE,
		'containers' => array(
			'leftcol'
			)
		),
	'created_time' => array(
		'title' => 'Date',
		'sortable' => TRUE,
		'containers' => array(
			'leftcol'
			)
		),
	);

echo "<table class='$container'>
  <thead>
    <tr>\n";

foreach ( $fields as $field => $values ) {
	if ( ! in_array( $container, $values['containers'] ) ) {
		continue;
		}
	
	if ( ! $values['sortable'] ) {
		echo "      <th>{$values['title']}</th>\n";

		continue;
		}

	$link = "?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start=0"
		. "&limit={$pager['limit']}"
		. "&field=$field";

	if ( ! isset( $sort['order'] ) ) {
		$link .= '&order=DESC';
		}
	else if ( $sort['order'] == 'ASC' && $sort['field'] == $field ) {
		$link .= "&order=DESC";
		}
	else {
		$link .= "&order=ASC";
		}

	if ( isset( $searchstring ) ) {
		$link .= "&searchstring=$searchstring";
		}

	echo "      <th class='sort' onclick=\"updateGadget('$gadget','$link')\">"
		. "{$values['title']}\n"
		. "        <div class='arrows'>\n";

	if ( ! ( $field == $sort['field'] && $sort['order'] == 'DESC' )  ) {
		echo "          <div class='sortup'></div>\n";
		}
	if ( ! ( $field == $sort['field'] && $sort['order'] == 'ASC' )  ) {
		echo "          <div class='sortdown'></div>\n";
		}

	echo "        </div>"
		. "      </th>\n";
	}

echo "    </tr>
  </thead>
  <tbody>\n";

foreach ( $tickets as $ticket ) {
	$ticket['title'] = htmlspecialchars( $ticket['title'],
		ENT_COMPAT|ENT_SUBSTITUTE );

	echo "    <tr>\n"
		. "<td><a href='ticket.php?ticket_id="
		. "{$ticket['ticket_id']}'>{$ticket['ticket_id']}</a></td>\n"
		. "<td>{$ticket['component']}</td>\n"
		. "<td><div class='short'>"
		. "<div class='tooltip'>{$ticket['title']}</div>"
		. "{$ticket['title']}</div></td>\n";

	if ( $container === 'leftcol' ) {
		echo "<td>{$ticket['owner']}</td>\n"
			. "<td>{$ticket['creator']}</td>\n"
			. "<td style='white-space: nowrap;'>"
			. "{$ticket['created_time']}</td>\n";
		}

	echo "    </tr>\n";
	}

if ( count( $tickets ) <= 0 ) {
	echo "<tr>\n"
		. "<td colspan=5 style='color: red; font-style: italic;'>"
		. "No tickets found</td>"
		. "</tr>\n";
	}

echo "    <tr>
      <td colspan=6 style='padding: 10px 0 0 0;'>
        <select class='limit' onchange=\"this.blur();"
	. "updateGadget('$gadget','?gadget_id=$gadget_id"
	. "&container=$container"
	. "&start={$pager['start']}"
	. "&limit='+this.value"
        . "+'&field={$sort['field']}"
        . "&order={$sort['order']}"
	. "&searchstring=$searchstring');\">\n";

for ( $i = 10; $i <= 50; $i += 10 ) {
	echo "          <option value=$i";

	if ( $i === ( int ) $pager['limit'] ) {
		echo " selected";
		}

	echo ">$i</option>\n";
	}

echo "        </select>
        <div onclick=\"updateGadget('$gadget','?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['prev']}"
		. "&limit={$pager['limit']}"
		. "&field={$sort['field']}"
		. "&order={$sort['order']}"
		. "&searchstring=$searchstring')\" class='page-left'></div>
        <div onclick=\"updateGadget('$gadget','?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['next']}"
		. "&limit={$pager['limit']}"
		. "&field={$sort['field']}"
		. "&order={$sort['order']}"
		. "&searchstring=$searchstring')\" class='page-right'></div>
        <div class='viewing'>{$pager['info']}</div>
        <div class='new' onclick=\"drawForm('newticket','')\">new ticket</div>
      </td>
    </tr>
  </tbody>
</table>\n";
?>
