<?php
$gadget = 'usertally';

if ( basename( getcwd() ) === 'gadgets' ) {
	require_once( '../inc/session.php' );

	$single = FALSE;

       if ( isset( $_GET['gadget_id'] ) ) {
		$gadget_id = $_GET['gadget_id'];
		}
	else {
		die();
		}

	if ( isset( $_GET['container'] ) ) {
		$container = $_GET['container'];
		}
	else {
		die();
		}
	}
else {
	require_once( 'inc/session.php' );

	$single = TRUE;
	$gadget_id = '';
	$container = '';
	}

if ( $_SESSION['user']['priv_id'] < 1 ) {
	header( 'HTTP/1.1 401 Unauthorized' );
	die( 'You do not have permission to view this resource' );
	}

$sort = array();

if ( isset( $_GET['field'] ) ) {
	$sort['field'] = $_GET['field'];
	}
else {
	$sort['field'] = 'user_name';
	}
if ( isset( $_GET['order'] ) ) {
	$sort['order'] = $_GET['order'];
	}
else {
	$sort['order'] = 'ASC';
	}

if ( isset( $_GET['searchstring'] ) ) {
	$searchstring = $_GET['searchstring'];
	}
else {
	$searchstring = NULL;
	}

$pager = pager( getusercount( $searchstring ), $_GET );
$usertally = usertally( $pager, $sort, $searchstring );

if ( isset( $usertally[0] ) && $usertally[0] === -1 ) {
	header( 'HTTP/1.1 500 Internal server error' );
	die( "ERROR: in getusers():\n\n" . $usertally[1] );
	}

/*
 * Here we're just populating an array that describes the TH fields
 * This should make it a little nicer to add/remove fields if needed
 * without having to mess with the sort logic
 */

$fields = array(
	'user_name' => array(
		'title' => 'User',
		'sortable' => TRUE,
		'containers' => array(
			'leftcol',
			'rightcol'
			)
		),
	'opennum' => array(
		'title' => 'Open',
		'sortable' => TRUE,
		'containers' => array(
			'leftcol',
			'rightcol'
			)
		),
	'resolvednum' => array(
		'title' => 'Resolved',
		'sortable' => TRUE,
		'containers' => array(
			'leftcol',
			'rightcol'
			)
		)
	);

echo "  <table class='$container'>
    <thead>
      <tr>\n";

foreach ( $fields as $field => $values ) {
	if ( ! in_array( $container, $values['containers'] ) && ! $single ) {
		continue;
		}
	
	if ( ! $values['sortable'] ) {
		echo "      <th>{$values['title']}</th>\n";

		continue;
		}

	$link = "?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['prev']}"
		. "&limit={$pager['limit']}"
		. "&field=$field";

	if ( ! isset( $sort['order'] ) ) {
		$link .= '&order=DESC';
		}
	else if ( $sort['order'] == 'ASC' ) {
		$link .= "&order=DESC";
		}
	else {
		$link .= "&order=ASC";
		}

	if ( isset( $searchstring ) ) {
		$link .= "&searchstring=$searchstring";
		}

	echo "      <th class='sort' onclick=\""
		. "updateGadget('$gadget','$link')\">"
		. "{$values['title']}\n"
		. "        <div class='arrows'>\n";

	if ( ! ( $field == $sort['field'] && $sort['order'] == 'DESC' )  ) {
		echo "          <div class='sortup'></div>\n";
		}
	if ( ! ( $field == $sort['field'] && $sort['order'] == 'ASC' )  ) {
		echo "          <div class='sortdown'></div>\n";
		}

	echo "        </div>"
		. "      </th>\n";
	}

echo "      </tr>
    </thead>
    <tbody>\n";

foreach ( $usertally as $user ) {
	if ( $user['disabled'] > 0 ) {
		$class = 'strike';
		}
	else {
		$class = '';
		}

	echo "      <tr>\n"
		. "        <td><span class='$class'>"
		. "{$user['user_name']}</span></td>\n"
		. "      <td>{$user['opennum']}</td>\n"
		. "        <td>{$user['resolvednum']}</td>\n";
	}

if ( count( $usertally ) <= 0 ) {
	echo "<tr>\n"
		. "<td colspan=7 style='color: red; font-style: italic;'>"
		. "No users found</td>"
		. "</tr>\n";
	}

echo "      <tr>
        <td colspan=7 style='padding: 10px 0 0 0;'>
          <select class='limit' onchange=\"this.blur();";

if ( $single ) {
	echo "location.href='users.php"
		. "?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['start']}"
		. "&limit='+this.value"
		. "+'&field={$sort['field']}"
		. "&order={$sort['order']}"
		. "&searchstring=$searchstring';\">\n";
	}
else {
	echo "updateGadget('$gadget','?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['start']}"
		. "&limit='+this.value"
                . "+'&field={$sort['field']}"
                . "&order={$sort['order']}"
		. "&searchstring=$searchstring');\">\n";
	}

for ( $i = 10; $i <= 50; $i += 10 ) {
	echo "            <option value=$i";

	if ( $i === ( int ) $pager['limit'] ) {
		echo " selected";
		}

	echo ">$i</option>\n";
	}

echo "          </select>\n";

if ( $single == FALSE ) {
	echo "          <div onclick=\"updateGadget('$gadget',"
		. "'?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['prev']}"
		. "&limit={$pager['limit']}"
		. "&field={$sort['field']}"
		. "&order={$sort['order']}"
		. "&searchstring=$searchstring')\""
		. " class='page-left'></div>\n"
		. "          <div onclick=\"updateGadget('$gadget',"
		. "'?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['next']}"
		. "&limit={$pager['limit']}"
		. "&field={$sort['field']}"
		. "&order={$sort['order']}"
		. "&searchstring=$searchstring')\""
		. " class='page-right'></div>\n";
	}
else {
	echo "          <a href='users.php?start={$pager['prev']}"
		. "&limit={$pager['limit']}' class='page-left'></a>\n"
		. "          <a href='users.php?start={$pager['next']}"
		. "&limit={$pager['limit']}' class='page-right'></a>\n";
	}

echo "          <div class='viewing'>{$pager['info']}</div>
        </td>
      </tr>
    </tbody>
  </table>\n";
?>
