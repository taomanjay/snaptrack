<?php
$gadget = 'projects';

if ( basename( getcwd() ) === 'gadgets' ) {
	require_once( '../inc/session.php' );

	$single = FALSE;

	if ( isset( $_GET['gadget_id'] ) ) {
		$gadget_id = $_GET['gadget_id'];
		}
	else {
		die();
		}

	if ( isset( $_GET['container'] ) ) {
		$container = $_GET['container'];
		}
	else {
		die();
		}
	}
else {
	require_once( 'inc/session.php' );

	$single = TRUE;
	$gadget_id = '';
	$container = '';
	}

if ( $_SESSION['user']['priv_id'] < 2 ) {
	header( 'HTTP/1.1 401 Unauthorized' );
	die( 'You do not have permission to view this resource' );
	}

$sort = array();

if ( isset( $_GET['field'] ) ) {
	$sort['field'] = $_GET['field'];
	}
else {
	$sort['field'] = 'created_time';
	}
if ( isset( $_GET['order'] ) ) {
	$sort['order'] = $_GET['order'];
	}
else {
	$sort['order'] = 'ASC';
	}

if ( isset( $_GET['searchstring'] ) ) {
	$searchstring = $_GET['searchstring'];
	}
else {
	$searchstring = NULL;
	}

$pager = pager( getprojectcount( $searchstring ), $_GET );
$projects = getprojects( $pager, $sort, $searchstring );

if ( isset( $projects[0] ) && $projects[0] === -1 ) {
	header( 'HTTP/1.1 500 Internal server error' );
	die( "ERROR: in getprojects():\n\n" . $projects[1] );
	}

$fields = array(
        'project_name' => array(
                'title' => 'Project',
                'sortable' => TRUE,
                'containers' => array(
                        'leftcol',
                        'rightcol'
                        )
                ),
        'ticketnum' => array(
                'title' => 'Open Tickets',
                'sortable' => TRUE,
                'containers' => array(
                        'leftcol',
                        'rightcol'
                        )
                ),
	'adminnum' => array(
		'title' => 'Admins',
		'sortable' => TRUE,
		'containers' => array(
			'leftcol'
			)
		),
	'groupnum' => array(
		'title' => 'Groups',
		'sortable' => TRUE,
		'containers' => array(
			'leftcol'
			)
		),
	'componentnum' => array(
		'title' => 'Components',
		'sortable' => TRUE,
		'colspan' => 2,
		'containers' => array(
			'leftcol',
			'rightcol'
			)
		),
	'edit' => array(
		'title' => '',
		'sortable' => FALSE,
		'containers' => array(
			'leftcol',
			'rightcol'
			)
		)
	);

echo "  <table class='$container'>
    <thead>
      <tr>\n";

foreach ( $fields as $field => $values ) {
	if ( ! in_array( $container, $values['containers'] ) && ! $single ) {
		continue;
		}

	if ( isset( $values['colspan'] ) ) {
		$colspan = "colspan={$values['colspan']}";
		}
	else {
		$colspan = '';
		}
	
	if ( ! $values['sortable'] ) {
		echo "      <th $colspan>{$values['title']}</th>\n";

		continue;
		}

	$link = "?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['prev']}"
		. "&limit={$pager['limit']}"
		. "&field=$field";

	if ( ! isset( $sort['order'] ) ) {
		$link .= '&order=DESC';
		}
	else if ( $sort['order'] == 'ASC' ) {
		$link .= "&order=DESC";
		}
	else {
		$link .= "&order=ASC";
		}

	if ( isset( $searchstring ) ) {
		$link .= "&searchstring=$searchstring";
		}

	echo "      <th class='sort' onclick=\"updateGadget('$gadget','$link')\""
		. " $colspan>{$values['title']}\n"
		. "        <div class='arrows'>\n";

	if ( ! ( $field == $sort['field'] && $sort['order'] == 'DESC' )  ) {
		echo "          <div class='sortup'></div>\n";
		}
	if ( ! ( $field == $sort['field'] && $sort['order'] == 'ASC' )  ) {
		echo "          <div class='sortdown'></div>\n";
		}

	echo "        </div>"
		. "      </th>\n";
	}

echo "      </tr>
    </thead>
    <tbody>\n";

foreach ( $projects as $project ) {
	echo "      <tr>\n"
		. "        <td id='project_name{$project['project_id']}'>"
		. "{$project['project_name']}</td>\n"
		. "        <td>{$project['ticketnum']}</td>\n";

	if ( $single || $container === 'leftcol' ) {
		echo "        <td style='max-width: 400px;'>"
		. "{$project['adminnum']}</td>\n"
		. "        <td>{$project['groupnum']}</td>\n";
		}

	echo "        <td style='width: 56px;'>"
		. "<div class='toggle components' onclick=\""
		. "drawForm('components',"
		. "'?project_id={$project['project_id']}');\">edit"
		. "</div></td>\n"
		. "        <td>{$project['componentnum']}</td>"
		. "        <td><div class='edit'"
		. " onclick=\"drawForm('editproject',"
		. "	'?project_id={$project['project_id']}');\">"
		. "</div></td>\n"
		. "      </tr>\n";
	}

if ( count( $projects ) <= 0 ) {
	echo "<tr>\n"
		. "<td colspan=7 style='color: red; font-style: italic;'>"
		. "No projects found</td>"
		. "</tr>\n";
	}

echo "      <tr>
        <td colspan=7 style='padding: 10px 0 0 0;'>
          <select class='limit' onchange=\"this.blur();"
	. "updateGadget('$gadget','?gadget_id=$gadget_id"
	. "&container=$container"
	. "&start={$pager['start']}"
	. "&limit='+this.value"
	. "+'&field={$sort['field']}"
	. "&order={$sort['order']}"
	. "&searchstring=$searchstring');\">\n";

for ( $i = 10; $i <= 50; $i += 10 ) {
	echo "          <option value=$i";

	if ( $i === ( int ) $pager['limit'] ) {
		echo " selected";
		}

	echo ">$i</option>\n";
	}

echo "          </select>
          <div onclick=\"updateGadget('$gadget','?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['prev']}"
		. "&limit={$pager['limit']}"
		. "&field={$sort['field']}"
		. "&order={$sort['order']}"
		. "&searchstring=$searchstring')\" class='page-left'></div>
          <div onclick=\"updateGadget('$gadget','?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['next']}"
		. "&limit={$pager['limit']}"
		. "&field={$sort['field']}"
		. "&order={$sort['order']}"
		. "&searchstring=$searchstring')\" class='page-right'></div>
          <div class='viewing'>{$pager['info']}</div>
          <div class='new' onclick=\"drawForm('newproject','')\">new project</div>
        </td>
      </tr>
    </tbody>
  </table>\n";
?>
