<?php
require( '../inc/session.php' );

if ( $_SESSION['user']['priv_id'] < 1 ) {
	header( 'Location: ../login.php' );

	die();
	}

if ( isset( $_GET['container'] ) ) {
	$container = $_GET['container'];
	}
else {
	die();
	}

lsgadgets( $container );
?>
