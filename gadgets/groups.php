<?php
$gadget = 'groups';

if ( basename( getcwd() ) === 'gadgets' ) {
	require_once( '../inc/session.php' );

	$single = FALSE;

	if ( isset( $_GET['gadget_id'] ) ) {
		$gadget_id = $_GET['gadget_id'];
		}
	else {
		die();
		}

	if ( isset( $_GET['container'] ) ) {
		$container = $_GET['container'];
		}
	else {
		die();
		}
	}
else {
	require_once( 'inc/session.php' );

	$single = TRUE;
	$gadget_id = '';
	$container = '';
	}

if ( $_SESSION['user']['priv_id'] < 2 ) {
	header( 'HTTP/1.1 401 Unauthorized' );
	die( 'You do not have permission to view this resource' );
	}

$sort = array();

if ( isset( $_GET['field'] ) ) {
	$sort['field'] = $_GET['field'];
	}
else {
	$sort['field'] = 'group_name';
	}
if ( isset( $_GET['order'] ) ) {
	$sort['order'] = $_GET['order'];
	}
else {
	$sort['order'] = 'ASC';
	}

if ( isset( $_GET['searchstring'] ) ) {
	$searchstring = $_GET['searchstring'];
	}
else {
	$searchstring = NULL;
	}

$pager = pager( getgroupcount( $searchstring ), $_GET );
$groups = getgroups( $pager, $sort, $searchstring );

if ( isset( $groups[0] ) && $groups[0] === -1 ) {
	header( 'HTTP/1.1 500 Internal server error' );
	die( "ERROR: in getgroups():\n\n" . $groups[1] );
	}

/*
 * Here we're just populating an array that describes the TH fields
 * This should make it a little nicer to add/remove fields if needed
 * without having to mess with the sort logic
 */

$fields = array(
	'group_name' => array(
		'title' => 'Group',
		'sortable' => TRUE,
		'containers' => array(
			'leftcol',
			'rightcol'
			)
		),
	'membernum' => array(
		'title' => 'Members',
		'sortable' => TRUE,
		'containers' => array(
			'leftcol',
			'rightcol'
			)
		),
	'projectnum' => array(
		'title' => 'Projects',
		'sortable' => TRUE,
		'containers' => array(
			'leftcol',
			'rightcol'
			)
		),
	'edit' => array(
		'title' => '',
		'sortable' => FALSE,
		'containers' => array(
			'leftcol',
			'rightcol'
			)
		)
	);

echo "  <table class='$container'>
    <thead>
      <tr>\n";

foreach ( $fields as $field => $values ) {
	if ( ! in_array( $container, $values['containers'] ) && ! $single ) {
		continue;
		}
	
	if ( ! $values['sortable'] ) {
		echo "      <th>{$values['title']}</th>\n";

		continue;
		}

	$link = "?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['prev']}"
		. "&limit={$pager['limit']}"
		. "&field=$field";

	if ( ! isset( $sort['order'] ) ) {
		$link .= '&order=DESC';
		}
	else if ( $sort['order'] == 'ASC' ) {
		$link .= "&order=DESC";
		}
	else {
		$link .= "&order=ASC";
		}

	if ( isset( $searchstring ) ) {
		$link .= "&searchstring=$searchstring";
		}

	echo "      <th class='sort' onclick=\"updateGadget('$gadget','$link')\">"
		. "{$values['title']}\n"
		. "        <div class='arrows'>\n";

	if ( ! ( $field == $sort['field'] && $sort['order'] == 'DESC' )  ) {
		echo "          <div class='sortup'></div>\n";
		}
	if ( ! ( $field == $sort['field'] && $sort['order'] == 'ASC' )  ) {
		echo "          <div class='sortdown'></div>\n";
		}

	echo "        </div>"
		. "      </th>\n";
	}

echo "      </tr>
    </thead>
    <tbody>\n";

foreach ( $groups as $group ) {
	if ( isset( $group_users[0] ) && $group_users[0] === -1 ) {
		echo "<tr><td colspan=7><font color='red'>ERROR: "
			. "getgroupusers() - {$group_users[1]}</td></tr>\n";
		break;
		}

	echo "    <tr>\n      <td id='group_name{$group['group_id']}'>"
		. "{$group['group_name']}</td>\n"
		. "      <td style='max-width: 400px;'>"
		. "{$group['membernum']}</td>\n"
		. "      <td>{$group['projectnum']}</td>\n"
		. "      <td><div class='edit'"
		. " onclick=\"drawForm('editgroup','"
		. "?group_id={$group['group_id']}');\">"
		. "</div></td>\n"
		. "    </tr>\n";
	}

echo "    <tr>
      <td colspan=7 style='padding: 10px 0 0 0;'>
        <select class='limit' onchange=\"this.blur();";

if ( $single ) {
	echo "location.href='?gadget_id=$gadget_id"
	. "&container=$container"
	. "&start={$pager['start']}"
	. "&limit='+this.value"
	. "+'&field={$sort['field']}"
        . "&order={$sort['order']}"
	. "&searchstring=$searchstring';\">\n";
	}
else {
	echo "updateGadget('$gadget','?gadget_id=$gadget_id"
	. "&container=$container"
	. "&start={$pager['start']}"
	. "&limit='+this.value"
	. "+'&field={$sort['field']}"
	. "&order={$sort['order']}"
	. "&searchstring=$searchstring');\">\n";
	}

for ( $i = 10; $i <= 50; $i += 10 ) {
	echo "          <option value=$i";

	if ( $i === ( int ) $pager['limit'] ) {
		echo " selected";
		}
 
	echo ">$i</option>\n";
        }

echo "        </select>\n";

if ( $single ) {
	echo "        <a href='?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['prev']}"
		. "&limit={$pager['limit']}"
		. "&field={$sort['field']}"
		. "&order={$sort['order']}"
		. "&searchstring=$searchstring')\""
		. " class='page-left'></a>\n"
		. "        <a href='?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['next']}"
		. "&limit={$pager['limit']}"
		. "&field={$sort['field']}"
		. "&order={$sort['order']}"
		. "&searchstring=$searchstring')\""
		. " class='page-right'></a>\n";
	}
else {
	echo "        <div onclick=\"updateGadget('$gadget',"
		. "'?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['prev']}"
		. "&limit={$pager['limit']}"
		. "&field={$sort['field']}"
		. "&order={$sort['order']}"
		. "&searchstring=$searchstring')\""
		. " class='page-left'></div>\n"
		. "        <div onclick=\"updateGadget('$gadget',"
		. "'?gadget_id=$gadget_id"
		. "&container=$container"
		. "&start={$pager['next']}"
		. "&limit={$pager['limit']}"
		. "&field={$sort['field']}"
		. "&order={$sort['order']}"
		. "&searchstring=$searchstring')\""
		. " class='page-right'></div>\n";
	}

echo "        <div class='viewing'>{$pager['info']}</div>
        <div class='new' onclick=\"drawForm('newgroup','')\">new group</div>
      </td>
    </tr>
  </tbody>
</table>\n";
?>
